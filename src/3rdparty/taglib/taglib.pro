include(../../../shared.pri)
TEMPLATE = lib
QT      -= gui
TARGET   = tag
DESTDIR  = ../lib

CONFIG += \
    create_prl \
    release \
    precompile_header

VERSION = 1.6.3

DEFINES += TAGLIB_MAKEDLL

DEFINES += NDEBUG WITH_ASF WITH_MP4 TAGLIB_NO_CONFIG MAKE_TAGLIB_LIB

symbian: {
    CONFIG += staticlib
    MMP_RULES += "EXPORTUNFROZEN"
    TARGET.EPOCALLOWDLLDATA = 1
} else:unix:!macx: {
    target.path = /opt/qt-jukebox/lib
    INSTALLS += target
}

include(src/src.pri)
