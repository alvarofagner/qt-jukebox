include(../../../shared.pri)
TEMPLATE = lib
QT      += network
TARGET   = kqoauth
DESTDIR  = ../lib

CONFIG += \
    create_prl \
    release \
    precompile_headers

VERSION = 0.95

DEFINES += KQOAuth_MAKEDLL KQOAUTH

symbian: {
    CONFIG += staticlib
    MMP_RULES += "EXPORTUNFROZEN"
    TARGET.EPOCALLOWDLLDATA = 1
} else:unix:!macx: {
    target.path = /opt/qt-jukebox/lib
    INSTALLS += target
}

include(src/src.pri)
