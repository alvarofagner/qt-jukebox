INCLUDEPATH += .

HEADERS += \
    src/kqoauthmanager.h \
    src/kqoauthrequest.h \
    src/kqoauthrequest_1.h \
    src/kqoauthrequest_xauth.h \
    src/kqoauthglobals.h \
    src/kqoauthrequest_p.h \
    src/kqoauthmanager_p.h \
    src/kqoauthauthreplyserver.h \
    src/kqoauthauthreplyserver_p.h \
    src/kqoauthutils.h \
    src/kqoauthrequest_xauth_p.h

SOURCES += \
    src/kqoauthmanager.cpp \
    src/kqoauthrequest.cpp \
    src/kqoauthutils.cpp \
    src/kqoauthauthreplyserver.cpp \
    src/kqoauthrequest_1.cpp \
    src/kqoauthrequest_xauth.cpp
