INCLUDEPATH += $$PWD

HEADERS += \
    src/json_parser.hh \
    src/json_scanner.h \
    src/location.hh \
    src/parser_p.h \
    src/position.hh \
    src/qjson_debug.h \
    src/stack.hh \
    src/parser.h \
    src/parserrunnable.h \
    src/qobjecthelper.h \
    src/serializer.h \
    src/serializerrunnable.h \
    src/qjson_export.h

SOURCES += \
    src/json_parser.cc \
    src/json_scanner.cpp \
    src/parser.cpp \
    src/parserrunnable.cpp \
    src/qobjecthelper.cpp \
    src/serializer.cpp \
    src/serializerrunnable.cpp
