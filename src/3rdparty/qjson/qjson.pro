include(../../../shared.pri)
TEMPLATE = lib
QT      -= gui
TARGET   = qjson
DESTDIR  = ../lib

CONFIG += \
    create_prl \
    release \
    precompile_header

VERSION = 0.7.1

DEFINES += QJSON_MAKEDLL

symbian: {
    CONFIG += staticlib
    MMP_RULES += "EXPORTUNFROZEN"
    TARGET.EPOCALLOWDLLDATA = 1
} else:unix:!macx: {
    target.path = /opt/qt-jukebox/lib
    INSTALLS += target
}

include(src/src.pri)
