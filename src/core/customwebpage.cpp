#include "customwebpage.h"

CustomWebPage::CustomWebPage(QWidget *parent)
    : QWebPage(parent)
{
}

QString CustomWebPage::userAgentForUrl(const QUrl & url) const
{
    Q_UNUSED(url)
    return "Mozilla/5.0 (en_US) AppleWebKit (KHTML, like Gecko) Safari";
}
