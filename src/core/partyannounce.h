#ifndef PARTYANNOUNCE_H
#define PARTYANNOUNCE_H

#include <QtCore/QObject>
#include <QSharedPointer>
#include <QtCore/QVariant>
#include <QtNetwork/QHostAddress>

#include "partyitem.h"

class QUdpSocket;
class QTimer;

class PartyAnnounce : public QObject
{
    Q_OBJECT

public:
    PartyAnnounce(QObject *parent = 0);
    void start(const QSharedPointer<PartyItem> &party);
    void stop();

private slots:
    void sendDatagram();

private:
    QUdpSocket *m_socket;
    QTimer *m_timer;
    QVariantMap m_partyData;
#if QT_VERSION >= 0x040800
    static const QHostAddress s_groupAddress;
    static const int s_ttl;
#endif

    void setData(const QSharedPointer<PartyItem> &party);
};

#endif // PARTYANNOUNCE_H
