/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef MUSICITEMSMODELPROXY_H
#define MUSICITEMSMODELPROXY_H

#include <QSortFilterProxyModel>
#include <QtCore/QMetaType>

#include "musicitem.h"


class MusicItemModelProxy : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    MusicItemModelProxy(QObject *parent = 0);
    MusicItemModelProxy(const MusicItemModelProxy &other);

    uint itemsByPage() const { return m_itemsByPage; }
    void setItemsByPage(int items) { m_itemsByPage = items; }

    uint page() const { return m_page; }
    void setPage(uint page) { m_page = page; }

public slots:
    QSharedPointer<MusicItem> get(int idx);
    QVariant getDataByIndex(int idx, int column);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    uint m_itemsByPage;
    uint m_page;

    bool itemOnPage(int sourcePage, int page) const;
};

Q_DECLARE_METATYPE(MusicItemModelProxy *)

#endif // MUSICITEMSMODELPROXY_H
