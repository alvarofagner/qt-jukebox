/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef MUSICITEMSMODEL_H
#define MUSICITEMSMODEL_H

#include <QtCore/QModelIndex>
#include <QtCore/QSharedPointer>
#include <QtCore/QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQueryModel>

#include "musicitem.h"

class MusicItemModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    MusicItemModel(QObject *parent = 0, QSqlDatabase db = QSqlDatabase(), const QString &tableName = QLatin1String("songsClient"));
    QVariant data(const QModelIndex &rowIndex, int role = Qt::DisplayRole) const;
    QSharedPointer<MusicItem> get(int index);
    ListOfMusicItems listOfMusicItems(const QString &filter = QString());
    QSharedPointer<MusicItem> itemById(const QString &id);

    void clear();

public slots:
    bool insertMusicItem(const QSharedPointer<MusicItem> &item);
    bool insertMusicItems(const ListOfMusicItems &items);
    bool updateMusicItem(const QSharedPointer<MusicItem> &item);
    bool deleteMusicItem(const QSharedPointer<MusicItem> &item);
    bool updateVotesCount(const QSharedPointer<MusicItem> &item);
    bool updateVotesCount(const QString &id, int votesCount);

private:
    QSqlDatabase m_db;
    QString m_tableName;

    void refresh();
};

#endif // MUSICITEMSMODEL_H
