/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef NETWORKREQUEST_H
#define NETWORKREQUEST_H

#include <QByteArray>
#include <QHash>
#include <QString>
#include <QTimer>

#include <QNetworkRequest>


class NetworkRequest : public QObject
{
    Q_OBJECT

public:
    enum RequestType {
        RequestAuthentication
    };

    NetworkRequest(const QUrl &url, RequestType type, QObject *parent = 0);
    virtual ~NetworkRequest();

    int id() const;

    QUrl url() const;
    void setUrl(const QUrl &url);

    RequestType type() const;

    void setRawHeader(const QByteArray &key, const QByteArray &value);

    QNetworkRequest networkRequest() const;

signals:
    void timeout(NetworkRequest *request);

public slots:
    void startTimeoutTimer();
    void stopTimeoutTimer();

private slots:
    void onTimeout();

private:
    int m_id;
    RequestType m_type;
    QTimer m_timeoutTimer;
    QNetworkRequest m_networkRequest;
};

#endif // NETWORKREQUEST_H
