#include "partyannounce.h"

#include <QtNetwork/QUdpSocket>
#include <QtCore/QTimer>
#include <QtCore/QDebug>

#define UDP_PORT 12345
#define APP_NAME "Qt-Jukebox"

#if QT_VERSION >= 0x040800
const QHostAddress PartyAnnounce::s_groupAddress = QHostAddress("239.255.43.21");
const int  PartyAnnounce::s_ttl = 1;
#endif

PartyAnnounce::PartyAnnounce(QObject *parent)
    : QObject(parent)
    , m_socket(new QUdpSocket(this))
    , m_timer(new QTimer(this))
    , m_partyData()
{
    connect(m_timer, SIGNAL(timeout()), this, SLOT(sendDatagram()));
#if QT_VERSION >= 0x040800
    m_socket->setSocketOption(QAbstractSocket::MulticastTtlOption, s_ttl);
#endif
}

void PartyAnnounce::start(const QSharedPointer<PartyItem> &party)
{
    if (m_timer->isActive())
        m_timer->stop();

    setData(party);
    m_timer->start(1000);
}

void PartyAnnounce::stop()
{
    m_timer->stop();
}

void PartyAnnounce::setData(const QSharedPointer<PartyItem> &party)
{
    m_partyData["app"] = APP_NAME;
    m_partyData["action"] = "partyAnnounce";
    m_partyData["partyUuid"] = party->uuid();
    m_partyData["partyName"] = party->name();
    m_partyData["partyHashtag"] = party->hashtag();
}

void PartyAnnounce::sendDatagram()
{
    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_7);
    out << QVariant(m_partyData);

#if QT_VERSION >= 0x040800
    m_socket->writeDatagram(datagram.data(), datagram.size(), s_groupAddress, UDP_PORT);
#else
    m_socket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, UDP_PORT);
#endif
}
