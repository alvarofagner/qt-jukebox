/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QString>
#include <QtNetwork/QTcpServer>

class Jukebox;


class Server : public QTcpServer
{
    Q_OBJECT
public:

    Server(quint16 port, Jukebox *jukebox, QObject *parent = 0);

    void incomingConnection(int handle);

signals:
    void vote(const QString &id);

private slots:
    void onCatalogRequested();

private:
    Jukebox *m_jukebox;
};

#endif
