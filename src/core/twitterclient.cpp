/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "twitterclient.h"

#ifdef USE_3RDPARTY
#include <parser.h>
#else
#include <qjson/parser.h>
#endif

#include <QtCore/QDebug>
#include <QtCore/QSettings>

const QString TwitterClient::s_requestTokenUrl("https://api.twitter.com/oauth/request_token");
const QString TwitterClient::s_userAuthorizeUrl("https://api.twitter.com/oauth/authorize");
const QString TwitterClient::s_userAccessTokenUrl("https://api.twitter.com/oauth/access_token");
const QString TwitterClient::s_usersShowUrl("http://api.twitter.com/1/users/show.json");
const QString TwitterClient::s_statusTimelineUrl("http://api.twitter.com/1/statuses/user_timeline.json");
const QString TwitterClient::s_statusUpdateUrl("http://api.twitter.com/1/statuses/update.xml");
const QString TwitterClient::s_statusDestroyUrl("https://api.twitter.com/1/statuses/destroy/%1.json");
const QString TwitterClient::s_streamTrackUrl("https://stream.twitter.com/1/statuses/filter.json");
const QString TwitterClient::s_verifyCredentials("https://api.twitter.com/1/account/verify_credentials.json");

const QString TwitterClient::s_consumerKey("zQDZdO4nNyajn5msqGNQ");
const QString TwitterClient::s_consumerSecretKey("GGLtRwkBFAOVJANrj3u63AYiz92YCwD5w6gA85a98");

TwitterClient::TwitterClient(QObject *parent)
    : QObject(parent)
    , m_oauthManager(new KQOAuthManager(this))
    , m_oauthRequest(new KQOAuthRequest())
    , m_settings(new QSettings(this))
    , m_credentialsStatus(CredentialsNotAvailable)
{
    m_oauthManager->setHandleUserAuthorization(true);
    m_oauthRequest->setEnableDebugOutput(true);

    connect(m_oauthManager, SIGNAL(requestReady(QByteArray)),
            SLOT(onRequestReady(QByteArray)));
    connect(m_oauthManager, SIGNAL(authorizedRequestReady(QByteArray, int)),
            SLOT(onAuthorizedRequestReady(QByteArray, int)));
    connect(m_oauthManager, SIGNAL(authenticatedStreamUpdated(QByteArray, int)),
            SLOT(onAuthorizedRequestReady(QByteArray, int)));
    connect(m_oauthManager, SIGNAL(authorizedRequestDone()),
            SLOT(onAuthorizedRequestDone()));
}

TwitterClient::~TwitterClient()
{
    delete m_oauthRequest;
    delete m_oauthManager;
}

bool TwitterClient::isAuthenticated()
{
    return !username().isEmpty() && !oAuthToken().isEmpty() && !oAuthTokenSecret().isEmpty();
}

void TwitterClient::auth()
{
    connect(m_oauthManager, SIGNAL(temporaryTokenReceived(QString, QString)),
            this, SLOT(onTemporaryTokenReceived(QString, QString)));

    connect(m_oauthManager, SIGNAL(authorizationReceived(QString, QString)),
            this, SLOT(onAuthorizationReceived(QString, QString)));

    connect(m_oauthManager, SIGNAL(accessTokenReceived(QString, QString)),
            this, SLOT(onAccessTokenReceived(QString, QString)));

    m_oauthRequest->initRequest(KQOAuthRequest::TemporaryCredentials, QUrl(s_requestTokenUrl));

    m_oauthRequest->setConsumerKey(s_consumerKey);
    m_oauthRequest->setConsumerSecretKey(s_consumerSecretKey);

    m_oauthManager->executeRequest(m_oauthRequest);

}

void TwitterClient::onTemporaryTokenReceived(QString token, QString tokenSecret)
{
    Q_UNUSED(token);
    Q_UNUSED(tokenSecret);

    QPair<QString, QString> tokenParam = qMakePair(QString("oauth_token"), QString(token));
    QUrl url(s_userAuthorizeUrl, QUrl::StrictMode);
    url.addQueryItem(tokenParam.first, tokenParam.second);

    if (m_oauthManager->lastError() == KQOAuthManager::NoError) {
        qDebug() << "Opening authorization web site: " << url;
        m_oauthManager->getUserAuthorization(url);
    }
}

void TwitterClient::onAuthorizationReceived(QString token, QString verifier)
{
    Q_UNUSED(token);
    Q_UNUSED(verifier);

    if (m_oauthManager->lastError() != KQOAuthManager::NoError) {
        emit error(m_oauthManager->lastError());
        return;
    }

    m_oauthManager->getUserAccessTokens(QUrl(s_userAccessTokenUrl));
}

void TwitterClient::onAccessTokenReceived(QString token, QString tokenSecret)
{
    m_settings->setValue("oauth_token", token);
    m_settings->setValue("oauth_token_secret", tokenSecret);

    emit authenticatedChanged();
}

void TwitterClient::onAuthorizedRequestReady(QByteArray response, int id)
{
    QJson::Parser parser;
    bool ok;

    QVariant result = parser.parse(response, &ok);
    if (!ok) {
        qWarning("[TwitterClient] Error parsing.");
        return;
    }

    switch (id) {
    case GetUserInfoID:
        emit userInfo(result);
        break;
    case GetStatusTimelineID:
        emit userTimeline(result);
        break;
    case TrackTweetID:
        emit newTrackedTweet(result);
        break;
    case VerifyCredentials:
        processCredentials(result);
        break;
    }
}

void TwitterClient::onRequestReady(QByteArray response)
{
    qDebug() << "Response from the service: " << m_oauthManager->lastError() << response;
}

void TwitterClient::onAuthorizedRequestDone()
{
    QString reason;
    QString description;

    switch (m_oauthManager->lastError()) {
    case KQOAuthManager::NoError:
        qDebug() << "[TwitterClient]" << Q_FUNC_INFO << "Request sent to Twitter!";
        return;
    case KQOAuthManager::NetworkError:
        reason = "Network error";
        description = "Timeout, cannot connect.";
        break;
    case KQOAuthManager::RequestEndpointError:
        reason = "Request endpoint error";
        description = "Request endpoint is not valid.";
        break;
    case KQOAuthManager::RequestValidationError:
        reason = "Request is not valid";
        description = "Some parameter missing?";
        break;
    case KQOAuthManager::RequestUnauthorized:
        reason = "Authorization error";
        description = "Trying to access a resource without tokens.";
        break;
    case KQOAuthManager::RequestError:
        reason = "The given request to KQOAuthManager is invalid";
        description = "NULL?,";
        break;
    case KQOAuthManager::ManagerError:
        reason = "KQOAuth Manager error";
        description = "Manager error, cannot use for sending requests.";
        break;
    }

    emit errorAuthenticating(reason, description);
    qDebug() << "[TwitterClient]" << Q_FUNC_INFO << "lastError:" << m_oauthManager->lastError();
}

void TwitterClient::processCredentials(const QVariant &var)
{
    if (var.isNull() || (m_oauthManager->lastError() != KQOAuthManager::NoError)) {
        m_credentialsStatus = CredentialsInvalid;
        emit credentialsStatusChanged();
        return;
    }

    QVariantMap map = var.toMap();

    m_screenName = map.value("screen_name").toString();

    m_credentialsStatus = CredentialsValid;
    emit credentialsStatusChanged();
}

void TwitterClient::initRequest(KQOAuthRequest::RequestType requestType, const QUrl &url,
                                KQOAuthRequest *request)
{
    if (!request) {
        request = m_oauthRequest;
    }

    request->initRequest(requestType, url);
    request->setConsumerKey(s_consumerKey);
    request->setConsumerSecretKey(s_consumerSecretKey);
    request->setToken(oAuthToken());
    request->setTokenSecret(oAuthTokenSecret());
}

void TwitterClient::sendTweet(const QString &tweet)
{
    if (oAuthToken().isEmpty() || oAuthTokenSecret().isEmpty()) {
        qDebug() << "[TwitterClient] No access tokens. Aborting.";
        return;
    }

    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_statusUpdateUrl));

    KQOAuthParameters params;
    params.insert("status", tweet.count() >= 140 ? tweet.left(136) + "..." : tweet);
    m_oauthRequest->setAdditionalParameters(params);

    m_oauthManager->executeRequest(m_oauthRequest);
}

void TwitterClient::removeTweet(const QString &id)
{
    if (oAuthToken().isEmpty() || oAuthTokenSecret().isEmpty()) {
        qDebug() << "[TwitterClient] No access tokens. Aborting.";
        return;
    }

    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_statusDestroyUrl.arg(id)));

    m_oauthManager->executeRequest(m_oauthRequest);
}

void TwitterClient::verifyCredentials()
{
    if (oAuthToken().isEmpty() || oAuthTokenSecret().isEmpty()) {
        qDebug() << "[TwitterClient] No access tokens.";
        processCredentials(QVariant());
        return;
    }

    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_verifyCredentials));

    m_oauthRequest->setHttpMethod(KQOAuthRequest::GET);

    m_oauthManager->executeAuthorizedRequest(m_oauthRequest, VerifyCredentials);
}

void TwitterClient::track(const QString &keyword, const QString &followIDs)
{
    if (oAuthToken().isEmpty() || oAuthTokenSecret().isEmpty()) {
        qDebug() << "[TwitterClient] No access tokens. Aborting.";
        return;
    }

    KQOAuthRequest *request = new KQOAuthRequest(this);
    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_streamTrackUrl), request);

    KQOAuthParameters params;
    if (!keyword.isEmpty()) {
        params.insert("track", keyword);
    }
    if (!followIDs.isEmpty()) {
        params.insert("follow", followIDs);
    }
    request->setAdditionalParameters(params);

    m_oauthManager->executeAuthorizedRequest(request, TrackTweetID);
}

void TwitterClient::getUserInfo(const QString &userName)
{
    KQOAuthParameters params;
    params.insert("screen_name", userName);

    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_usersShowUrl));

    m_oauthRequest->setAdditionalParameters(params);
    m_oauthRequest->setHttpMethod(KQOAuthRequest::GET);

    m_oauthManager->executeAuthorizedRequest(m_oauthRequest, GetUserInfoID);
}

void TwitterClient::getUserTimeline(const QString &userName)
{
    KQOAuthParameters params;
    if (userName.count()) {
        params.insert("screen_name", userName);
    }

    initRequest(KQOAuthRequest::AuthorizedRequest, QUrl(s_statusTimelineUrl));

    m_oauthRequest->setAdditionalParameters(params);
    m_oauthRequest->setHttpMethod(KQOAuthRequest::GET);

    m_oauthManager->executeAuthorizedRequest(m_oauthRequest, GetStatusTimelineID);
}

QString TwitterClient::username() const
{
    return m_screenName;
}

QString TwitterClient::oAuthToken() const
{
    return m_settings->value("oauth_token").toString();
}

QString TwitterClient::oAuthTokenSecret() const
{
    return m_settings->value("oauth_token_secret").toString();
}
