/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef FONTLOADERFAKE_H
#define FONTLOADERFAKE_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QUrl>

class FontLoaderFake : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(int status READ status WRITE setStatus NOTIFY statusChanged)

public:
    FontLoaderFake(QObject *parent = 0);

    QString name() const { return "Nokia Sans"; }
    void setName(const QString &) { }

    QUrl source() const { return QUrl(); }
    void setSource(const QUrl &) { }

    int status() const { return 0; }
    void setStatus(int) { }

signals:
    void nameChanged();
    void sourceChanged();
    void statusChanged();

};

#endif // FONTLOADERFAKE_H
