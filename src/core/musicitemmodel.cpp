/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "musicitemmodel.h"
#include <QtCore/QByteArray>
#include <QtCore/QHash>
#include <QtCore/QMetaProperty>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtCore/QDebug>


MusicItemModel::MusicItemModel(QObject *parent, QSqlDatabase db, const QString &tableName)
    : QSqlQueryModel(parent)
    , m_db(db)
    , m_tableName(tableName)
{
    QHash<int, QByteArray> roles;

    roles[Qt::UserRole + 0] = "SongId";
    roles[Qt::UserRole + 1] = "SongTitle";
    roles[Qt::UserRole + 2] = "ArtistName";
    roles[Qt::UserRole + 3] = "AlbumName";
    roles[Qt::UserRole + 4] = "SongUrl";
    roles[Qt::UserRole + 5] = "VotesCount";

    setRoleNames(roles);
    refresh();
}

QSharedPointer<MusicItem> MusicItemModel::itemById(const QString &id)
{
    QSqlQuery query(m_db);
    query.prepare("select * from " + m_tableName + " where songId = :songId");
    query.bindValue(":songId", id);

    if (query.exec() && query.next()) {
        QSharedPointer<MusicItem> item(new MusicItem);
        QStringList props = item->objectDBProperties();

        QList<QString>::iterator i;
        for (i = props.begin(); i != props.end(); ++i)
            item->setProperty(i->toAscii(), query.record().value(*i));

        return item;
    }

    return QSharedPointer<MusicItem>();
}

void MusicItemModel::clear()
{
    QSqlQuery query(m_db);
    query.prepare("delete from " + m_tableName);
    query.exec();
    QSqlQueryModel::clear();
}

QVariant MusicItemModel::data(const QModelIndex &rowIndex, int role) const
{
    if (!rowIndex.isValid())
        return QVariant();

    if (role < Qt::UserRole) {
        return QSqlQueryModel::data(rowIndex, role);
    } else {
        return QSqlQueryModel::data(index(rowIndex.row(), role - Qt::UserRole), Qt::DisplayRole);
    }
}

QSharedPointer<MusicItem> MusicItemModel::get(int idx)
{
    QSharedPointer<MusicItem> item(new MusicItem());

    const QStringList props = item->objectDBProperties();
    for (int i = 0; i < props.count(); i++) {
        item->setProperty(props[i].toAscii(), data(index(idx, 0), Qt::UserRole + i));
    }

    return item;
}

ListOfMusicItems MusicItemModel::listOfMusicItems(const QString &filter)
{
    Q_UNUSED(filter)

    ListOfMusicItems list;

    for (int i = 0; i < rowCount(); i++) {
        list.append(get(i));
    }

    return list;
}

bool MusicItemModel::insertMusicItem(const QSharedPointer<MusicItem> &item)
{
    QSqlQuery query(m_db);
    query.prepare("insert into " + m_tableName +
                  " (songId, songTitle, artistName, albumName, songUrl, votesCount)"
                  " values (:songId, :songTitle, :artistName, :albumName, :songUrl, :votesCount);");
    query.bindValue(":songId", item->songId());
    query.bindValue(":songTitle", item->songTitle());
    query.bindValue(":artistName", item->artistName());
    query.bindValue(":albumName", item->albumName());
    query.bindValue(":songUrl", item->songUrl());
    query.bindValue(":votesCount", item->votesCount());

    bool ret = query.exec();
    refresh();
    return ret;
}

bool MusicItemModel::insertMusicItems(const ListOfMusicItems &items)
{
    m_db.transaction();
    QSqlQuery query(m_db);
    QList<QSharedPointer<MusicItem> >::const_iterator i;
    for (i = items.begin(); i != items.end(); ++i) {
        query.prepare("insert into " + m_tableName +
                      " (songId, songTitle, artistName, albumName, songUrl, votesCount)"
                      " values (:songId, :songTitle, :artistName, :albumName, :songUrl, :votesCount);");
        query.bindValue(":songId", (*i)->songId());
        query.bindValue(":songTitle", (*i)->songTitle());
        query.bindValue(":artistName", (*i)->artistName());
        query.bindValue(":albumName", (*i)->albumName());
        query.bindValue(":songUrl", (*i)->songUrl());
        query.bindValue(":votesCount", (*i)->votesCount());
        query.exec();
    }

    bool ret = m_db.commit();
    refresh();
    return ret;
}

bool MusicItemModel::updateMusicItem(const QSharedPointer<MusicItem> &item)
{
    QSqlQuery query(m_db);
    query.prepare("update " + m_tableName +
                  " set songTitle = :songTitle, artistName = :artistName,"
                  " albumName = :albumName, songUrl = :songUrl, votesCount = :votesCount"
                  " where songId = :songId;");
    query.bindValue(":songId", item->songId());
    query.bindValue(":songTitle", item->songTitle());
    query.bindValue(":artistName", item->artistName());
    query.bindValue(":albumName", item->albumName());
    query.bindValue(":songUrl", item->songUrl());
    query.bindValue(":votesCount", item->votesCount());
    bool ret = query.exec();
    refresh();
    return ret;
}

bool MusicItemModel::deleteMusicItem(const QSharedPointer<MusicItem> &item)
{
    QSqlQuery query(m_db);
    query.prepare("delete from " + m_tableName +
                  " where songId = :songId;");
    query.bindValue(":songId", item->songId());
    bool ret = query.exec();
    refresh();
    return ret;
}

bool MusicItemModel::updateVotesCount(const QSharedPointer<MusicItem> &item)
{
    return updateVotesCount(item->songId(), item->votesCount());
}

bool MusicItemModel::updateVotesCount(const QString &id, int votesCount)
{
    QSqlQuery query(m_db);
    query.prepare("update " + m_tableName +
                  " set votesCount = :votesCount"
                  " where songId = :songId;");
    query.bindValue(":songId", id);
    query.bindValue(":votesCount", votesCount);
    bool ret = query.exec();
    refresh();
    return ret;
}

void MusicItemModel::refresh()
{
    setQuery("select * from " + m_tableName + " order by SongId;", m_db);
    while (canFetchMore()) {
        fetchMore();
    }
}
