/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include <QtGui/QApplication>
#include <QtGui/QStyle>
#include <QtGui/QDesktopWidget>

#include "global.h"
#include "jukebox.h"
#include "utils.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName("Nokia");
    app.setApplicationName("QtJukebox");

    Jukebox::Behavior behavior = Jukebox::JukeboxInvalid;

#if defined(USE_GUESTMODE)
    behavior = Jukebox::JukeboxGuestMode;
    app.setApplicationName("QtJukeboxGuest");
#else
    QStringList args = app.arguments();
    if (args.contains("--guest-mode")) {
        behavior = Jukebox::JukeboxGuestMode;
        app.setApplicationName("QtJukeboxGuest");
    }
#endif

    Jukebox jukebox(behavior);
    Utils::setOrientation(Utils::Landscape);
#if defined(Q_WS_HARMATTAN) || defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR) || \
    defined(Q_WS_MAEMO_5) || defined(Q_WS_MAEMO_6)

    jukebox.setGeometry(QApplication::desktop()->screenGeometry());
    jukebox.setPlatform(Jukebox::MobilePlatform);
    jukebox.showFullScreen();
#elif defined(Q_WS_WIN32)
    jukebox.setPlatform(Jukebox::DesktopPlatform);
    jukebox.setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, QSize(864, 480),  app.desktop()->availableGeometry()));
    jukebox.show();
#else
    jukebox.setPlatform(Jukebox::DesktopPlatform);
    jukebox.setGeometry(QRect(0, 0, 864, 480));
    jukebox.show();
#endif

    return app.exec();
}
