/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>

#include "global.h"

QFileInfo Global::dataDir()
{
    QStringList pathList(QString(XSTR(DATA_PATH)).split(":", QString::SkipEmptyParts));

    foreach (QFileInfo path, pathList)
        if (path.exists() && path.isDir())
            return path;

    return QFileInfo();
}

QFileInfo Global::qmlDir()
{
    QStringList pathList(QString(XSTR(QML_PATH)).split(":", QString::SkipEmptyParts));

    foreach (QFileInfo path, pathList)
        if (path.exists() && path.isDir())
            return path;

    return QFileInfo();
}
