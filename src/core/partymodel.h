#ifndef PARTYMODEL_H
#define PARTYMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QList>
#include <QtCore/QSharedPointer>

#include "partyitem.h"

class PartyModel : public QAbstractListModel
{
    Q_OBJECT

public:
    PartyModel(QObject *parent = 0);

    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QSharedPointer<PartyItem> searchParty(const QString &uuid) const;

public slots:
    void addItem(const QSharedPointer<PartyItem> &item);
    void clear();

private:
    QList<QSharedPointer<PartyItem> > m_items;
};

#endif // PARTYMODEL_H
