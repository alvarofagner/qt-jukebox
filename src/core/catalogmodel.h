/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef CATALOGMODEL_H
#define CATALOGMODEL_H

#include "musicitemmodelproxy.h"

#include <QtCore/QAbstractListModel>
#include <QtCore/QList>
#include <QtCore/QMetaType>
#include <QtCore/QSharedPointer>

class CatalogModel : public QAbstractListModel
{
    Q_OBJECT

public:
    CatalogModel(QObject *parent = 0);
    CatalogModel(const CatalogModel &other);
    CatalogModel(QAbstractItemModel *musicModel, QObject *parent = 0);

public slots:
    QVariant data(const QModelIndex &rowIndex, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    void refreshItems();

private:
    QAbstractItemModel *m_musicModel;
    int m_itemsByPage;
    QList<QSharedPointer<MusicItemModelProxy> > m_items;
};

Q_DECLARE_METATYPE(CatalogModel *)

#endif // CATALOGMODEL_H
