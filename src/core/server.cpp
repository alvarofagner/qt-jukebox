/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "server.h"
#include "serversocket.h"
#include "jukebox.h"
#include "catalogcontroller.h"
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtCore/QDebug>

Server::Server(quint16 port, Jukebox *jukebox, QObject *parent)
    : QTcpServer(parent)
    , m_jukebox(jukebox)
{
    listen(QHostAddress::Any, port);
}

void Server::incomingConnection(int handle)
{
    qDebug() << "### New Connection";

    ServerSocket *sock = new ServerSocket;
    sock->setSocketDescriptor(handle);
    QThread *th = new QThread;
    sock->moveToThread(th);

    // delete socket at disconnect
    connect(sock, SIGNAL(disconnected()),
            sock, SLOT(deleteLater()));
    // stop thread at socket delete
    connect(sock, SIGNAL(destroyed()),
            th, SLOT(quit()));
    // delete thread at thread stop
    connect(th, SIGNAL(finished()),
            th, SLOT(deleteLater()));

    // parent is a Jukebox instance (need to be!)
    connect(m_jukebox->catalogController(), SIGNAL(catalogAddedItems(ListOfMusicItems)),
            sock, SLOT(onCatalogAddedItems(ListOfMusicItems)));
    connect(m_jukebox->catalogController(), SIGNAL(catalogDeletedItems(ListOfMusicItems)),
            sock, SLOT(onCatalogDeletedItems(ListOfMusicItems)));
    connect(m_jukebox->catalogController(), SIGNAL(catalogUpdatedItems(ListOfMusicItems)),
            sock, SLOT(onCatalogUpdatedItems(ListOfMusicItems)));
    connect(m_jukebox->catalogController(), SIGNAL(catalogSettedItems(ListOfMusicItems)),
            sock, SLOT(onCatalogListItems(ListOfMusicItems)));
    connect(m_jukebox, SIGNAL(nowPlaying(QString)),
            sock, SLOT(onNowPlaying(QString)));
    connect(sock, SIGNAL(catalogRequested()),
            this, SLOT(onCatalogRequested()));
    connect(sock, SIGNAL(vote(QString)),
            this, SIGNAL(vote(QString)));

    th->start();
}

void Server::onCatalogRequested()
{
    ServerSocket *sock = qobject_cast<ServerSocket*>(sender());
    if (sock) {
        QMetaObject::invokeMethod(sock, "onCatalogListItems",
                                  Q_ARG(ListOfMusicItems, m_jukebox->catalogController()->catalogItems()));
        MusicItem *nowPlayingSong = m_jukebox->nowPlayingSong();
        if (nowPlayingSong) {
            sock->setNowPlayingSongId(nowPlayingSong->songId());
            QTimer::singleShot(100, sock, SLOT(forceNowPlaying()));
        }
    }
}
