#ifndef PARTYDISCOVER_H
#define PARTYDISCOVER_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtNetwork/QHostAddress>

#include "partyitem.h"
#include "partymodel.h"

class QTimer;
class QUdpSocket;

class PartyDiscover : public QObject
{
    Q_OBJECT
public:
    PartyDiscover(QObject *parent = 0);

    PartyModel *partyModel() const;

public slots:
    void start();
    void stop();

signals:
    void timeout();
    void partyFound(const QSharedPointer<PartyItem> &party);

private slots:
    void processDatagrams();

private:
    QUdpSocket *m_socket;
    PartyModel *m_partyModel;
    QTimer *m_timer;
#if QT_VERSION >= 0x040800
    static const QHostAddress s_groupAddress;
#endif
};

#endif // PARTYDISCOVER_H
