/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "catalogcontroller.h"
#include "catalogmodel.h"
#include "circularprogress.h"
#include "clientsocket.h"
#include "global.h"
#include "jukebox.h"
#include "musicitem.h"
#include "musicitemmodel.h"
#include "musicitemmodelproxy.h"
#include "partyannounce.h"
#include "partydiscover.h"
#include "server.h"
#include "twitterclient.h"
#include "utils.h"
#include "importpathsmodel.h"

#include "fontloaderfake.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QRegExp>
#include <QtCore/QSharedPointer>
#include <QtCore/QTimer>
#include <QtCore/QUuid>
#include <QtCore/QSettings>
#include <QtDeclarative/QDeclarativeContext>
#include <QtDeclarative/QDeclarativeView>
#include <QtGui/QApplication>
#include <QtGui/QDesktopServices>
#include <QtGui/QDesktopWidget>
#include <QtGui/QResizeEvent>
#include <QtNetwork/QNetworkInterface>

#define LISTENING_PORT 12345

const QString Jukebox::s_applicationName("qt-jukebox");
const QString Jukebox::s_organizationName("Nokia");

Jukebox::Jukebox(Behavior behavior, QWidget *parent)
    : QMainWindow(parent)
    , m_view(new QDeclarativeView(this))
    , m_context(m_view->rootContext())
#ifdef Q_OS_SYMBIAN
    , m_appHeight(360)
    , m_appWidth(640)
#else
    , m_appHeight(480)
    , m_appWidth(864)
#endif
    , m_contentHeight(1317)
    , m_behavior(behavior)
    , m_platform(DesktopPlatform)
    , m_twitterClient(new TwitterClient(this))
    , m_server(0)
    , m_clientSocket(0)
    , m_partyAnnounce(new PartyAnnounce(this))
    , m_partyDiscover(new PartyDiscover(this))
    , m_mediaPlayer(new QMediaPlayer(this))
    , m_defaultNowPlayingSong(QSharedPointer<MusicItem>(new MusicItem))
    , m_nowPlayingSong(m_defaultNowPlayingSong)
    , m_playerPlaying(false)
    , m_party(QSharedPointer<PartyItem>(new PartyItem))
    , m_partyType(PrivateParty)
    , m_djPort(0)
    , m_catalogController(new CatalogController(this))
    , m_importPathsModel(new ImportPathsModel())
    , m_catalogLoaded(false)
    , m_settings(new QSettings(this))
{
    connect((QObject *)m_view->engine(), SIGNAL(quit()), SLOT(close()));

    setCentralWidget(m_view);
    m_view->setResizeMode(QDeclarativeView::SizeRootObjectToView);

    setMinimumHeight(m_appHeight);
    setMinimumWidth(m_appWidth);

    connect(m_catalogController, SIGNAL(catalogAddedItems(ListOfMusicItems)),
            this, SIGNAL(catalogEmptyChanged()));
    connect(m_catalogController, SIGNAL(catalogDeletedItems(ListOfMusicItems)),
            this, SIGNAL(catalogEmptyChanged()));
    connect(m_catalogController, SIGNAL(catalogSettedItems(ListOfMusicItems)),
            this, SIGNAL(catalogEmptyChanged()));
    connect(m_catalogController, SIGNAL(filesCountUpdated(int)),
            this, SIGNAL(catalogCountChanged()));
    connect(m_catalogController, SIGNAL(catalogLoadedChanged()),
            this, SIGNAL(catalogLoadedChanged()));
    connect(this, SIGNAL(behaviorChanged()),
            this, SIGNAL(connectedChanged()));
    connect(this, SIGNAL(partyChanged()),
            this, SLOT(refreshCatalog()));
    connect(this, SIGNAL(behaviorChanged()),
            this, SLOT(refreshCatalog()));

    connect(m_mediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)),
            this, SLOT(onMediaStateChanged(QMediaPlayer::State)));
    connect(m_mediaPlayer, SIGNAL(durationChanged(qint64)),
            this, SIGNAL(songDurationChanged(qint64)));
    connect(m_mediaPlayer, SIGNAL(positionChanged(qint64)),
            this, SIGNAL(songPositionChanged(qint64)));

    m_party->setUuid(QUuid::createUuid().toString());

    qRegisterMetaType<ListOfMusicItems>("ListOfMusicItems");
    qmlRegisterType<CircularProgress>("com.qtjukebox.types", 1, 0, "CircularProgress");
    qmlRegisterType<PartyModel>("com.qtjukebox.types", 1, 0, "PartyModel");
    qmlRegisterType<MusicItemModel>("com.qtjukebox.types", 1, 0, "MusicItemModel");
    qmlRegisterType<MusicItemModelProxy>("com.qtjukebox.types", 1, 0, "MusicItemModelProxy");
    qmlRegisterType<CatalogModel>("com.qtjukebox.types", 1, 0, "CatalogModel");
    qmlRegisterType<Jukebox>("com.qtjukebox.types", 1, 0, "MusicJukebox");
    qmlRegisterType<MusicItem>("com.qtjukebox.types", 1, 0, "MusicItem");
    qmlRegisterType<PartyItem>("com.qtjukebox.types", 1, 0, "PartyItem");
    qmlRegisterType<ImportPathsModel>("com.qtjukebox.types", 1, 0, "ImportPathsModel");
    qmlRegisterType<TwitterClient>("com.qtjukebox.types", 1, 0, "TwitterClient");
#ifdef Q_OS_SYMBIAN
    qmlRegisterType<FontLoaderFake>("com.qtjukebox.types", 1, 0, "FontLoader");
#endif

    m_context->setContextProperty("musicJukebox", this);
    m_context->setContextProperty("partyDiscover", m_partyDiscover);
    m_context->setContextProperty("partyModel", m_partyDiscover->partyModel());
    m_context->setContextProperty("catalogModel", m_catalogController->catalogModel());
    m_context->setContextProperty("rankedModel", m_catalogController->rankedListModel());
    m_context->setContextProperty("importPathsModel", m_importPathsModel);
    m_context->setContextProperty("twitterClient", m_twitterClient);

#ifdef JUKEBOX_DEBUG
    m_view->setSource(Utils::qmlPath().absoluteFilePath() + "/main.qml");
#else
    m_view->setSource(QUrl("qrc:/qml/main.qml"));
#endif

    switch (m_behavior) {
    case JukeboxGuestMode:
        qDebug() << "running as Guest jukebox mode";
        startGuestMode();
        break;
    default:
        qWarning() << "unknown jukebox mode" << m_behavior;
        break;
    }
}

void Jukebox::resizeEvent(QResizeEvent *event)
{
    m_appHeight = event->size().height();
    m_appWidth = event->size().width();
    m_contentHeight = 3 * event->size().height();

    emit appHeightChanged();
    emit appWidthChanged();
    emit contentHeightChanged();
}

Jukebox::~Jukebox()
{
    delete m_mediaPlayer;
    delete m_server;
}

void Jukebox::setPlatform(Platform platform)
{
    m_platform = platform;
    emit platformChanged();
}

void Jukebox::setBehavior(Behavior behavior)
{
    if (m_behavior != behavior) {
        m_behavior = behavior;
        emit behaviorChanged();
        emit catalogEmptyChanged();
        switch (m_behavior) {
        case JukeboxDjMode:
            qDebug() << "running as DJ jukebox mode";
            if (startDJMode())
                m_partyAnnounce->start(m_party);
            break;
        case JukeboxClientMode:
            qDebug() << "running as Client jukebox mode";
            startClientView();
            break;
        default:
            qWarning() << "unknown jukebox mode" << m_behavior;
            break;
        }
    }
}

void Jukebox::onRankedListRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)
    Q_UNUSED(first)
    Q_UNUSED(last)
    m_nowPlayingSong = m_catalogController->rankedListModel()->get(0);
    emit nowPlayingSongChanged();
}

MusicItem* Jukebox::nowPlayingSong() const
{
    return m_nowPlayingSong.data();
}

void Jukebox::playPauseSong(bool playing)
{
    if (!m_playerPlaying) {
        if (playing) {
            disconnect(m_catalogController->rankedListModel(), SIGNAL(rowsInserted(QModelIndex, int, int)),
                       this, SLOT(onRankedListRowsInserted(QModelIndex, int, int)));

            if (m_mediaPlayer->state() == QMediaPlayer::PausedState) {
                if (m_playingMode == Jukebox::AutoPlay)
                    m_mediaPlayer->play();
            } else {
                nextSong();
            }
            m_playerPlaying = true;
        }
    } else {
        m_mediaPlayer->pause();
        m_playerPlaying = false;
    }
}

void Jukebox::stopSong() {
    m_playerPlaying = false;
    if (m_playingMode == Jukebox::AutoPlay)
        m_mediaPlayer->stop();
}

void Jukebox::nextSong()
{
    m_nowPlayingSong = m_catalogController->rankedListModel()->get(0);
    emit nowPlayingSongChanged();
    emit nowPlaying(m_nowPlayingSong->songId());

    ListOfMusicItems list;
    if (m_nowPlayingSong->votesCount() > 0) {
        m_nowPlayingSong->setVotesCount(-1);
    } else {
        m_nowPlayingSong->setVotesCount(m_nowPlayingSong->votesCount() - 1);
    }
    list.append(m_nowPlayingSong);
    m_catalogController->updateItemsInCatalog(list);

    if (m_playingMode == Jukebox::AutoPlay) {
        m_mediaPlayer->setMedia(QMediaContent(QUrl::fromUserInput(m_nowPlayingSong->songUrl().path())));
        m_mediaPlayer->play();
    }
    m_playerPlaying = true;
}

QString Jukebox::dataDir() const
{
    return Global::dataDir().absoluteFilePath();
}

CatalogController* Jukebox::catalogController()
{
    return m_catalogController;
}

PartyItem* Jukebox::party() const
{
    return m_party.data();
}

void Jukebox::setPartyType(const PartyType type)
{
    if (type != m_partyType) {
        m_partyType = type;
        emit partyTypeChanged();
    }
}

bool Jukebox::isConnected() const
{
    return (m_behavior == JukeboxDjMode)
           || (m_clientSocket
               && (m_clientSocket->state() == QAbstractSocket::ConnectedState));
}

bool Jukebox::startClientView()
{
    m_partyDiscover->start();
    return true;
}

void Jukebox::searchParties()
{
    qDebug() << __func__;
    m_partyDiscover->start();
}

QString Jukebox::localPartyName()
{
    return m_settings->value("PartyName").toString();
}

void Jukebox::setLocalPartyName(const QString &name)
{
    if (name != localPartyName()) {
        m_settings->setValue("PartyName", name);
        emit localPartyNameChanged();
    }
}

QString Jukebox::catalogFilename()
{
    return m_settings->value("CatalogFilename").toString();
}

void Jukebox::setCatalogFilename(const QString &name)
{
    if (name != catalogFilename()) {
        m_settings->setValue("CatalogFilename", name);
        emit catalogFilenameChanged();
    }
}

void Jukebox::createParty(const QString &name)
{
    m_party->setName(name);
    setLocalPartyName(name);

    if (m_partyType == PublicParty) {
        connect(m_twitterClient, SIGNAL(newTrackedTweet(QVariant)),
                this, SLOT(onNewTrackedTweet(QVariant)));

        m_twitterClient->track(m_party->hashtag());
    }
}

bool Jukebox::isAlreadyVoted(const QString &id)
{
    return m_votedSongs.contains(id);
}

bool Jukebox::startGuestMode()
{
    setStyleSheet("background-color: black;");
    showFullScreen();
    emit appWidthChanged();
    emit appHeightChanged();

    m_partyDiscover->start();

    return true;
}

bool Jukebox::startDJMode()
{
    connect(m_catalogController->rankedListModel(), SIGNAL(rowsInserted(QModelIndex, int, int)),
            this, SLOT(onRankedListRowsInserted(QModelIndex, int, int)));

    m_server = new Server(LISTENING_PORT, this);

    connect(m_server, SIGNAL(vote(QString)),
            this, SLOT(onClientVote(QString)));

    return m_server->isListening();
}

bool Jukebox::sendVote(const QString &id)
{
    if (m_behavior == JukeboxDjMode) {
        onClientVote(id);
    } else {
        if (!m_clientSocket
                || m_clientSocket->state() != QAbstractSocket::ConnectedState) {
            emit showMessageDialog("Lost connection",
                                   "You can't vote now. Please wait for reconnection");
            return false;
        }

        m_clientSocket->sendVote(id);
        m_votedSongs.append(id);
    }

    QSharedPointer<MusicItem> item = m_catalogController->musicItemsModel()->itemById(id);
    if (item) {
        emit showMessageDialog("Successfully voted!",
                               "<br>Your vote was computed for " + item->songTitle()
                               + " by " + item->artistName() + ".");
    }

    return true;
}

bool Jukebox::removeMusic(const QString &id)
{
    if (id.isEmpty())
        return false;

    QSharedPointer<MusicItem> item = m_catalogController->itemById(id.toUpper());

    if (!item)
        return false;

    ListOfMusicItems list;
    list.append(item);
    m_catalogController->deleteItemsInCatalog(list);

    return true;
}

void Jukebox::loadCatalog(const QStringList &paths)
{
    m_catalogController->loadCatalog(paths);
}

bool Jukebox::connectClientSocket(const QString &uuid)
{
    QSharedPointer<PartyItem> party = m_partyDiscover->partyModel()->searchParty(uuid);
    if (party) {
        m_party = party;
        emit partyChanged();
        return connectClientSocket(party->address(), party->port());
    }

    return false;
}

bool Jukebox::connectClientSocket(const QHostAddress &address, quint16 port)
{
    if (m_clientSocket
            && (m_clientSocket->state() == QTcpSocket::ConnectedState)) {
        qDebug() << "client already connected";
        return false;
    }

    qDebug() << "trying to connect to " << address << port;
    delete m_clientSocket;

    m_djAddress = address;
    m_djPort = port;

    m_clientSocket = new ClientSocket(this);
    connect(m_clientSocket, SIGNAL(catalogAddedItems(ListOfMusicItems)),
            m_catalogController, SLOT(addItemsInCatalog(ListOfMusicItems)));
    connect(m_clientSocket, SIGNAL(catalogDeletedItems(ListOfMusicItems)),
            m_catalogController, SLOT(deleteItemsInCatalog(ListOfMusicItems)));
    connect(m_clientSocket, SIGNAL(catalogUpdatedItems(ListOfMusicItems)),
            m_catalogController, SLOT(updateItemsInCatalog(ListOfMusicItems)));
    connect(m_clientSocket, SIGNAL(catalogListItems(ListOfMusicItems)),
            m_catalogController, SLOT(setItemsInCatalog(ListOfMusicItems)));
    connect(m_clientSocket, SIGNAL(nowPlaying(QString)),
            this, SLOT(onNowPlaying(QString)));
    connect(m_clientSocket, SIGNAL(connected()),
            this, SLOT(onClientSocketConnected()));
    connect(m_clientSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(onConnectionError(QAbstractSocket::SocketError)));
    connect(m_clientSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            this, SIGNAL(connectedChanged()));
    m_clientSocket->connectToHost(address, port);

    return true;
}

bool Jukebox::connectClientSocket(const QString &address, quint16 port)
{
    return connectClientSocket(QHostAddress(address), port);
}

void Jukebox::sortCatalogBySongName()
{
    m_catalogController->sortCatalogBySongName();
}

void Jukebox::sortCatalogByArtistName()
{
    m_catalogController->sortCatalogByArtistName();
}

void Jukebox::onNewTrackedTweet(const QVariant &var)
{
    const QVariantMap map = var.toMap();
    const QString text = map["text"].toString();
    const QStringList list = text.split(' ', QString::SkipEmptyParts);

    if (m_behavior == JukeboxDjMode) {
        QString id;

        QRegExp rx("#?[a-zA-Z][0-9][a-zA-Z][0-9]");
        QList<QString>::const_iterator i;
        for (i = list.begin(); i != list.end(); ++i) {
            if (rx.indexIn(*i) >= 0) {
                id = *i;
                break;
            }
        }

        if (id.startsWith("#")) {
            id.remove(0, 1);
        }

        if (id.length() > 0)
            onClientVote(id);
    }
}

void Jukebox::onClientSocketConnected()
{
    ClientSocket *sock = qobject_cast<ClientSocket*>(sender());
    if (sock) {
        qDebug() << "connected!";
        sock->requestCatalog();
    }
}

bool Jukebox::importCatalog()
{
    return m_behavior == JukeboxDjMode;
}

bool Jukebox::catalogEmpty()
{
    return m_catalogController->isCatalogEmpty();
}

int Jukebox::catalogCount()
{
    return m_catalogController->modelCount();
}

QString Jukebox::catalogImportDir() const
{
    return QUrl::fromLocalFile(QDir::homePath()).toString();
}

void Jukebox::onNowPlaying(const QString &id)
{
    m_votedSongs.removeAll(id);
    m_nowPlayingSong = m_catalogController->itemById(id);
    emit nowPlayingSongChanged();
}

void Jukebox::onClientVote(const QString &id)
{
    if (!id.isEmpty()) {
        QSharedPointer<MusicItem> item = m_catalogController->itemById(id.toUpper());

        if (item) {
            qDebug() << "vote to" << id;
            ListOfMusicItems list;
            item->setVotesCount(item->votesCount() + 1);
            list.append(item);
            m_catalogController->updateItemsInCatalog(list);
        }
    }
}

void Jukebox::onConnectionError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError)
    m_clientSocket->close();
    // try reconnect after 5 seconds
    QTimer::singleShot(5000, this, SLOT(tryReconnectClient()));
}

void Jukebox::onMediaStateChanged(QMediaPlayer::State state)
{
    if (state == QMediaPlayer::StoppedState && m_playerPlaying)
        emit songEnded();
}

void Jukebox::refreshCatalog()
{
    if (m_behavior == JukeboxDjMode)
        m_catalogController->initDjMode();
    else
        m_catalogController->initClientMode();

    emit catalogEmptyChanged();
}

void Jukebox::tryReconnectClient()
{
    connectClientSocket(m_djAddress, m_djPort);
}
