#include "filepicker.h"

#include <QtCore/QDir>
#include <QtCore/QDirIterator>

FilePicker::FilePicker(QObject *parent)
    : QObject(parent)
{
    m_songFilters << "*.mp3";
}

// TODO Make it assynchronous
QStringList FilePicker::songListFromDir(const QString &dir, bool recursive)
{
    QDirIterator::IteratorFlags flags =
        recursive ? QDirIterator::Subdirectories : QDirIterator::NoIteratorFlags;

    m_songList.clear();

    QDirIterator it(dir, m_songFilters, QDir::Files, flags);
    while (it.hasNext())
        m_songList << it.next();

    return m_songList;
}
