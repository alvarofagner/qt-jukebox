#ifndef FILEPICKER_H
#define FILEPICKER_H

#include <QtCore/QObject>
#include <QtCore/QStringList>

class FilePicker : public QObject
{
    Q_OBJECT

public:
    FilePicker(QObject *parent = 0);

    void setSongFilters(const QStringList &songFilters);

    QStringList songListFromDir(const QString &dir, bool recursive = true);

private:
    QStringList m_songFilters;
    QStringList m_songList;
};

#endif
