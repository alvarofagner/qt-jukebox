#include "partymodel.h"
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>


PartyModel::PartyModel(QObject *parent)
    : QAbstractListModel(parent)
{
    const PartyItem item;
    QHash<int, QByteArray> roles;
    const int propertyCount = item.metaObject()->propertyCount();
    QMetaProperty metaProperty;

    for (int i = 0; i < propertyCount; ++i) {
        metaProperty = item.metaObject()->property(i);
        roles[Qt::UserRole + i] = metaProperty.name();
    }

    setRoleNames(roles);
}

void PartyModel::addItem(const QSharedPointer<PartyItem> &item)
{
    // find any duplicates
    QList<QSharedPointer<PartyItem> >::const_iterator i;
    for (i = m_items.begin(); i != m_items.end(); ++i)
        if ((*i)->uuid() == item->uuid())
            return;

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_items.append(item);
    endInsertRows();
}

void PartyModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    m_items.clear();
    endRemoveRows();
}

int PartyModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_items.count();
}

QVariant PartyModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_items.count())
        return QVariant();

    QVariant dataValue;
    const QSharedPointer<PartyItem> item = m_items[index.row()];
    const int propertyCount = item->metaObject()->propertyCount();
    QMetaProperty metaProperty;

    for (int propertyIndex = 0; propertyIndex < propertyCount; ++propertyIndex) {
        if ((role - Qt::UserRole) == propertyIndex) {
            metaProperty = item->metaObject()->property(propertyIndex);
            dataValue = item->property(metaProperty.name());
            break;
        }
    }

    return dataValue;
}

QSharedPointer<PartyItem> PartyModel::searchParty(const QString &uuid) const
{
    QList<QSharedPointer<PartyItem> >::const_iterator i;
    for (i = m_items.begin(); i != m_items.end(); ++i)
        if ((*i)->uuid() == uuid)
            return *i;

    return QSharedPointer<PartyItem>();
}
