/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include "musicitem.h"
#include <QtNetwork/QTcpSocket>

class ClientSocket : public QTcpSocket
{
    Q_OBJECT
public:
    ClientSocket(QObject *parent = 0);

signals:
    void catalogAddedItems(ListOfMusicItems items);
    void catalogDeletedItems(ListOfMusicItems items);
    void catalogUpdatedItems(ListOfMusicItems items);
    void catalogListItems(ListOfMusicItems items);
    void nowPlaying(const QString &id);

public slots:
    void requestCatalog();
    void sendVote(const QString &id);

private slots:
    void onReadyRead();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError error);
};

#endif // CLIENTSOCKET_H
