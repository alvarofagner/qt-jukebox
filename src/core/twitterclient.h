/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef TWITTERCLIENT_H
#define TWITTERCLIENT_H

#include <QtCore/QObject>
#include <QtCore/QVariant>

#include "kqoauthmanager.h"

class QWebView;
class QSettings;
class QTimer;

class TwitterClient : public QObject
{
    Q_OBJECT

    Q_ENUMS(CredentialsStatus)
    Q_PROPERTY(QString username READ username NOTIFY usernameChanged)
    Q_PROPERTY(CredentialsStatus credentialsStatus READ credentialsStatus NOTIFY credentialsStatusChanged)

public:
    enum CredentialsStatus {
        CredentialsNotAvailable,
        CredentialsValid,
        CredentialsInvalid
    };

    TwitterClient(QObject *parent = 0);
    ~TwitterClient();

    Q_INVOKABLE bool isAuthenticated();
    Q_INVOKABLE void auth();
    void sendTweet(const QString &tweet);
    void removeTweet(const QString &id);
    Q_INVOKABLE void verifyCredentials();
    void track(const QString &keyword, const QString &followIDs = QString());
    void getUserInfo(const QString &userName);
    void getUserTimeline(const QString &userName = QString());

    CredentialsStatus credentialsStatus() { return m_credentialsStatus; }
    QString username() const;


signals:
    void error(const KQOAuthManager::KQOAuthError &error);
    void errorAuthenticating(const QString &reason, const QString &description);
    void requestDone();
    void authUrlReady(const QUrl &url);
    void newTrackedTweet(const QVariant &var);
    void userInfo(const QVariant &var);
    void userTimeline(const QVariant &var);
    void usernameChanged();
    void authenticatedChanged();
    void credentialsStatusChanged();

private slots:
    void onTemporaryTokenReceived(QString temporaryToken, QString temporaryTokenSecret);
    void onAuthorizationReceived(QString token, QString verifier);
    void onAccessTokenReceived(QString token, QString tokenSecret);
    void onAuthorizedRequestReady(QByteArray response, int id);
    void onRequestReady(QByteArray response);
    void onAuthorizedRequestDone();

private:
    enum RequestTypeID {
        GetUserInfoID = 10,
        TrackTweetID = 20,
        GetStatusTimelineID = 30,
        VerifyCredentials = 40
    };

    void initRequest(KQOAuthRequest::RequestType requestType, const QUrl &url,
                     KQOAuthRequest *request = 0);
    QString oAuthToken() const;
    QString oAuthTokenSecret() const;
    void processCredentials(const QVariant &var);

    KQOAuthManager *m_oauthManager;
    KQOAuthRequest *m_oauthRequest;
    QSettings *m_settings;
    QString m_screenName;
    CredentialsStatus m_credentialsStatus;

    static const QString s_requestTokenUrl;
    static const QString s_userAuthorizeUrl;
    static const QString s_userAccessTokenUrl;
    static const QString s_usersShowUrl;
    static const QString s_statusTimelineUrl;
    static const QString s_statusUpdateUrl;
    static const QString s_statusDestroyUrl;
    static const QString s_streamTrackUrl;
    static const QString s_verifyCredentials;

    static const QString s_consumerKey;
    static const QString s_consumerSecretKey;
};

#endif // TWITTERCLIENT_H
