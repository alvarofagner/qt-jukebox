/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef NETWORK_H
#define NETWORK_H

#include "networkrequest.h"

#include <QObject>
#include <QVariant>
#include <QNetworkReply>
#include <QQueue>

class QNetworkAccessManager;
class QNetworkReply;

class Network : public QObject
{
    Q_OBJECT

public:
    enum Error {
        UnknownError
    };

    Network(QObject *parent = 0);
    ~Network();

signals:
    void error(NetworkRequest::RequestType type, Network::Error code);

private:
    int doGet(const QString &url, NetworkRequest::RequestType type);
    int doPost(const QString &url, NetworkRequest::RequestType type);
    int doHead(const QString &url, NetworkRequest::RequestType type);

private slots:
    void onReplyFinished(QNetworkReply *reply);

private:
    NetworkRequest *createRequest(const QString &url, NetworkRequest::RequestType type);
    void doFetchMusicProfile();
    void doFetchMusicMatch();

    void handleReplyError(QNetworkReply *reply, NetworkRequest *request);
    void handleAuthenticate(QNetworkReply *reply);

    QNetworkAccessManager *m_networkManager;
    QHash<QNetworkReply*, NetworkRequest*> m_requests;
    Error m_error;
};

#endif // NETWORK_H
