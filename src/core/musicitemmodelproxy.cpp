/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "musicitemmodelproxy.h"
#include "musicitem.h"
#include <QtCore/QDebug>


MusicItemModelProxy::MusicItemModelProxy(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_itemsByPage(0)
    , m_page(0)
{
}

MusicItemModelProxy::MusicItemModelProxy(const MusicItemModelProxy &other)
    : QSortFilterProxyModel(0)
    , m_itemsByPage(other.m_itemsByPage)
    , m_page(other.m_page)
{
}

QSharedPointer<MusicItem> MusicItemModelProxy::get(int idx)
{
    QSharedPointer<MusicItem> item(new MusicItem());

    const QStringList props = item->objectDBProperties();
    for (int i = 0; i < props.count(); i++) {
        item->setProperty(props[i].toAscii(), data(index(idx, 0), Qt::UserRole + i));
    }

    return item;
}

QVariant MusicItemModelProxy::getDataByIndex(int idx, int column)
{
    return data(index(idx, column));
}

bool MusicItemModelProxy::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const QVariant leftData = sourceModel()->data(sourceModel()->index(left.row(), 0),
                                                  left.column() + Qt::UserRole);
    const QVariant rightData = sourceModel()->data(sourceModel()->index(right.row(), 0),
                                                   right.column() + Qt::UserRole);

    if (leftData == rightData) {
        switch (left.column()) {
        case 1:
            // same album title? lets sort by song ID
            return lessThan(sourceModel()->index(left.row(), 0),
                            sourceModel()->index(right.row(), 0));
        case 2:
            // same artist name? lets sort by album name
            return lessThan(sourceModel()->index(left.row(), 3),
                            sourceModel()->index(right.row(), 3));
        case 3:
            // same album name? lets sort by song title
            return lessThan(sourceModel()->index(left.row(), 1),
                            sourceModel()->index(right.row(), 1));
        case 5:
            // same votes count? lets sort by song ID
            return lessThan(sourceModel()->index(right.row(), 0),
                            sourceModel()->index(left.row(), 0));
        }
    }

    switch (leftData.type()) {
    case QVariant::ULongLong:
        return leftData.toULongLong() < rightData.toULongLong();
    case QVariant::LongLong:
        return leftData.toLongLong() < rightData.toLongLong();
    case QVariant::String:
        return leftData.toString() < rightData.toString();
    default:
        return left.row() < right.row();
    }
}

bool MusicItemModelProxy::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    Q_UNUSED(sourceParent)

    if (itemOnPage(sourceRow, m_page))
        return true;

    return false;
}

bool MusicItemModelProxy::itemOnPage(int sourceRow, int page) const
{
    if (m_itemsByPage > 0) {
        int lowerBound = page * m_itemsByPage;
        int upperBound = (page + 1) * m_itemsByPage;

        if ((sourceRow < lowerBound) || (sourceRow >= upperBound))
            return false;
    }

    return true;
}
