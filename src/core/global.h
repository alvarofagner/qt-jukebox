/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#define XSTR(s) STR(s)
#define STR(s) #s

#include <QFileInfo>
#include <QObject>

class Global : public QObject
{
    Q_OBJECT

public:
    static QFileInfo dataDir();
    static QFileInfo qmlDir();
};

#endif
