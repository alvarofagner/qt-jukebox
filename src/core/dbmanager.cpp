/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "dbmanager.h"
#include "musicitem.h"

#include <QtCore/QDir>
#include <QtCore/QStringBuilder>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include <QtGui/QDesktopServices>
#include <QtCore/QDebug>

DBManager::DBManager(const DBManager &other)
    : QObject()
    , m_db(QSqlDatabase::addDatabase(QLatin1String("QSQLITE")))
    , m_tableModel(0)
{
    Q_UNUSED(other)
}

DBManager::DBManager(QObject *parent)
    : QObject(parent)
    , m_db(QSqlDatabase::addDatabase(QLatin1String("QSQLITE")))
    , m_tableModel(0)
    , m_tableName(QLatin1String("songsClient"))
{
}

void DBManager::refreshClientMode()
{
    m_tableName = QLatin1String("songsClient");
    start();
}

void DBManager::refreshDjMode()
{
    m_tableName = QLatin1String("songsDj");
    start();
}

void DBManager::start()
{
#ifdef Q_OS_SYMBIAN
    const QDir databaseDir("E:\\Jukebox\\");
#else
    const QDir databaseDir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#endif

    if (!databaseDir.exists()) {
        if (!databaseDir.mkpath(databaseDir.path())) {
            qCritical() << "[DBManager]"
                        << "Could not create database directory"
                        << databaseDir.path();
            return;
        }
    }

#ifdef Q_OS_SYMBIAN
    m_db.setDatabaseName("E:\\Jukebox\\jukebox.db");
#else
    m_db.setDatabaseName(databaseDir.path() % QLatin1String("/jukebox.db"));
#endif

    if (!m_db.open()) {
        qCritical() << "[DBManager]"
                    << "Could not open database:"
                    << m_db.lastError().text();
        return;
    }

    deleteClientSongsTable();
    createSongsTable();
    m_tableModel = new MusicItemModel(this, m_db, m_tableName);
}

bool DBManager::createSongsTable()
{
    QSqlQuery query(m_db);

    return query.exec("create table if not exists " + m_tableName +
                      " (SongId varchar primary key not null,"
                      " SongTitle varchar,"
                      " ArtistName varchar,"
                      " AlbumName varchar,"
                      " SongUrl varchar,"
                      " VotesCount integer default 0)");
}

bool DBManager::deleteClientSongsTable()
{
    QSqlQuery query(m_db);

    return query.exec("drop table songsClient if exists");
}
