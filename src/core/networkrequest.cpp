/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "networkrequest.h"

static const int serverTimeout = 60000;

int getNextRequestId()
{
    static int value = 0;
    return ++value;
}


NetworkRequest::NetworkRequest(const QUrl &url, RequestType type, QObject *parent)
    : QObject(parent)
    , m_id(getNextRequestId())
    , m_type(type)
    , m_networkRequest(url)
{
    m_timeoutTimer.setInterval(serverTimeout);
    connect(&m_timeoutTimer, SIGNAL(timeout()),
            this, SLOT(onTimeout()));
}

NetworkRequest::~NetworkRequest()
{
    m_timeoutTimer.stop();
}

int NetworkRequest::id() const
{
    return m_id;
}
QUrl NetworkRequest::url() const
{
    return m_networkRequest.url();
}

void NetworkRequest::setUrl(const QUrl &url)
{
    m_networkRequest.setUrl(url);
}

NetworkRequest::RequestType NetworkRequest::type() const
{
    return m_type;
}

void NetworkRequest::setRawHeader(const QByteArray &key, const QByteArray &value)
{
    m_networkRequest.setRawHeader(key, value);
}

QNetworkRequest NetworkRequest::networkRequest() const
{
    return m_networkRequest;
}

void NetworkRequest::startTimeoutTimer()
{
    m_timeoutTimer.start();
}

void NetworkRequest::stopTimeoutTimer()
{
    m_timeoutTimer.stop();
}

void NetworkRequest::onTimeout()
{
    m_timeoutTimer.stop();

    emit timeout(this);
}
