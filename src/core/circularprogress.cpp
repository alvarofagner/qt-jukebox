/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "circularprogress.h"

#include <QtGui/QPainter>

CircularProgress::CircularProgress(QDeclarativeItem *parent)
    : QDeclarativeItem(parent)
    , m_color(QColor(0, 0, 0))
    , m_progress(0.0)
    , m_barWidth(10)
    , m_clockWise(false)
{
    setFlag(QGraphicsItem::ItemHasNoContents, false);
}

void CircularProgress::setColor(const QColor &color)
{
    if (m_color == color)
        return;

    m_color = color;

    emit colorChanged();

    update();
}

void CircularProgress::setBarWidth(int barWidth)
{
    if (m_barWidth == barWidth)
        return;

    m_barWidth = barWidth;

    emit barWidthChanged();

    update();
}

void CircularProgress::setProgress(qreal progress)
{
    if (m_progress == progress)
        return;

    m_progress = progress;

    emit progressChanged();

    update();
}

void CircularProgress::setClockWise(bool clockWise)
{
    if (m_clockWise == clockWise)
        return;

    m_clockWise = clockWise;

    emit clockWiseChanged();
}

void CircularProgress::setStartPosition(StartPosition startPosition)
{
    if (m_startPosition == startPosition)
        return;

    m_startPosition = startPosition;

    emit startPositionChanged();
}

int CircularProgress::startPositionAngle() const
{
    switch (m_startPosition) {
    case Up:
        return 90;
    case Left:
        return 180;
    case Down:
        return 270;
    case Right:
        return 360;
    default:
        return 90;
    }
}

void CircularProgress::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen(m_color, m_barWidth);
    pen.setCapStyle(Qt::RoundCap);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing);

    QRectF bounding = boundingRect();
    QRectF paintRect(bounding.left() + m_barWidth / 2, bounding.top() + m_barWidth / 2,
                     bounding.width() - m_barWidth, bounding.height() - m_barWidth);

    int clockWiseFactor = m_clockWise ? -1 : 1;

    painter->drawArc(paintRect, startPositionAngle() * 16, clockWiseFactor * m_progress * 360 * 16);
}
