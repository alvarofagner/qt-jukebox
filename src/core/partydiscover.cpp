#include "partydiscover.h"

#include <QtCore/QTimer>
#include <QtNetwork/QUdpSocket>
#include <QtCore/QDebug>

#define UDP_PORT 12345
#define APP_NAME "Qt-Jukebox"

#if QT_VERSION >= 0x040800
const QHostAddress PartyDiscover::s_groupAddress = QHostAddress("239.255.43.21");
#endif

PartyDiscover::PartyDiscover(QObject *parent)
    : QObject(parent)
    , m_socket()
    , m_partyModel(new PartyModel(this))
    , m_timer(new QTimer(this))
{
    connect(this, SIGNAL(partyFound(QSharedPointer<PartyItem>)),
            m_partyModel, SLOT(addItem(QSharedPointer<PartyItem>)));
    connect(m_timer, SIGNAL(timeout()), this, SLOT(stop()));
}

PartyModel *PartyDiscover::partyModel() const
{
    return m_partyModel;
}

void PartyDiscover::start()
{
    m_partyModel->clear();
    m_socket = new QUdpSocket(this);
    m_socket->bind(UDP_PORT, QUdpSocket::ShareAddress);
#if QT_VERSION >= 0x040800
    m_socket->joinMulticastGroup(s_groupAddress);
#endif
    connect(m_socket, SIGNAL(readyRead()),
            this, SLOT(processDatagrams()));
    m_timer->start(10000);
}

void PartyDiscover::stop()
{
    disconnect(m_socket, SIGNAL(readyRead()),
               this, SLOT(processDatagrams()));
    m_socket->close();
    emit timeout();
}

void PartyDiscover::processDatagrams()
{
    QUdpSocket *socket = qobject_cast<QUdpSocket *>(sender());
    if (!socket)
        return;

    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        QHostAddress host;

        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &host);

        QVariant message;
        QDataStream dataStream(&datagram, QIODevice::ReadOnly);
        dataStream >> message;

        const QVariantMap map = message.toMap();
        if (map["app"].toString() == APP_NAME) {
            const QString action = map["action"].toString();
            if (action == "partyAnnounce") {
                // a new party found
                QSharedPointer<PartyItem> party(new PartyItem);
                party->setUuid(map["partyUuid"].toString());
                party->setName(map["partyName"].toString());
                party->setHashtag(map["partyHashtag"].toString());
                party->setAddress(host.toString());
                emit partyFound(party);
            }
        }
    }
}
