/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef MUSICITEM_H
#define MUSICITEM_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QUrl>
#include <QtCore/QVariant>


class MusicItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString songId READ songId WRITE setSongId NOTIFY songIdChanged)
    Q_PROPERTY(QString songTitle READ songTitle WRITE setSongTitle NOTIFY songTitleChanged)
    Q_PROPERTY(QString artistName READ artistName WRITE setArtistName NOTIFY artistNameChanged)
    Q_PROPERTY(QString albumName READ albumName WRITE setAlbumName NOTIFY albumNameChanged)
    Q_PROPERTY(QUrl songUrl READ songUrl WRITE setSongUrl NOTIFY songUrlChanged)
    Q_PROPERTY(int votesCount READ votesCount WRITE setVotesCount NOTIFY votesCountChanged)

public:

    MusicItem(QObject *parent = 0);
    ~MusicItem();

    QStringList objectDBProperties() const;

    QString songId() const;
    void setSongId(const QString &songId);

    QString songTitle() const;
    void setSongTitle(const QString &songTitle);

    QString artistName() const;
    void setArtistName(const QString &artistName);

    QString albumName() const;
    void setAlbumName(const QString &albumName);

    QUrl songUrl() const;
    void setSongUrl(const QUrl &songUrl);

    int votesCount() const;
    void setVotesCount(int votesCount);

signals:
    void songIdChanged();
    void songTitleChanged();
    void artistNameChanged();
    void albumNameChanged();
    void songUrlChanged();
    void votesCountChanged();

private:
    QString m_songId;
    QString m_songTitle;
    QString m_artistName;
    QString m_albumName;
    QUrl m_songUrl;
    int m_votesCount;
};

typedef QList<QSharedPointer<MusicItem> > ListOfMusicItems;

ListOfMusicItems variantToListOfMusicItems(const QVariant &var);
QVariant listOfMusicItemsToVariant(const ListOfMusicItems &items);

#endif // MUSICITEM_H
