/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "clientsocket.h"

#ifdef USE_3RDPARTY
#include <parser.h>
#include <serializer.h>
#else
#include <qjson/parser.h>
#include <qjson/serializer.h>
#endif

#include <QtCore/QDebug>
#include <QtCore/QByteArray>
#include <QtCore/QVariant>

ClientSocket::ClientSocket(QObject *parent)
    : QTcpSocket(parent)
{
    connect(this, SIGNAL(readyRead()),
            this, SLOT(onReadyRead()));

    connect(this, SIGNAL(disconnected()),
            this, SLOT(onDisconnected()));

    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(onError(QAbstractSocket::SocketError)));
}

void ClientSocket::onDisconnected()
{
    qDebug() << "[ClientSocket] Client disconnected!";
}

void ClientSocket::onError(QAbstractSocket::SocketError error)
{
    qDebug() << "[ClientSocket] Error: " << error;
}

void ClientSocket::requestCatalog()
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "list";

    write(serializer.serialize(map) + "\r\n");
}

void ClientSocket::sendVote(const QString &id)
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "vote";
    map["id"] = id;

    write(serializer.serialize(map) + "\r\n");
}

void ClientSocket::onReadyRead()
{
    QByteArray line;
    QVariant var;
    QJson::Parser parser;
    bool parserOk;

    while (canReadLine()) {
        line = readLine();
        parserOk = true;
        var = parser.parse(line, &parserOk);

        if (!parserOk) {
            qWarning() << "json error at"
                       << parser.errorLine()
                       << parser.errorString();
        } else {
            QVariantMap map = var.toMap();
            QString what = map["what"].toString();
            if (what == "catalog") {
                QString action = map["action"].toString();
                //qDebug() << map["items"];
                if (action == "add") {
                    emit catalogAddedItems(variantToListOfMusicItems(map["items"]));
                } else if (action == "delete") {
                    emit catalogDeletedItems(variantToListOfMusicItems(map["items"]));
                } else if (action == "update") {
                    emit catalogUpdatedItems(variantToListOfMusicItems(map["items"]));
                } else if (action == "list") {
                    emit catalogListItems(variantToListOfMusicItems(map["items"]));
                }
                return;
            }
            if (what == "player") {
                QString action = map["action"].toString();
                if (action == "nowPlaying") {
                    emit nowPlaying(map["items"].toString());
                }
            }

        }
    }
}
