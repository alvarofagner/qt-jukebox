/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef CIRCULARPROGRESS_H
#define CIRCULARPROGRESS_H

#include <QDeclarativeItem>
#include <QColor>

class CircularProgress : public QDeclarativeItem
{
    Q_OBJECT

    Q_ENUMS(StartPosition)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(qreal progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(int barWidth READ barWidth WRITE setBarWidth NOTIFY barWidthChanged)
    Q_PROPERTY(bool clockWise READ clockWise WRITE setClockWise NOTIFY clockWiseChanged)
    Q_PROPERTY(StartPosition startPosition READ startPosition WRITE setStartPosition NOTIFY startPositionChanged)

public:
    enum StartPosition {
        Up,
        Down,
        Left,
        Right
    };

    CircularProgress(QDeclarativeItem *parent = 0);

    QColor color() const { return m_color; }
    void setColor(const QColor &color);

    qreal progress() const { return m_progress; }
    void setProgress(qreal progress);

    int barWidth() const { return m_barWidth; }
    void setBarWidth(int barWidth);

    int clockWise() const { return m_clockWise; }
    void setClockWise(bool clockWise);

    StartPosition startPosition() const { return m_startPosition; }
    void setStartPosition(StartPosition startPosition);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void colorChanged();
    void progressChanged();
    void barWidthChanged();
    void clockWiseChanged();
    void startPositionChanged();

private:
    int startPositionAngle() const;

    QColor m_color;
    qreal m_progress;
    int m_barWidth;
    bool m_clockWise;
    StartPosition m_startPosition;
};

#endif

