/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "utils.h"

#include <QtCore/QByteArray>
#include <QtCore/QCryptographicHash>
#include <QtCore/QStringList>

#ifdef Q_OS_SYMBIAN
#include <eikenv.h>
#include <coemain.h>
#include <aknappui.h>
#endif


Utils::Utils(QObject *parent) : QObject(parent)
{
}

Utils::~Utils()
{
}

void Utils::setOrientation(const Orientation &arg)
{
#ifdef Q_OS_SYMBIAN
    CAknAppUi *aknAppUi = dynamic_cast<CAknAppUi *>(CEikonEnv::Static()->AppUi());
    if (!aknAppUi)
        return;

    switch (arg) {
    case Landscape:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationLandscape);
        break;
    case Portrait:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationPortrait);
        break;
    case Auto:
        aknAppUi->SetOrientationL(CAknAppUi::EAppUiOrientationAutomatic);
    }
#endif

    return;
}

QFileInfo Utils::qmlPath()
{
    QStringList pathList(QString(XSTR(QML_PATH)).split(":", QString::SkipEmptyParts));

    foreach (const QFileInfo &path, pathList) {
        if (path.exists() && path.isDir())
            return path;
    }

    return QFileInfo();
}

QString Utils::musicItemId(const QByteArray &array) const
{
    bool ok = false;
    QByteArray stringId(QCryptographicHash::hash(array, QCryptographicHash::Md4).toHex());
    stringId.resize(4);
    uint decimalId = stringId.toUInt(&ok, 16);

    if (ok)
        return formattedId(decimalId);
    else
        return QString();
}

QString Utils::formattedId(uint id) const
{
    uint remainder;
    uint quotient;
    uint div = 26;
    QChar letter;
    QString formattedId;

    for (int count = 0; count < 4; count++) {
        remainder = id % div;
        quotient = id / div;

        if (div == 26) {
            letter = remainder + 'A';
            formattedId.append(letter.toAscii());
            div = 10;
        } else {
            formattedId.append(QString::number(remainder));
            div = 26;
        }

        id = quotient;
    }

    return formattedId;
}
