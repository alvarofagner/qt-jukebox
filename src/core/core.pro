include(../../shared.pri)
QT += declarative network sql

# Major.Minor.Build Version
VERSION = 1.0.0
CONFIG += \
    warn_on \
    depend_includepath \
    link_pkgconfig \
    link_prl \
    ordered \
    qdeclarative-boostable

TEMPLATE = app

use-guestmode: TARGET = qt-jukebox-guest
else: TARGET = qt-jukebox

CONFIG += mobility
MOBILITY = multimedia

DEPENDPATH += $$OUT_PWD/../3rdparty/lib/
LIBS += -L$$OUT_PWD/../3rdparty/lib/

use-3rdparty {
    INCLUDEPATH += \
        $$PWD/../3rdparty/qjson/src \
        $$PWD/../3rdparty/kqoauth/src \
        $$PWD/../3rdparty/taglib/src \
        $$PWD/../3rdparty/taglib/src/toolkit

    DEFINES += TAGLIB_NO_CONFIG
} else {
    CONFIG += kqoauth
}

# kqoauth #
symbian: LIBS += -lkqoauth.lib
else: LIBS += -lkqoauth

# qjson #
harmattan: LIBS += -lqjson
else:win32: LIBS += -lqjson
else:symbian: LIBS += -lqjson.lib
else:macx: LIBS += -lqjson
else:unix: PKGCONFIG += QJson

# taglib #
harmattan: LIBS += -ltag
else:win32: LIBS += -ltag1
else:symbian: LIBS += -ltag.lib
else:macx: LIBS += -ltag
else:unix: PKGCONFIG += taglib

RESOURCES = $$PWD/../resources.qrc

harmattan {
    desktopfile.files = $$PWD/../data/qt-jukebox.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile

    icon.files = $$PWD/../data/images/qt-jukebox.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    INSTALLS += icon

    target.path = /opt/qt-jukebox/bin
    INSTALLS += target

} else:symbian {
    ICON = $$PWD/../data/images/qt-jukebox.svg

    LIBS += \
        -lavkon \
        -leikcore \
        -lcone

    DEPLOYMENT.installer_header = 0xA000D7CE
    TARGET.UID3 = 0xE5C40926
    TARGET.CAPABILITY += NetworkServices
    TARGET.EPOCHEAPSIZE = 0x20000 0x8000000
    TARGET.EPOCSTACKSIZE = 0x14000

    symbian-abld|symbian-sbsv2 {
        # ro-section in gui can exceed default allocated space,
        # so move rw-section a little further
        QMAKE_LFLAGS.ARMCC += --rw-base 0x800000
        QMAKE_LFLAGS.GCCE += -Tdata 0xC00000
    }

    addFiles.pkg_postrules += "\"$${OUT_PWD}/../../debian/copyright\" - \"\", FILETEXT, TEXTEXIT"
    DEPLOYMENT += addFiles

    RESOURCES = $$PWD/../resources_symbian.qrc
} else:macx {
    ICON = $$PWD/../data/images/qt-jukebox.icns
} else:win32 {
    RC_FILE += $$PWD/../qt-jukebox.rc
} else:unix {
    script.files = $$PWD/../data/qt-jukebox.sh
    script.path = /usr/bin
    INSTALLS += script

    target.path = /opt/qt-jukebox/bin
    INSTALLS += target
}

SOURCES += \
    $$PWD/catalog.cpp \
    $$PWD/catalogcontroller.cpp \
    $$PWD/catalogmodel.cpp \
    $$PWD/circularprogress.cpp \
    $$PWD/clientsocket.cpp \
    $$PWD/dbmanager.cpp \
    $$PWD/fontloaderfake.cpp \
    $$PWD/global.cpp \
    $$PWD/importpathsmodel.cpp \
    $$PWD/jukebox.cpp \
    $$PWD/main.cpp \
    $$PWD/musicitemmodel.cpp \
    $$PWD/musicitemmodelproxy.cpp \
    $$PWD/network.cpp \
    $$PWD/networkrequest.cpp \
    $$PWD/partyannounce.cpp \
    $$PWD/partydiscover.cpp \
    $$PWD/twitterclient.cpp \
    $$PWD/server.cpp \
    $$PWD/serversocket.cpp \
    $$PWD/musicitem.cpp \
    $$PWD/partyitem.cpp \
    $$PWD/partymodel.cpp \
    $$PWD/utils.cpp \
    $$PWD/filepicker.cpp

HEADERS += \
    $$PWD/catalog.h \
    $$PWD/catalogcontroller.h \
    $$PWD/catalogmodel.h \
    $$PWD/circularprogress.h \
    $$PWD/clientsocket.h \
    $$PWD/dbmanager.h \
    $$PWD/fontloaderfake.h \
    $$PWD/global.h \
    $$PWD/importpathsmodel.h \
    $$PWD/jukebox.h \
    $$PWD/musicitemmodel.h \
    $$PWD/musicitemmodelproxy.h \
    $$PWD/network.h \
    $$PWD/networkrequest.h \
    $$PWD/partyannounce.h \
    $$PWD/partydiscover.h \
    $$PWD/twitterclient.h \
    $$PWD/server.h \
    $$PWD/serversocket.h \
    $$PWD/musicitem.h \
    $$PWD/partyitem.h \
    $$PWD/partymodel.h \
    $$PWD/utils.h \
    $$PWD/filepicker.h
