/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "catalog.h"
#include "musicitem.h"
#include "utils.h"

#include "fileref.h"
#include "tag.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QStringRef>


Catalog::Catalog(QObject *parent)
    : QObject(parent)
    , m_utils(new Utils(this))
{
}

Catalog::~Catalog()
{
}

void Catalog::setPlaylistResource(const QStringList &resources)
{
    m_listItems.clear();

    for (int i = 0; i < resources.count(); ++i) {
        if (resources.at(i).endsWith(".xspf", Qt::CaseInsensitive)) {
            QFile file(resources.at(i));
            if (!file.open(QFile::ReadOnly | QFile::Text)) {
                qCritical() << __func__  << "File cannot be read...";
                return;
            }

            m_xml.setDevice(&file);

            parseXSPF();
        } else if (resources.at(i).endsWith(".mp3", Qt::CaseInsensitive)) {
            addSingleFile(resources.at(i));
        }
    }

    emit allMusicItemsReady();
}

void Catalog::addSingleFile(const QString &path)
{
    QSharedPointer<MusicItem> musicItem = QSharedPointer<MusicItem>(new MusicItem());

    TagLib::FileRef fileRef(path.toAscii().data());
    TagLib::Tag *tag = fileRef.tag();
    if (!fileRef.isNull() && tag) {
        musicItem->setSongTitle(TStringToQString(tag->title()));
        musicItem->setArtistName(TStringToQString(tag->artist()));
        musicItem->setAlbumName(TStringToQString(tag->album()));
    }

    QFileInfo info(path);
    musicItem->setSongUrl(QUrl::fromUserInput(info.absoluteFilePath()));

    if (musicItem->songTitle().isEmpty())
        musicItem->setSongTitle(info.fileName());

    QString key = musicItem->songTitle() + musicItem->artistName() + musicItem->albumName();
    musicItem->setSongId(m_utils->musicItemId(key.toAscii()));

    m_listItems.append(musicItem);

    emit musicItemReady(musicItem);
}

void Catalog::parseXSPF()
{
    QSharedPointer<MusicItem> musicitem;
    QStringRef name;
    QStringRef lastName;
    int depth = 0;

    while (!m_xml.atEnd()) {
        QXmlStreamReader::TokenType token = m_xml.readNext();
        lastName = name;
        name = m_xml.name();

        switch (token) {
        case QXmlStreamReader::StartElement:
            if ((depth == 2)
                    && (name == "track")) {
                musicitem = QSharedPointer<MusicItem>(new MusicItem());
            }
            depth++;
            break;
        case QXmlStreamReader::EndElement:
            if ((depth == 3)
                    && (name == "track")
                    && musicitem) {
                QString key = musicitem->songTitle() + musicitem->artistName() + musicitem->albumName();
                musicitem->setSongId(m_utils->musicItemId(key.toAscii()));
                m_listItems.append(musicitem);
                emit musicItemReady(musicitem);

                musicitem.clear();
            }
            depth--;
            break;
        case QXmlStreamReader::Characters:
            if (!m_xml.isWhitespace()) {
                if ((depth == 4)
                        && musicitem) {
                    const QString text = m_xml.text().toString();
                    if (lastName == "title") {
                        musicitem->setSongTitle(text);
                    } else if (lastName == "location") {
                        musicitem->setSongUrl(text);
                    } else if (lastName == "creator") {
                        musicitem->setArtistName(text);
                    } else if (lastName == "album") {
                        musicitem->setAlbumName(text);
                    }
                }
            }
            break;
        default:
            // no warning please
            break;
        }
    }
}

QList<QSharedPointer<MusicItem> > Catalog::musicItems() const
{
    return m_listItems;
}
