#ifndef IMPORTPATHSMODEL_H
#define IMPORTPATHSMODEL_H

#include <qdeclarative.h>
#include <QStringList>
#include <QUrl>
#include <QAbstractListModel>

class QDeclarativeContext;
class QModelIndex;

class ImportPathsModelPrivate;

class ImportPathsModel : public QAbstractListModel, public QDeclarativeParserStatus
{
    Q_OBJECT

    Q_PROPERTY(QUrl path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QUrl parentPath READ parentPath NOTIFY pathChanged)
    Q_PROPERTY(QStringList stringFilters READ stringFilters WRITE setStringFilters)
    Q_PROPERTY(int count READ count)

public:
    ImportPathsModel(QObject *parent = 0);
    ~ImportPathsModel();

    enum Roles {
        FileNameRole = Qt::UserRole + 1,
        FilePathRole,
        FileCheckedRole
    };

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    int count() const { return rowCount(QModelIndex()); }

    QUrl path() const;
    void setPath(const QUrl &folder);

    QUrl parentPath() const;

    QStringList stringFilters() const;
    void setStringFilters(const QStringList &filters);

    Q_INVOKABLE bool isFolder(int index) const;

    Q_INVOKABLE bool isChecked(int index) const;
    Q_INVOKABLE bool setChecked(int index, bool value);

    Q_INVOKABLE QStringList selectedPaths();

    virtual void classBegin();
    virtual void componentComplete();

Q_SIGNALS:
    void pathChanged();

private:
    ImportPathsModelPrivate * const d_ptr;

    Q_DECLARE_PRIVATE(ImportPathsModel)

    Q_PRIVATE_SLOT(d_func(), void refresh())
    Q_PRIVATE_SLOT(d_func(), void inserted(const QModelIndex &, int, int))
    Q_PRIVATE_SLOT(d_func(), void removed(const QModelIndex &, int, int))
    Q_PRIVATE_SLOT(d_func(), void handleDataChanged(const QModelIndex &, const QModelIndex &))
};


QML_DECLARE_TYPE(ImportPathsModel)

#endif // IMPORTPATHSMODEL_H
