#include "partyitem.h"

PartyItem::PartyItem(QObject *parent)
    : QObject(parent)
    , m_port(12345)
{
}

QString PartyItem::uuid() const
{
    return m_uuid;
}

void PartyItem::setUuid(const QString &uuid)
{
    if (m_uuid != uuid) {
        m_uuid = uuid;
        emit uuidChanged();
    }
}

QString PartyItem::name() const
{
    return m_name;
}

void PartyItem::setName(const QString &name)
{
    if (m_name != name) {
        m_name = name;
        emit nameChanged();
    }
}

QString PartyItem::hashtag() const
{
    return m_hashtag;
}

void PartyItem::setHashtag(const QString &hashtag)
{
    if (m_hashtag != hashtag) {
        m_hashtag = hashtag;
        emit hashtagChanged();
    }
}

QString PartyItem::address() const
{
    return m_address;
}

void PartyItem::setAddress(const QString &address)
{
    if (m_address != address) {
        m_address = address;
        emit addressChanged();
    }
}

int PartyItem::port() const
{
    return m_port;
}

void PartyItem::setPort(int port)
{
    if (m_port != port) {
        m_port = port;
        emit portChanged();
    }
}
