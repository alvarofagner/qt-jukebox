/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <QtNetwork/QTcpSocket>
#include "musicitem.h"

class ServerSocket : public QTcpSocket
{
    Q_OBJECT
public:
    ServerSocket(QObject *parent = 0);

    void setNowPlayingSongId(const QString &id);

public slots:
    void onCatalogAddedItems(const ListOfMusicItems &items);
    void onCatalogDeletedItems(const ListOfMusicItems &items);
    void onCatalogUpdatedItems(const ListOfMusicItems &items);
    void onCatalogListItems(const ListOfMusicItems &items);
    void onNowPlaying(const QString &id);
    void forceNowPlaying();

signals:
    void catalogRequested();
    void vote(const QString &id);

private slots:
    void onReadyRead();

private:
    QString m_nowPlayingSongId;
};

#endif // SERVERSOCKET_H
