/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef CATALOG_CONTROLLER_H
#define CATALOG_CONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include "musicitem.h"

class Catalog;
class CatalogModel;
class DBManager;
class FilePicker;
class MusicItemModel;
class MusicItemModelProxy;


class CatalogController : public QObject
{
    Q_OBJECT

public:
    CatalogController(QObject *parent = 0);

    void initClientMode();
    void initDjMode();
    MusicItemModel* musicItemsModel();
    Catalog* catalog();
    CatalogModel* catalogModel();
    MusicItemModelProxy* rankedListModel();
    ListOfMusicItems catalogItems();
    QSharedPointer<MusicItem> itemById(const QString &id);
    bool isCatalogEmpty();
    int modelCount();

signals:
    void catalogAddedItems(ListOfMusicItems items);
    void catalogDeletedItems(ListOfMusicItems items);
    void catalogUpdatedItems(ListOfMusicItems items);
    void catalogSettedItems(ListOfMusicItems items);
    void filesCountUpdated(int count);
    void catalogLoadedChanged();

public slots:
    void onAllMusicItemsReady();
    void onMusicItemReady(const QSharedPointer<MusicItem> &item);

    void addItemsInCatalog(const ListOfMusicItems &items);
    void deleteItemsInCatalog(const ListOfMusicItems &items);
    void updateItemsInCatalog(const ListOfMusicItems &items);
    void setItemsInCatalog(const ListOfMusicItems &items);

    void sortCatalogBySongName();
    void sortCatalogByArtistName();
    void loadCatalog(const QStringList &paths);

private:
    void init();

    DBManager *m_dbManager;
    MusicItemModel *m_musicItemsModel;
    CatalogModel *m_catalogModel;
    MusicItemModelProxy *m_rankedListModel;
    MusicItemModelProxy *m_sortedModel;
    Catalog *m_catalog;
    FilePicker *m_filepicker;
};

#endif // CATALOG_CONTROLLER_H
