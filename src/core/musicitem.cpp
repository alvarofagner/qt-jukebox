/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "musicitem.h"

#ifdef USE_3RDPARTY
#include <qobjecthelper.h>
#else
#include <qjson/qobjecthelper.h>
#endif

#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>

#include <QtSql/QSqlField>


MusicItem::MusicItem(QObject *parent)
    : QObject(parent)
    , m_votesCount(0)
{
}

MusicItem::~MusicItem()
{
}

QStringList MusicItem::objectDBProperties() const
{
    QStringList properties;

    const QMetaObject *metaobject = this->metaObject();
    const int count = metaobject->propertyCount();
    for (int i = 0; i < count; ++i) {
        QMetaProperty prop = metaobject->property(i);
        QString propertyName = prop.name();
        if (prop.isStored() && propertyName != "objectName") {
            properties << propertyName;
        }
    }

    return properties;
}

QString MusicItem::songId() const
{
    return m_songId;
}

void MusicItem::setSongId(const QString &songId)
{
    if (songId != m_songId) {
        m_songId = songId;
        emit songIdChanged();
    }
}

QString MusicItem::songTitle() const
{
    return m_songTitle;
}

void MusicItem::setSongTitle(const QString &songTitle)
{
    if (songTitle != m_songTitle) {
        m_songTitle = songTitle;
        emit songTitleChanged();
    }
}

QString MusicItem::artistName() const
{
    return m_artistName;
}

void MusicItem::setArtistName(const QString &artistName)
{
    if (artistName != m_artistName) {
        m_artistName = artistName;
        emit artistNameChanged();
    }
}

QString MusicItem::albumName() const
{
    return m_albumName;
}

void MusicItem::setAlbumName(const QString &albumName)
{
    if (albumName != m_albumName) {
        m_albumName = albumName;
        emit albumNameChanged();
    }
}

QUrl MusicItem::songUrl() const
{
    return m_songUrl;
}

void MusicItem::setSongUrl(const QUrl &songUrl)
{
    if (songUrl != m_songUrl) {
        m_songUrl = songUrl;
        emit songUrlChanged();
    }
}

int MusicItem::votesCount() const
{
    return m_votesCount;
}

void MusicItem::setVotesCount(int votesCount)
{
    if (votesCount != m_votesCount) {
        m_votesCount = votesCount;
        emit votesCountChanged();
    }
}

ListOfMusicItems variantToListOfMusicItems(const QVariant &var)
{
    QVariantList list = var.toList();
    ListOfMusicItems listOfItems;

    QList<QVariant>::const_iterator i;
    for (i = list.begin(); i != list.end(); ++i) {
        QSharedPointer<MusicItem> item = QSharedPointer<MusicItem>(new MusicItem);
        QVariantMap map = i->toMap();

        QMap<QString, QVariant>::const_iterator j;
        for (j = map.constBegin(); j != map.constEnd(); ++j)
            item->setProperty(j.key().toAscii(), j.value());

        listOfItems.append(item);
    }

    return listOfItems;
}

QVariant listOfMusicItemsToVariant(const ListOfMusicItems &items)
{
    QVariantList list;

    QList<QSharedPointer<MusicItem> >::const_iterator i;
    for (i = items.begin(); i != items.end(); ++i) {
        QVariantMap map;
        QStringList propList = (*i)->objectDBProperties();

        QList<QString>::const_iterator j;
        for (j = propList.begin(); j != propList.end(); ++j)
            map[*j] = (*i)->property((*j).toAscii());

        list.append(map);
    }

    return list;
}
