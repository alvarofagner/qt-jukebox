#include <QtGui/QFileSystemModel>
#include <QtDeclarative/QDeclarativeContext>
#include <QtCore/QSet>
#include <QtGui/QDesktopServices>

#include "importpathsmodel.h"

class ImportPathsModelPrivate
{
public:
    ImportPathsModelPrivate(ImportPathsModel *parent);

    void refresh();
    void inserted(const QModelIndex &index, int start, int end);
    void removed(const QModelIndex &index, int start, int end);
    void handleDataChanged(const QModelIndex &start, const QModelIndex &end);

    ImportPathsModel * const q_ptr;
    Q_DECLARE_PUBLIC(ImportPathsModel)

    QFileSystemModel *model;
    QStringList stringFilters;
    QModelIndex pathIndex;
    int count;
    QSet<QPersistentModelIndex> checklist;
};

ImportPathsModelPrivate::ImportPathsModelPrivate(ImportPathsModel *parent)
    : q_ptr(parent)
    , model(new QFileSystemModel())
    , count(0)
{
    model->setNameFilterDisables(false);
    QString musicPath = QDesktopServices::storageLocation(QDesktopServices::MusicLocation);
    model->setRootPath(musicPath.isEmpty() ? QDir::homePath() : musicPath);
    model->setFilter(QDir::AllDirs | QDir::Files | QDir::Drives | QDir::NoDotAndDotDot);
}

void ImportPathsModelPrivate::refresh()
{
    Q_Q(ImportPathsModel);

    pathIndex = QModelIndex();
    if (count) {
        emit q->beginRemoveRows(QModelIndex(), 0, count);
        count = 0;
        emit q->endRemoveRows();
    }
    pathIndex = model->index(model->rootPath());
    int newcount = model->rowCount(pathIndex);
    if (newcount) {
        emit q->beginInsertRows(QModelIndex(), 0, newcount - 1);
        count = newcount;
        emit q->endInsertRows();
    }
}

void ImportPathsModelPrivate::inserted(const QModelIndex &index, int start, int end)
{
    Q_Q(ImportPathsModel);

    if (index == pathIndex) {
        emit q->beginInsertRows(QModelIndex(), start, end);
        count = model->rowCount(pathIndex);
        emit q->endInsertRows();
    }
}

void ImportPathsModelPrivate::removed(const QModelIndex &index, int start, int end)
{
    Q_Q(ImportPathsModel);

    if (index == pathIndex) {
        emit q->beginRemoveRows(QModelIndex(), start, end);
        count = model->rowCount(pathIndex);
        emit q->endRemoveRows();
    }
}

void ImportPathsModelPrivate::handleDataChanged(const QModelIndex &start, const QModelIndex &end)
{
    Q_Q(ImportPathsModel);

    if (start.parent() == pathIndex)
        emit q->dataChanged(q->index(start.row(), 0), q->index(end.row(), 0));
}

ImportPathsModel::ImportPathsModel(QObject *parent)
    : QAbstractListModel(parent)
    , d_ptr(new ImportPathsModelPrivate(this))
{
    QHash<int, QByteArray> roles;
    roles[FileNameRole] = "fileName";
    roles[FilePathRole] = "filePath";
    roles[FileCheckedRole] = "fileChecked";
    setRoleNames(roles);

    Q_D(ImportPathsModel);

    connect(d->model, SIGNAL(rowsInserted(const QModelIndex&, int, int)),
            this, SLOT(inserted(const QModelIndex&, int, int)));
    connect(d->model, SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
            this, SLOT(removed(const QModelIndex&, int, int)));
    connect(d->model, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)),
            this, SLOT(handleDataChanged(const QModelIndex&, const QModelIndex&)));
    connect(d->model, SIGNAL(modelReset()),
            this, SLOT(refresh()));
    connect(d->model, SIGNAL(layoutChanged()),
            this, SLOT(refresh()));
}

ImportPathsModel::~ImportPathsModel()
{
    delete d_ptr;
}

QVariant ImportPathsModel::data(const QModelIndex &index, int role) const
{
    Q_D(const ImportPathsModel);

    QVariant rv;
    QModelIndex modelIndex = d->model->index(index.row(), 0, d->pathIndex);
    if (modelIndex.isValid()) {
        if (role == FileNameRole)
            rv = d->model->data(modelIndex, QFileSystemModel::FileNameRole).toString();
        else if (role == FilePathRole)
            rv = QUrl::fromLocalFile(d->model->data(modelIndex, QFileSystemModel::FilePathRole).toString());
        else if (role == FileCheckedRole)
            rv = d->checklist.contains(index) ? true : false;
    }

    return rv;
}


bool ImportPathsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_D(ImportPathsModel);

    QModelIndex modelIndex = d->model->index(index.row(), 0, d->pathIndex);
    if (!modelIndex.isValid())
        return false;

    if (role == FileNameRole)
        return d->model->setData(modelIndex, value, QFileSystemModel::FileNameRole);
    else if (role == FilePathRole)
        return d->model->setData(modelIndex, value, QFileSystemModel::FilePathRole);
    else if (role == FileCheckedRole) {
        if (value == true)
            d->checklist.insert(index);
        else
            d->checklist.remove(index);
    }
    return true;
}

bool ImportPathsModel::setChecked(int index, bool value)
{
    Q_D(ImportPathsModel);

    QModelIndex modelIndex = d->model->index(index, 0, d->pathIndex);
    if (modelIndex.isValid())
        return setData(modelIndex, QVariant(value), ImportPathsModel::FileCheckedRole);

    return false;
}

QStringList ImportPathsModel::selectedPaths()
{
    Q_D(const ImportPathsModel);

    QStringList selectedPaths;

    QList<QPersistentModelIndex> indexModels = d->checklist.values();
    for (int i = 0; i < indexModels.size(); i++)
        selectedPaths << d->model->data(indexModels.at(i), QFileSystemModel::FilePathRole).toString();

    return selectedPaths;
}

int ImportPathsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    Q_D(const ImportPathsModel);
    return d->count;
}

QUrl ImportPathsModel::path() const
{
    Q_D(const ImportPathsModel);
    return d->model->rootPath();
}

void ImportPathsModel::setPath(const QUrl &path)
{
    Q_D(ImportPathsModel);

    if (path.toLocalFile() == d->model->rootPath())
        return;

    QModelIndex index = d->model->index(path.toLocalFile());
    if ((index.isValid() && d->model->isDir(index)) || path.toLocalFile().isEmpty()) {
        d->model->setRootPath(path.toLocalFile());
        QMetaObject::invokeMethod(this, "refresh", Qt::QueuedConnection);
        emit pathChanged();
    }
}

QUrl ImportPathsModel::parentPath() const
{
    Q_D(const ImportPathsModel);

    QString localFile = d->model->rootPath();
    if (!localFile.isEmpty()) {
        QDir dir(localFile);
#if defined(Q_OS_SYMBIAN) || defined(Q_OS_WIN)
        if (dir.isRoot())
            dir.setPath("");
        else
#endif
            dir.cdUp();
        localFile = dir.path();
    } else {
        int pos = d->model->rootPath().lastIndexOf(QLatin1Char('/'));
        if (pos == -1)
            return QUrl();
        localFile = d->model->rootPath().left(pos);
    }
    return QUrl::fromLocalFile(localFile);
}

QStringList ImportPathsModel::stringFilters() const
{
    Q_D(const ImportPathsModel);
    return d->stringFilters;
}

void ImportPathsModel::setStringFilters(const QStringList &filters)
{
    Q_D(ImportPathsModel);
    d->stringFilters = filters;
    d->model->setNameFilters(d->stringFilters);
}

void ImportPathsModel::classBegin()
{
}

void ImportPathsModel::componentComplete()
{
    Q_D(const ImportPathsModel);
    if (d->model->rootPath().isEmpty() || !QDir().exists(d->model->rootPath()))
        setPath(QUrl(QLatin1String("file://") + QDir::currentPath()));

    if (!d->pathIndex.isValid())
        QMetaObject::invokeMethod(this, "refresh", Qt::QueuedConnection);
}

bool ImportPathsModel::isFolder(int index) const
{
    Q_D(const ImportPathsModel);
    if (index != -1) {
        QModelIndex id = d->model->index(index, 0, d->pathIndex);
        if (id.isValid())
            return d->model->isDir(id);
    }
    return false;
}

bool ImportPathsModel::isChecked(int index) const
{
    Q_D(const ImportPathsModel);
    if (index != -1) {
        QModelIndex id = d->model->index(index, 0, d->pathIndex);
        if (id.isValid())
            return (d->checklist.contains(id));
    }
    return false;
}

#include "moc_importpathsmodel.cpp"
