#ifndef CUSTOMWEBPAGE_H
#define CUSTOMWEBPAGE_H

#include <QtWebKit/QWebPage>

class CustomWebPage : public QWebPage
{
    Q_OBJECT
public:
    CustomWebPage(QWidget *parent = 0);

    QString userAgentForUrl(const QUrl &url) const;

};

#endif // CUSTOMWEBPAGE_H
