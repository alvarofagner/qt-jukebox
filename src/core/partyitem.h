#ifndef PARTYITEM_H
#define PARTYITEM_H

#include <QtCore/QObject>
#include <QtCore/QString>

class PartyItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString uuid READ uuid WRITE setUuid NOTIFY uuidChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString hashtag READ hashtag WRITE setHashtag NOTIFY hashtagChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)

public:
    PartyItem(QObject *parent = 0);

    QString uuid() const;
    void setUuid(const QString &uuid);

    QString name() const;
    void setName(const QString &name);

    QString hashtag() const;
    void setHashtag(const QString &hashtag);

    QString address() const;
    void setAddress(const QString &address);

    int port() const;
    void setPort(int port);

signals:
    void uuidChanged();
    void nameChanged();
    void hashtagChanged();
    void addressChanged();
    void portChanged();

private:
    QString m_uuid;
    QString m_name;
    QString m_hashtag;
    QString m_address;
    int m_port;
};

#endif // PARTYITEM_H
