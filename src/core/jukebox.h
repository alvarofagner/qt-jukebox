/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef JUKEBOX_H
#define JUKEBOX_H

#include <QtGui/QMainWindow>
#include <QtCore/QModelIndex>
#include <QtCore/QSettings>
#include <QtCore/QUrl>
#include <QtMultimediaKit/QMediaPlayer>
#include <QtNetwork/QHostAddress>

#include "musicitem.h"
#include "partyitem.h"
#include "importpathsmodel.h"

class CatalogController;
class ClientSocket;
class PartyAnnounce;
class PartyDiscover;
class QDeclarativeContext;
class QDeclarativeView;
class QMediaPlayer;
class TwitterClient;
class Server;
class FilePicker;
class QResizeEvent;


class Jukebox : public QMainWindow
{
    Q_OBJECT
    Q_ENUMS(Behavior)
    Q_ENUMS(PartyType)
    Q_ENUMS(Platform)
    Q_ENUMS(PlayingMode)

    Q_PROPERTY(int appHeight READ appHeight WRITE setAppHeight NOTIFY appHeightChanged)
    Q_PROPERTY(int appWidth READ appWidth WRITE setAppWidth NOTIFY appWidthChanged)
    Q_PROPERTY(int contentHeight READ contentHeight WRITE setContentHeight NOTIFY contentHeightChanged)
    Q_PROPERTY(bool importCatalog READ importCatalog NOTIFY catalogEmptyChanged)
    Q_PROPERTY(bool catalogEmpty READ catalogEmpty NOTIFY catalogEmptyChanged)
    Q_PROPERTY(int catalogCount READ catalogCount NOTIFY catalogCountChanged)
    Q_PROPERTY(Behavior behavior READ behavior WRITE setBehavior NOTIFY behaviorChanged)
    Q_PROPERTY(QString catalogImportDir READ catalogImportDir NOTIFY catalogImportDirChanged)

    Q_PROPERTY(MusicItem* nowPlayingSong READ nowPlayingSong NOTIFY nowPlayingSongChanged)

    Q_PROPERTY(Platform platform READ platform WRITE setPlatform NOTIFY platformChanged)
    Q_PROPERTY(PartyType partyType READ partyType WRITE setPartyType NOTIFY partyTypeChanged)
    Q_PROPERTY(PlayingMode playingMode READ playingMode WRITE setPlayingMode)
    Q_PROPERTY(PartyItem* party READ party NOTIFY partyChanged)

    Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)
    Q_PROPERTY(QString localPartyName READ localPartyName WRITE setLocalPartyName NOTIFY localPartyNameChanged)
    Q_PROPERTY(QString catalogFilename READ catalogFilename WRITE setCatalogFilename NOTIFY catalogFilenameChanged)

public:
    enum Behavior {
        JukeboxDjMode,
        JukeboxClientMode,
        JukeboxRankMode,
        JukeboxCatalogMode,
        JukeboxGuestMode,
        JukeboxInvalid
    };

    enum PartyType {
        PrivateParty,
        PublicParty
    };

    enum Platform {
        MobilePlatform,
        DesktopPlatform
    };

    enum PlayingMode {
        AutoPlay,
        DjPro
    };

    Jukebox(Behavior behavior = JukeboxInvalid, QWidget *parent = 0);
    ~Jukebox();

    QString dataDir() const;
    CatalogController* catalogController();

    int appHeight() { return m_appHeight; }
    void setAppHeight(int appHeight) { m_appHeight = appHeight; }

    int appWidth() { return m_appWidth; }
    void setAppWidth(int appWidth) { m_appWidth = appWidth; }

    int contentHeight() { return m_contentHeight; }
    void setContentHeight(int contentHeight) { m_contentHeight = contentHeight; }

    bool importCatalog();
    bool catalogEmpty();
    int catalogCount();

    Behavior behavior() { return m_behavior; }
    void setBehavior(Behavior behavior);

    Platform platform() { return m_platform; }
    void setPlatform(Platform platform);

    PlayingMode playingMode() { return m_playingMode; }
    void setPlayingMode(PlayingMode mode) { m_playingMode = mode; }

    PartyType partyType() { return m_partyType; }
    void setPartyType(PartyType type);

    PartyItem* party() const;

    MusicItem* nowPlayingSong() const;
    QString catalogImportDir() const;

    bool isConnected() const;

    QString localPartyName();
    void setLocalPartyName(const QString &name);
    QString catalogFilename();
    void setCatalogFilename(const QString &name);

public slots:
    bool connectClientSocket(const QString &uuid);
    bool connectClientSocket(const QHostAddress &address, quint16 port);
    bool connectClientSocket(const QString &address, quint16 port);
    bool sendVote(const QString &id);
    bool removeMusic(const QString &id);

    void sortCatalogBySongName();
    void sortCatalogByArtistName();

    void loadCatalog(const QStringList &paths);
    void playPauseSong(bool playing);
    void stopSong();
    void nextSong();
    void searchParties();
    bool isAlreadyVoted(const QString &id);
    void createParty(const QString &name);

protected slots:
    void resizeEvent(QResizeEvent *event);

private slots:
    void onClientSocketConnected();
    void onNewTrackedTweet(const QVariant &var);
    void onRankedListRowsInserted(const QModelIndex &parent, int first, int last);
    void onNowPlaying(const QString &id);
    void onClientVote(const QString &id);
    void onConnectionError(QAbstractSocket::SocketError socketError);
    void onMediaStateChanged(QMediaPlayer::State);
    void refreshCatalog();
    void tryReconnectClient();

signals:
    void appHeightChanged();
    void appWidthChanged();
    void contentHeightChanged();
    void authenticatedChanged();
    void behaviorChanged();
    void platformChanged();
    void catalogEmptyChanged();
    void catalogCountChanged();
    void nowPlayingSongChanged();
    void nowPlaying(const QString &id);
    void songDurationChanged(qint64 time);
    void songEnded();
    void songPositionChanged(qint64 time);
    void catalogImportDirChanged();
    void catalogLoadedChanged();
    void partyChanged();
    void partyTypeChanged();
    void connectedChanged();
    void showMessageDialog(const QString &title, const QString &text);
    void localPartyNameChanged();
    void catalogFilenameChanged();
    void rememberTwitterAccountChanged();

private:
    bool startClientView();
    bool startGuestMode();
    bool startDJMode();

    QDeclarativeView *m_view;
    QDeclarativeContext *m_context;
    int m_appHeight;
    int m_appWidth;
    int m_contentHeight;
    Behavior m_behavior;
    Platform m_platform;
    PlayingMode m_playingMode;
    TwitterClient *m_twitterClient;
    Server *m_server;
    ClientSocket *m_clientSocket;

    PartyAnnounce *m_partyAnnounce;
    PartyDiscover *m_partyDiscover;

    QMediaPlayer *m_mediaPlayer;

    QSharedPointer<MusicItem> m_defaultNowPlayingSong;
    QSharedPointer<MusicItem> m_nowPlayingSong;
    bool m_playerPlaying;
    QSharedPointer<PartyItem> m_party;
    PartyType m_partyType;

    QHostAddress m_djAddress;
    quint16 m_djPort;
    CatalogController *m_catalogController;
    ImportPathsModel *m_importPathsModel;

    bool m_catalogLoaded;

    QStringList m_votedSongs;
    QSettings *m_settings;

    static const QString s_applicationName;
    static const QString s_organizationName;
};

#endif // END JUKEBOX
