/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "catalog.h"
#include "catalogcontroller.h"
#include "catalogmodel.h"
#include "dbmanager.h"
#include "filepicker.h"
#include "musicitemmodel.h"
#include "musicitemmodelproxy.h"

#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

CatalogController::CatalogController(QObject *parent)
    : QObject(parent)
    , m_dbManager(0)
    , m_musicItemsModel(0)
    , m_catalogModel(0)
    , m_rankedListModel(0)
    , m_sortedModel(0)
    , m_catalog(new Catalog(this))
    , m_filepicker(new FilePicker(this))
{
    m_rankedListModel = new MusicItemModelProxy(this);
    m_rankedListModel->setDynamicSortFilter(true);
    m_rankedListModel->sort(5, Qt::DescendingOrder);

    m_sortedModel = new MusicItemModelProxy(this);
    m_sortedModel->setDynamicSortFilter(true);
    m_sortedModel->sort(2, Qt::AscendingOrder);

    m_catalogModel = new CatalogModel(m_sortedModel, this);

    // XXX I don't know if it belongs to here
    connect(m_catalog, SIGNAL(allMusicItemsReady()),
            this, SLOT(onAllMusicItemsReady()));
    /*connect(m_catalog, SIGNAL(musicItemReady(const QSharedPointer<MusicItem> &)),
            this, SLOT(onMusicItemReady(const QSharedPointer<MusicItem> &)));*/
}

void CatalogController::init()
{
    m_musicItemsModel = m_dbManager->tableModel();

    m_rankedListModel->setSourceModel(m_musicItemsModel);
    m_sortedModel->setSourceModel(m_musicItemsModel);

    m_catalogModel->refreshItems();
    emit filesCountUpdated(modelCount());
}

void CatalogController::initClientMode()
{
    if (!m_dbManager)
        m_dbManager = new DBManager(this);

    m_dbManager->refreshClientMode();
    init();
}

void CatalogController::initDjMode()
{
    if (!m_dbManager)
        m_dbManager = new DBManager(this);

    m_dbManager->refreshDjMode();
    init();
}

MusicItemModel* CatalogController::musicItemsModel()
{
    return m_musicItemsModel;
}

Catalog* CatalogController::catalog()
{
    return m_catalog;
}

CatalogModel* CatalogController::catalogModel()
{
    return m_catalogModel;
}

MusicItemModelProxy* CatalogController::rankedListModel()
{
    return m_rankedListModel;
}

ListOfMusicItems CatalogController::catalogItems()
{
    return m_musicItemsModel->listOfMusicItems();
}

QSharedPointer<MusicItem> CatalogController::itemById(const QString &id)
{
    return m_musicItemsModel->itemById(id);
}

bool CatalogController::isCatalogEmpty()
{
    if (!m_musicItemsModel)
        return true;
    return m_musicItemsModel->rowCount() <= 0;
}

int CatalogController::modelCount()
{
    if (!m_musicItemsModel)
        return 0;
    return m_musicItemsModel->rowCount();
}

void CatalogController::onAllMusicItemsReady()
{
    if (!m_musicItemsModel || !m_catalog) {
        qCritical() << __func__ << m_catalogModel;
        return;
    }

    setItemsInCatalog(m_catalog->musicItems());
    m_sortedModel->sort(m_sortedModel->sortColumn(), m_sortedModel->sortOrder());
}

void CatalogController::onMusicItemReady(const QSharedPointer<MusicItem> &item)
{
    if (!m_musicItemsModel || !m_catalog) {
        qCritical() << __func__ << m_catalogModel << m_catalog;
        return;
    }

    if (!item.isNull()) {
        ListOfMusicItems list;
        list.append(item);
        addItemsInCatalog(list);
    }
}

void CatalogController::addItemsInCatalog(const ListOfMusicItems &items)
{
    QList<QSharedPointer<MusicItem> >::const_iterator i;
    for (i = items.begin(); i != items.end(); ++i)
        if (!m_musicItemsModel->itemById((*i)->songId()))
            m_musicItemsModel->insertMusicItem(*i);

    emit catalogAddedItems(items);
}

void CatalogController::deleteItemsInCatalog(const ListOfMusicItems &items)
{
    QList<QSharedPointer<MusicItem> >::const_iterator i;
    for (i = items.begin(); i != items.end(); ++i)
        m_musicItemsModel->deleteMusicItem(*i);

    emit catalogDeletedItems(items);
}

void CatalogController::updateItemsInCatalog(const ListOfMusicItems &items)
{
    QList<QSharedPointer<MusicItem> >::const_iterator i;
    for (i = items.begin(); i != items.end(); ++i)
        m_musicItemsModel->updateMusicItem(*i);

    emit catalogUpdatedItems(items);
}

void CatalogController::setItemsInCatalog(const ListOfMusicItems &items)
{
    m_musicItemsModel->clear();

    m_musicItemsModel->insertMusicItems(items);

    emit catalogSettedItems(items);
}

void CatalogController::sortCatalogBySongName()
{
    m_sortedModel->sort(1, Qt::AscendingOrder);
}

void CatalogController::sortCatalogByArtistName()
{
    m_sortedModel->sort(2, Qt::AscendingOrder);
}

void CatalogController::loadCatalog(const QStringList &paths)
{
    if (!m_filepicker) {
        qCritical() << __func__ << "FilePicker is null...";
        return;
    }

    if (paths.isEmpty()) {
        emit filesCountUpdated(0);
        m_catalog->setPlaylistResource(QStringList());
    }

    QStringList files;
    for (int i=0; i < paths.count(); i++) {
        QUrl u = QUrl::fromUserInput(paths.at(i));
        QFileInfo info(u.toLocalFile());

        if (!info.isDir())
            files << paths.at(i);
        else
            files << m_filepicker->songListFromDir(info.absoluteFilePath());
        m_catalog->setPlaylistResource(files);
        emit filesCountUpdated(files.count());
    }

    m_catalogModel->refreshItems();
    emit catalogLoadedChanged();
}
