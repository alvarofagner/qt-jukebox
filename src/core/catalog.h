/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef CATALOG_H
#define CATALOG_H

#include "musicitem.h"

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtXml/QXmlStreamReader>

class Utils;

class Catalog : public QObject
{
    Q_OBJECT

public:
    Catalog(QObject *parent = 0);
    ~Catalog();
    void setPlaylistResource(const QStringList &resources);
    QList<QSharedPointer<MusicItem> > musicItems() const;

signals:
    void musicItemReady(const QSharedPointer<MusicItem> &item);
    void allMusicItemsReady();

private slots:
    void parseXSPF();
    void addSingleFile(const QString &path);

private:
    QList<QSharedPointer<MusicItem> > m_listItems;
    Utils *m_utils;
    QXmlStreamReader m_xml;
};

#endif
