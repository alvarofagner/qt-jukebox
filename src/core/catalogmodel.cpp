/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/


#include "catalogmodel.h"
#include "musicitemmodel.h"
#include <QtCore/qmath.h>
#include <QtCore/QDebug>

CatalogModel::CatalogModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_itemsByPage(6)
    , m_items()
{
}

CatalogModel::CatalogModel(const CatalogModel &other)
    : QAbstractListModel(0)
    , m_musicModel(other.m_musicModel)
    , m_itemsByPage(6)
    , m_items(other.m_items)
{
    QHash<int, QByteArray> roles;
    roles[0 + Qt::UserRole] = "frontModel";
    roles[1 + Qt::UserRole] = "backModel";

    setRoleNames(roles);
}

CatalogModel::CatalogModel(QAbstractItemModel *musicModel, QObject *parent)
    : QAbstractListModel(parent)
    , m_musicModel(musicModel)
    , m_itemsByPage(6)
    , m_items()
{
    QHash<int, QByteArray> roles;
    roles[0 + Qt::UserRole] = "frontModel";
    roles[1 + Qt::UserRole] = "backModel";

    setRoleNames(roles);

    connect(m_musicModel, SIGNAL(rowsInserted(QModelIndex, int, int)),
            this, SLOT(refreshItems()));
    connect(m_musicModel, SIGNAL(rowsAboutToBeRemoved(QModelIndex, int, int)),
            this, SLOT(refreshItems()));
}

QVariant CatalogModel::data(const QModelIndex &rowIndex, int role) const
{
    if (!rowIndex.isValid())
        return QVariant();

    switch (role) {
    // front model
    case 0 + Qt::UserRole:
        return QVariant::fromValue(m_items.value(rowIndex.row() * 2).data());
    // back model
    case 1 + Qt::UserRole:
        return QVariant::fromValue(m_items.value(rowIndex.row() * 2 + 1).data());
    }

    return QVariant();
}

int CatalogModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return qCeil(qreal(m_items.count()) / 2);
}

void CatalogModel::refreshItems()
{
    MusicItemModelProxy *proxy = qobject_cast<MusicItemModelProxy*>(m_musicModel);
    if (proxy) {
        MusicItemModel *model = qobject_cast<MusicItemModel*>(proxy->sourceModel());
        if (model) {
            while (model->canFetchMore()) {
                model->fetchMore();
            }
        }
    }

    int pages = qCeil(qreal(m_musicModel->rowCount()) / (m_itemsByPage * 2));

    beginResetModel();
    m_items.clear();
    endResetModel();

    // inserting more pages
    QSharedPointer<MusicItemModelProxy> item;
    for (int i = rowCount(); i < pages; i++) {
        beginInsertRows(QModelIndex(), i, i);
        // front side
        item = QSharedPointer<MusicItemModelProxy>(new MusicItemModelProxy);
        item->setItemsByPage(m_itemsByPage);
        item->setPage(i * 2);
        item->setSourceModel(m_musicModel);
        m_items.append(item);

        // back side
        item = QSharedPointer<MusicItemModelProxy>(new MusicItemModelProxy);
        item->setItemsByPage(m_itemsByPage);
        item->setPage(i * 2 + 1);
        item->setSourceModel(m_musicModel);
        m_items.append(item);
        endInsertRows();
    }

    // removing extra pages
    if (pages < rowCount()) {
        // NYI: do a real remove
        beginResetModel();
        m_items.clear();
        endResetModel();
        refreshItems();
    }
}
