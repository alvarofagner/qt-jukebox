/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#ifndef DBMANAGER_H
#define DBMANAGER_H

#include "musicitemmodel.h"

class QSqlDatabase;
class QSqlTableModel;

class DBManager : public QObject
{
    Q_OBJECT

public:
    DBManager(QObject *parent = 0);
    DBManager(const DBManager &other);

    void refreshClientMode();
    void refreshDjMode();
    MusicItemModel *tableModel() { return m_tableModel; }

private:
    bool createSongsTable();
    bool deleteClientSongsTable();
    void start();

    QSqlDatabase m_db;
    MusicItemModel *m_tableModel;
    QString m_tableName;
};

#endif // DBMANAGER_H
