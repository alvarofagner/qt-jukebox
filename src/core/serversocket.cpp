/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "serversocket.h"

#ifdef USE_3RDPARTY
#include <parser.h>
#include <serializer.h>
#else
#include <qjson/parser.h>
#include <qjson/serializer.h>
#endif

#include <QtCore/QByteArray>
#include <QtCore/QThread>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

ServerSocket::ServerSocket(QObject *parent)
    : QTcpSocket(parent)
{
    connect(this, SIGNAL(readyRead()),
            this, SLOT(onReadyRead()));
}

void ServerSocket::setNowPlayingSongId(const QString &id)
{
    m_nowPlayingSongId = id;
}

void ServerSocket::onCatalogAddedItems(const ListOfMusicItems &items)
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "add";
    map["items"] = listOfMusicItemsToVariant(items);

    write(serializer.serialize(map) + "\r\n");
    flush();
}

void ServerSocket::onCatalogDeletedItems(const ListOfMusicItems &items)
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "delete";
    map["items"] = listOfMusicItemsToVariant(items);

    write(serializer.serialize(map) + "\r\n");
    flush();
}

void ServerSocket::onCatalogUpdatedItems(const ListOfMusicItems &items)
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "update";
    map["items"] = listOfMusicItemsToVariant(items);

    write(serializer.serialize(map) + "\r\n");
    flush();
}

void ServerSocket::onCatalogListItems(const ListOfMusicItems &items)
{
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "catalog";
    map["action"] = "list";
    map["items"] = listOfMusicItemsToVariant(items);

    write(serializer.serialize(map) + "\r\n");
    flush();
}

void ServerSocket::onNowPlaying(const QString &id)
{
    if (id.isEmpty()) {
        return;
    }

    m_nowPlayingSongId = id;
    QVariantMap map;
    QJson::Serializer serializer;

    map["what"] = "player";
    map["action"] = "nowPlaying";
    map["items"] = id;

    write(serializer.serialize(map) + "\r\n");
    flush();
}

void ServerSocket::forceNowPlaying()
{
    onNowPlaying(m_nowPlayingSongId);
}

void ServerSocket::onReadyRead()
{
    while (canReadLine()) {
        QByteArray line = readLine();

        bool parserOk = true;
        QJson::Parser parser;
        QVariant var = parser.parse(line, &parserOk);

        if (!parserOk) {
            qWarning() << "json error at"
                       << parser.errorLine()
                       << parser.errorString();
        } else {
            QVariantMap map = var.toMap();
            QString what = map["what"].toString();
            if (what == "catalog") {
                QString action = map["action"].toString();
                if (action == "list") {
                    emit catalogRequested();
                } else if (action == "vote") {
                    emit vote(map["id"].toString());
                }
            }
        }
    }
}
