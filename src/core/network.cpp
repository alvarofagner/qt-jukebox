/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

#include "network.h"
#include "networkrequest.h"

#include <QtGui/QPixmap>
#include <QtCore/QDebug>
#include <QtGui/QDesktopServices>
#include <QtCore/QSettings>
#include <QtNetwork/QNetworkAccessManager>


Network::Network(QObject *parent)
    : QObject(parent)
    , m_networkManager(new QNetworkAccessManager(this))
    , m_error(UnknownError)
{
    connect(m_networkManager, SIGNAL(finished(QNetworkReply *)),
            this, SLOT(onReplyFinished(QNetworkReply *)));
}

Network::~Network()
{
}

void Network::onReplyFinished(QNetworkReply *reply)
{
    NetworkRequest *request = m_requests.take(reply);
    if (!request)
        return;

    reply->deleteLater();

    if (reply->error() != QNetworkReply::NoError) {
        handleReplyError(reply, request);
        return;
    }

    request->deleteLater();

    switch (request->type()) {

    case NetworkRequest::RequestAuthentication:
        handleAuthenticate(reply);
        break;

    default:
        qWarning("Invalid request type");
    }
}

NetworkRequest *Network::createRequest(const QString &url, NetworkRequest::RequestType type)
{
    NetworkRequest *request = new NetworkRequest(url, type);

    return request;
}

int Network::doGet(const QString &url, NetworkRequest::RequestType type)
{
    NetworkRequest *request = createRequest(url, type);

    QNetworkReply *reply = m_networkManager->get(request->networkRequest());
    m_requests[reply] = request;

    return request->id();
}

int Network::doPost(const QString &url, NetworkRequest::RequestType type)
{
    NetworkRequest *request = createRequest(url, type);

    QNetworkReply *reply = m_networkManager->post(request->networkRequest(), QByteArray());
    m_requests[reply] = request;

    return request->id();
}

int Network::doHead(const QString &url, NetworkRequest::RequestType type)
{
    NetworkRequest *request = createRequest(url, type);

    QNetworkReply *reply = m_networkManager->head(request->networkRequest());
    m_requests[reply] = request;

    return request->id();
}

void Network::handleReplyError(QNetworkReply *reply, NetworkRequest *request)
{
    m_requests.take(reply);

    switch (reply->error()) {

    default:
        qWarning("Network error not handled: %d -- request: %s", reply->error(), qPrintable(request->url().toString()));
    }

    emit error(request->type(), m_error);

    reply->deleteLater();
}

void Network::handleAuthenticate(QNetworkReply *reply)
{
    Q_UNUSED(reply);
    // XXX Need implementation
}
