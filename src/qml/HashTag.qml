import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: hashTag
    width: (leftBorder.width + hashMiddleBg.width + rightBorder.width)
    height: hashMiddleBg.height
    visible: {
        return (musicJukebox.party.hashtag != "")
    }

    Image {
        id: leftBorder
        source: "qrc:/data/images/hashtag_left_slice.png"
        anchors.top: hashTag.top
        anchors.left: hashTag.left
    }

    Image {
        id: hashMiddleBg
        anchors.top: hashTag.top
        anchors.left: leftBorder.right
        fillMode: "TileHorizontally"
        width: hashTagText.paintedWidth
        source: "qrc:/data/images/hashtag_middle_slice.png"
    }

    Image {
        id: rightBorder
        anchors.top: hashTag.top
        anchors.left: hashMiddleBg.right
        source: "qrc:/data/images/hashtag_right_slice.png"
    }

    Text {
        id: hashTagText
        anchors.left: hashMiddleBg.left
        anchors.verticalCenter: hashMiddleBg.verticalCenter
        anchors.right: hashMiddleBg.right

        color: "#afe044"

        font {
            bold: true
            pixelSize: {
                if (musicJukebox.appWidth > 640) {
                    return musicJukebox.appWidth * 0.02
                } else {
                    return musicJukebox.appWidth * 0.025
                }
            }
            family: nokiaPureRegular.name
        }
        text: musicJukebox.party.hashtag
    }
}
