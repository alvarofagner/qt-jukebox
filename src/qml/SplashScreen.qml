import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: splashScreen
    anchors.fill: parent

    Rectangle {
        id: gradientBackground
        anchors.fill: parent
        visible: true
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#a0db3e" }
            GradientStop { position: 1.0; color: "#5ca92e" }
        }

        Image {
            anchors.fill: parent
            fillMode: "Tile"
            source: "qrc:/data/images/dots.png"
            visible: {
                if (musicJukebox.platform == MusicJukebox.MobilePlatform)
                    return true
                else
                    return false
            }
        }

        Image {
            id: logo
            anchors.centerIn: parent
            source: "qrc:/data/images/logo_03.png"
        }

        Image {
            id: topLeftCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            rotation: -90
            anchors.left: parent.left
        }

        Image {
            id: topRightCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            anchors.right: parent.right
        }

        Image {
            id: bottomLeftCorner
            source: "qrc:/data/images/bord.png"
            rotation: 180
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }

        Image {
            id: bottomRightCorner
            source: "qrc:/data/images/bord.png"
            rotation: 90
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    Behavior on opacity {
        NumberAnimation {
            duration: 1000
            easing.type: Easing.OutCirc
        }
    }
}
