/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0
import "constants.js" as Constants

Item {
    property int limit: 50
    property int currentArea: 3
    property bool snapEnabled: true
    property alias rankedListInteractive: rankedList.interactive

    signal importSongs()

    function setCurrentViewArea(area) {
        if (!snapEnabled)
            return;

        if (area < 0 || area > 3) {
            // For invalid area
            return
        }

        buttonSnapUp.visible = false
        buttonSnapDown.visible = false

        currentArea = area

        if (area == 2) {
            rankedList.interactive = true
        } else {
            if (!rankedList.moving && (musicJukebox.behavior != MusicJukebox.JukeboxDjMode))
                rankedList.interactive = false
        }

        if (area == 0) {
            if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode) {
                flickableArea.contentY = area0.pos.y
                flickableArea.visibleView = Constants.FlickableArea.VisibleViewFirst
            } else {
                flickableArea.contentY = area1.pos.y
                flickableArea.visibleView = Constants.FlickableArea.VisibleViewSecond
            }
            buttonSnapUp.visible = false
            buttonSnapDown.visible = true

            return
        }

        if (area == 1) {
            flickableArea.contentY = area1.pos.y
            flickableArea.visibleView = Constants.FlickableArea.VisibleViewSecond
            buttonSnapUp.visible = false
            if (flickableArea.interactive) {
                if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode) {
                    buttonSnapUp.visible = true
                }
                buttonSnapDown.visible = true
            }
            return
        }

        if (area == 2) {
            flickableArea.contentY = area2.pos.y
            flickableArea.visibleView = Constants.FlickableArea.VisibleViewThird
            if (flickableArea.interactive) {
                buttonSnapUp.visible = true
                buttonSnapDown.visible = true
            }
            return
        }

        if (area == 3) {
            flickableArea.contentY = area3.pos.y
            flickableArea.visibleView = Constants.FlickableArea.VisibleViewForth
            if (flickableArea.interactive) {
                buttonSnapUp.visible = true
                buttonSnapDown.visible = false
            }
            return
        }
    }

    function backToCurrentArea() {
        if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewFirst) {
            flickableArea.contentY = area0.pos.y
        } else if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewSecond) {
            flickableArea.contentY = area1.pos.y
        } else if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewThird) {
            flickableArea.contentY = area2.pos.y
        } else if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewForth) {
            flickableArea.contentY = area3.pos.y
        }
    }

    function updateVisibleArea() {
        if (!snapEnabled)
            return;

        if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewFirst) {
            if (flickableArea.contentY > (area0.pos.y + limit)) {
                setCurrentViewArea(1)
            } else {
                backToCurrentArea()
            }
            return
        }

        if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewSecond) {
            if (flickableArea.contentY < (area1.pos.y - limit)) {
                setCurrentViewArea(0)
            }
            if (flickableArea.contentY > (area1.pos.y + limit)) {
                setCurrentViewArea(2)
            } else {
                backToCurrentArea()
            }
            return
        }

        if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewThird) {
            if (flickableArea.contentY < (area2.pos.y - limit)) {
                setCurrentViewArea(1)
            }
            if (flickableArea.contentY > (area2.pos.y + limit)) {
                setCurrentViewArea(3)
            } else {
                backToCurrentArea()
            }
            return
        }

        if (flickableArea.visibleView == Constants.FlickableArea.VisibleViewForth) {
            if (flickableArea.contentY < (area3.pos.y - limit)) {
                setCurrentViewArea(2)
            } else {
                backToCurrentArea()
            }
            return
        }
    }

    function showVoteDialog(id, songTitle, artistName) {
        if (musicJukebox.isAlreadyVoted(id)) {
            return showMessageDialog("Error",
                                     "You have already voted on this song.");
        }

        voteDialog.songId = id
        voteDialog.songName = songTitle
        voteDialog.artistName = artistName
        voteDialog.visible = true

    }

    function showMessageDialog(title, text) {
        messageDialog.title = title
        messageDialog.text = text
        messageDialog.visible = true
    }

    function enableSnap(enabled) {
        snapEnabled = enabled

        if (enabled) {
            buttonSnapUp.visible = true
            buttonSnapDown.visible = false
        } else {
            buttonSnapUp.visible = false
            buttonSnapDown.visible = false
        }
    }

    Flickable {
        id: flickableArea
        width: musicJukebox.appWidth
        height: musicJukebox.appHeight
        contentHeight: musicJukebox.contentHeight
        interactive: (musicJukebox.behavior != MusicJukebox.JukeboxCatalogMode
                      && musicJukebox.behavior != MusicJukebox.JukeboxRankMode)

        // This property is used to determine what area is visible and to
        // coordinate the snaps between the views. It changes among 0, 1, 2, 3.
        property int visibleView: Constants.FlickableArea.VisibleViewFirst

        Behavior on contentY {
            NumberAnimation {easing.type: "OutQuart"}
        }

        Rectangle {
            id: background
            width: flickableArea.width
            height: musicJukebox.contentHeight
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#ade044" }
                GradientStop { position: 0.5; color: "#57922a" }
                GradientStop { position: 1.0; color: "#224018" }
            }

            Image {
                anchors.fill: parent
                fillMode: "Tile"
                source: "qrc:/data/images/dots.png"
                visible: {
                    if (musicJukebox.platform == MusicJukebox.MobilePlatform)
                        return true
                    else
                        return false
                }
            }

            Image {
                id: topLeftCorner
                source: "qrc:/data/images/bord.png"
                anchors.top: parent.top
                rotation: -90
                anchors.left: parent.left
            }

            Image {
                id: topRightCorner
                source: "qrc:/data/images/bord.png"
                anchors.top: parent.top
                anchors.right: parent.right
            }

            Image {
                id: bottomLeftCorner
                source: "qrc:/data/images/bord.png"
                rotation: 180
                anchors.bottom: parent.bottom
                anchors.left: parent.left
            }

            Image {
                id: bottomRightCorner
                source: "qrc:/data/images/bord.png"
                rotation: 90
                anchors.bottom: parent.bottom
                anchors.right: parent.right
            }
        }

        // These areas are used to snap the views
        Rectangle {
            id: area0
            anchors.top: parent.top
            height: musicJukebox.appHeight
            width: parent.width
            opacity: 0.0
        }

        Player {
            id: player
            anchors.fill: area0
        }

        Item {
            id: area1
            anchors.top: {
                if (musicJukebox.behavior == MusicJukebox.JukeboxClientMode
                    || musicJukebox.behavior == MusicJukebox.JukeboxRankMode)
                    return parent.top
                else
                    return area0.top
            }
            anchors.topMargin: 0.57 * area0.height
            height: musicJukebox.appHeight
            width: parent.width
        }

        Rectangle {
            anchors.top: parent.top
            anchors.bottom: {
                if (musicJukebox.behavior == MusicJukebox.JukeboxClientMode
                    || musicJukebox.behavior == MusicJukebox.JukeboxRankMode)
                    return area1.top
                else if (musicJukebox.behavior == MusicJukebox.JukeboxCatalogMode)
                    return area3.top
                else
                    return area0.top
            }
            width: parent.width
            color: "black"
            visible: (musicJukebox.behavior != MusicJukebox.JukeboxDjMode)
        }

        Item {
            id: area2
            anchors.top: area0.top
            anchors.topMargin: 0.82 * area0.height
            height: musicJukebox.appHeight
            width: parent.width
        }

        RankedList {
            id: rankedList
            anchors.fill: area2
            interactive: true
        }

        Item {
            id: area3
            anchors.bottom: parent.bottom
            height: musicJukebox.appHeight
            width: parent.width
        }

        Catalog {
            id: catalog
            anchors.fill: area3
            onModalmode : {
                enableSnap(!enabled)
            }
        }

        Connections {
            target: flickableArea
            onMovementEnded: updateVisibleArea()
            onContentYChanged: {
                if (flickableArea.flicking)
                    updateVisibleArea()
            }
        }

        Connections {
            target: rankedList
            onMovementEnded: {
                if (flickableArea.visibleView != 2 && musicJukebox.behavior != MusicJukebox.JukeboxDjMode)
                    rankedList.interactive = false
            }
        }

        Connections {
            target: musicJukebox
            onContentHeightChanged: {
                setCurrentViewArea(currentArea)
            }
        }
    }

    Button {
        id: buttonSnapUp
        defaultSrc: "qrc:/data/images/bt_snap_up.png"
        pressedSrc: "qrc:/data/images/bt_snap_up.png"
        anchors.top: flickableArea.top
        anchors.topMargin: flickableArea.height * 0.046
        x: musicJukebox.appWidth - 60
        width: 40
        height: 40
        visible: false

        onClicked: {
            setCurrentViewArea(currentArea - 1)
        }
    }

    Button {
        id: buttonSnapDown
        defaultSrc: "qrc:/data/images/bt_snap_down.png"
        pressedSrc: "qrc:/data/images/bt_snap_down.png"

        x: musicJukebox.appWidth - 60
        anchors.bottom: flickableArea.bottom
        anchors.bottomMargin: flickableArea.height * 0.05
        width: 40
        height: 40
        visible: false

        onClicked: {
            setCurrentViewArea(currentArea + 1)
        }
    }


    GeneralDialog {
        id: voteDialog
        anchors.fill: parent
        visible: false

        property string songId: "A1B1"
        property string songName: "Song Name"
        property string artistName: "Artist"
        property bool djMode: musicJukebox.behavior === MusicJukebox.JukeboxDjMode

        numberOfButtons: djMode ? 3 : 2

        buttonOneText: "vote"
        buttonTwoText: djMode ? "remove" : "cancel"
        buttonThreeText: djMode ? "cancel" : ""
        defaultButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_normal.png"
        pressedButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_pressed.png"
        defaultButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_normal.png"
        pressedButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_pressed.png"

        text: {
            if (voteDialog.artistName != "")
                return "Do you want to vote on <br><b>" + voteDialog.songName + "</b> by <b>" + voteDialog.artistName + "</b>?"
            else
                return "Do you want to vote on <br><b>" + voteDialog.songName + "</b>?"
        }
    }

    GeneralDialog {
        id: confirmationDialog
        anchors.fill: parent
        visible: false

        numberOfButtons: 2
        buttonOneText: "yes"
        buttonTwoText: "no"

        text: "This song will be removed from the line up view. All votes received to this song will be reseted. " +
              "Do you want to proceed?"
    }

    Connections {
        target: rankedList
        onItemClicked: {
            if (musicJukebox.behavior != MusicJukebox.JukeboxDjMode
                && musicJukebox.behavior != MusicJukebox.JukeboxClientMode)
                return;
            //XXX We are passing a number, but we could pass something more readable
            var id = rankedModel.getDataByIndex(index, 0)
            var songTitle = rankedModel.getDataByIndex(index, 1)
            var artistName = rankedModel.getDataByIndex(index, 2)
            showVoteDialog(id, songTitle, artistName)
        }
    }

    Connections {
        target: voteDialog
        onButtonOneClicked: {
            musicJukebox.sendVote(voteDialog.songId)
        }
        onButtonTwoClicked: {
            // The 2nd button is 'remove' in Dj mode and 'cancel' in Client mode
            if (musicJukebox.behavior === MusicJukebox.JukeboxDjMode)
                confirmationDialog.visible = true
            else
                voteDialog.visible = false
        }
    }

    Connections {
        target: confirmationDialog
        onButtonOneClicked: musicJukebox.removeMusic(voteDialog.songId)
    }
}
