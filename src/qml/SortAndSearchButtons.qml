/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: sortAndSearchButtons
    property int spacing: 10
    property int topMargin: 7 
    width: musicJukebox.appWidth > 864 ? 56 : 0.065 * musicJukebox.appWidth
    height: sortAndSearchButtons.width

    signal clicked(int state)

    Item {
        id: sortByArtistButton
        width: musicJukebox.appHeight > 480 ? 56 : 0.12 * musicJukebox.appHeight
        height: sortByArtistButton.width

        Image {
            id: sortByArtistImgClicked
            width: parent.width
            smooth: musicJukebox.appHeight > 480 ? false : true
	    source: "qrc:/data/images/icon_artist_clicked.png"
            anchors.left: parent.left
        }

        Image {
            id: sortByArtistImgNormal
            width: parent.width
            smooth: musicJukebox.appHeight > 480 ? false : true
            source: "qrc:/data/images/icon_artist_normal.png"
            anchors.left: parent.left
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                sortAndSearchButtons.state = "sortedByArtist"
                sortAndSearchButtons.clicked(0)
            }
        }
    }

    Item {
        id: sortByArtistText
        anchors.left: sortByArtistButton.right
        anchors.leftMargin: spacing

        Text {
            id: sortByArtistText1
            text: "SORTED BY"
            color: "#FFFFFF"
            font { family: nokiaPureRegular.name; pixelSize: sortByArtistButton.width * 0.29 }
            smooth: true
        }

        Text {
            id: sortByArtistText2
            text: "ARTIST NAME"
            color: "#FFFFFF"
            font { pixelSize: sortByArtistButton.width * 0.29; family: nokiaPureBold.name; weight: Font.Bold }
            anchors.top: sortByArtistText1.bottom
        }

        Component.onCompleted: {
            sortByArtistText.width = sortByArtistText2.paintedWidth
            sortByArtistText.height = sortByArtistText1.height + sortByArtistText2.height
        }
    }

    Item {
        id: sortBySongButton
        anchors.leftMargin: spacing
        width: musicJukebox.appHeight > 480 ? 56 : 0.12 * musicJukebox.appHeight
        height: sortBySongButton.width

        Image {
            id: sortBySongImgClicked
            width: parent.width
            smooth: musicJukebox.appHeight > 480 ? false : true
            source: "qrc:/data/images/icon_song_clicked.png"
            anchors.left: parent.left
        }

        Image {
            id: sortBySongImgNormal
            width: parent.width
            smooth: musicJukebox.appHeight > 480 ? false : true
            source: "qrc:/data/images/icon_song_normal.png"
            anchors.left: parent.left
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                sortAndSearchButtons.state = "sortedBySongName"
                sortAndSearchButtons.clicked(1)
            }
        }
    }

    Item {
        id: sortBySongText
        x: sortBySongButton.width * 2 + spacing * 2

        Text {
            id: sortBySongText1
            text: "SORTED BY"
            color: "#FFFFFF"
            font { family: nokiaPureRegular.name; pixelSize: sortBySongButton.width * 0.29 }
        }

        Text {
            id: sortBySongText2
            text: "SONG NAME"
            color: "#FFFFFF"
            font { pixelSize: sortBySongButton.width * 0.29; family: nokiaPureBold.name; weight: Font.Bold }
            anchors.top: sortBySongText1.bottom
        }

        Component.onCompleted: {
            sortBySongText.width = sortBySongText1.paintedWidth
            sortBySongText.height = sortBySongText1.paintedHeight + sortBySongText2.height
        }
    }

    states: [
        State {
            name: "sortedByArtist"
            PropertyChanges {
                target: sortByArtistImgNormal
                opacity: 0
            }

            PropertyChanges {
                target: sortByArtistImgClicked
                opacity: 1
            }

            PropertyChanges {
                target: sortByArtistText
                opacity: 1
                y: topMargin
            }

            PropertyChanges {
                target: sortBySongImgNormal
                opacity: 1
            }

            PropertyChanges {
                target: sortBySongImgClicked
                opacity: 0
            }

            AnchorChanges {
                target: sortBySongButton
                anchors.left: sortByArtistText.right
            }

            PropertyChanges {
                target: sortBySongText
                opacity: 0
                y: sortBySongButton.height
            }
        },

        State {
            name: "sortedBySongName"
            PropertyChanges {
                target: sortByArtistImgClicked
                opacity: 0
            }

            PropertyChanges {
                target: sortByArtistImgNormal
                opacity: 1
            }

            PropertyChanges {
                target: sortByArtistText
                opacity: 0
                y: -1 * (sortByArtistText.height + topMargin)
            }

            PropertyChanges {
                target: sortBySongImgNormal
                opacity: 0
            }


            PropertyChanges {
                target: sortBySongImgClicked
                opacity: 1
            }

            AnchorChanges {
                target: sortBySongButton
                anchors.left: sortByArtistButton.right
            }

            PropertyChanges {
                target: sortBySongText
                opacity: 1
                y: topMargin
            }
        }
    ]

    transitions: Transition {
        // smoothly reanchor myRect and move into new position
        PropertyAnimation { properties: "opacity,y"; easing.type: Easing.InOutQuad }
        AnchorAnimation {}
    }

    Component.onCompleted: sortAndSearchButtons.state = "sortedByArtist"
}
