/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: generalDialog
    anchors.fill: parent
    z: 666

    property string title: ""
    property string titleHorizontalAlignment: "AlignHCenter"
    property int titleFontPixelSize: 0.11 * background.height
    property string titleFontFamily: nokiaPureRegular.name
    property bool titleFontBold: true

    property int spacing: 10

    property string text: ""
    property string textHorizontalAlignment: "AlignHCenter"
    property int textFontPixelSize: 0.07 * background.height
    property string textFontFamily: nokiaPureRegular.name
    property bool textFontBold: false

    property int numberOfButtons: 3

    property string buttonOneText: "one"
    property string defaultButtonOneIconSrc: ""
    property string pressedButtonOneIconSrc: ""
    property bool buttonOneEnabled: true

    property string buttonTwoText: "two"
    property string defaultButtonTwoIconSrc: ""
    property string pressedButtonTwoIconSrc: ""
    property bool buttonTwoEnabled: true

    property string buttonThreeText: "three"
    property string defaultButtonThreeIconSrc: ""
    property string pressedButtonThreeIconSrc: ""
    property bool buttonThreeEnabled: true

    property int maximumHeight: 380
    property int maximumWidth: 700

    signal buttonOneClicked()
    signal buttonTwoClicked()
    signal buttonThreeClicked()

    Rectangle {
        id: overlay
        color: "black"
        opacity: 0.5
        anchors.fill: parent

        MouseArea {
            anchors.fill: parent
        }
    }

    Image {
        id: background
        source: "qrc:/data/images/bg_dialog.png"
        anchors.centerIn: parent
        width: {
            var result = 0.81 * parent.width
            if (result > maximumWidth) {
                return maximumWidth
            } else {
                return result
            }
        }
        height: {
            var result = 0.79 * parent.height
            if (result > maximumHeight) {
                return maximumHeight
            } else {
                return result
            }
        }
        MouseArea { anchors.fill: parent }
    }

    Flickable {
        id: flickable
        clip: true
        contentHeight: titleArea.paintedHeight + spacing + contents.paintedHeight
        anchors.top: background.top
        anchors.topMargin: 30
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.left: background.left
        anchors.leftMargin: 30
        anchors.bottom: buttons.top
        anchors.bottomMargin: 20

        Text {
            id: titleArea
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: parent.left
            horizontalAlignment: titleHorizontalAlignment

            font.pixelSize: titleFontPixelSize
            font.family: titleFontFamily
            font.bold: titleFontBold

            wrapMode: "WordWrap"
            color: "#314d1f"
            text: title
        }

        Text {
            id: contents
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: titleArea.bottom
            anchors.topMargin: spacing
            horizontalAlignment: textHorizontalAlignment

            font.pixelSize: textFontPixelSize
            font.family: textFontFamily
            font.bold: textFontBold

            color: "#314d1f"
            wrapMode:"WordWrap"
            textFormat: "StyledText"
            text: generalDialog.text
        }
    }

    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            scrollbar.opacity = flickable.moving ? 1 : 0
        }
    }

    Scrollbar {
        id: scrollbar
        clip: true
        opacity: flickable.moving ? 1 : 0
        anchors.top: flickable.top
        anchors.bottom: flickable.bottom
        anchors.right: flickable.right
        yPosition: flickable.visibleArea.yPosition
        heightRatio: flickable.visibleArea.heightRatio

        Behavior on opacity {
            NumberAnimation {
                duration: 500
                easing.type: Easing.OutCirc
            }
        }
    }

    Connections {
        target: generalDialog
        onVisibleChanged: {
            if (generalDialog.visible) {
                scrollbar.opacity = 1
                timer.start()
            } else {
                scrollbar.opacity = 0
            }
        }
    }

    Connections {
        target: flickable
        onMovingChanged: scrollbar.opacity = flickable.moving ? 1 : 0
    }

    Component {
        id: repeaterButtonDelegate

        Item {
            width: background.width / 3
            height: background.height * 0.145

            Button {
                id: generalButton
                anchors.centerIn: parent

                width: background.width * 0.246
                height: background.height * 0.145

                // Button is visible depending on the number of buttons
                visible: index < numberOfButtons

                defaultSrc: "qrc:/data/images/bt_small_normal.png"
                pressedSrc: "qrc:/data/images/bt_small_pressed.png"
                defaultIconSrc: {
                    if (index === 0)
                        return defaultButtonOneIconSrc
                    else if (index === 1)
                        return defaultButtonTwoIconSrc
                    else if (index === 2)
                        return defaultButtonThreeIconSrc
                }
                pressedIconSrc: {
                    if (index === 0)
                        return pressedButtonOneIconSrc
                    else if (index === 1)
                        return pressedButtonTwoIconSrc
                    else if (index === 2)
                        return pressedButtonThreeIconSrc
                }

                margin: 5
                defaultTextColor: "#a18e36"
                pressedTextColor: "#ffdf3e"

                buttonText: {
                    if (index === 0)
                        return buttonOneText
                    else if (index === 1)
                        return buttonTwoText
                    else if (index === 2)
                        return buttonThreeText
                }

                enabled: {
                    if (index === 0)
                        return buttonOneEnabled
                    else if (index === 1)
                        return buttonTwoEnabled
                    else if (index === 2)
                        return buttonThreeEnabled
                }

                onClicked: {
                    if (index === 0)
                        buttonOneClicked()
                    else if (index === 1)
                        buttonTwoClicked()
                    else if (index === 2)
                        buttonThreeClicked()

                    generalDialog.visible = false
                }
            }
        }
    }

    Row {
        id: buttons
        anchors.horizontalCenter:  background.horizontalCenter
        anchors.bottom: background.bottom
        anchors.bottomMargin: 0.05 * background.height
        spacing: background.width * 0.023

        Repeater {
            model: numberOfButtons
            delegate: repeaterButtonDelegate
        }
    }
}
