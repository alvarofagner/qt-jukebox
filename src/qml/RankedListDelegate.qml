/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Component {
    id: rankedListDelegate

    Item {
        id: wrapper
        height: itemBg.height * 1.05
        width: musicJukebox.appWidth
        z: mainList.count - index

        function updateItemOpacity() {
            var distance
            var threshold = 40
            var itemHeight = 50

            if (wrapper.pos.y <= (mainList.contentY + itemHeight)) {
                distance = (wrapper.pos.y - mainList.contentY)
            } else if ((wrapper.pos.y + itemHeight) >= (mainList.contentY + mainList.height - threshold)) {
                distance = (mainList.contentY + mainList.height - (wrapper.pos.y + itemHeight))
            } else {
                distance = threshold
            }

            var newOpacity = distance/threshold
            if (newOpacity >= 1)
                newOpacity = 1
            else if (newOpacity <= 0)
                newOpacity = 0

            if (newOpacity != wrapper.opacity)
                wrapper.opacity = newOpacity
        }

        Image {
            id: itemBorder
            source: "qrc:/data/images/label_roundedbord.png"
            anchors.right: songTitle.left
        }

        Image {
            id: listArrow
            anchors.right: disc.left
            anchors.rightMargin: 6
            source: "qrc:/data/images/label_arrow.png"
        }

        Text {
            id: rankPosition
            anchors.fill: listArrow
            anchors.rightMargin: 9
            anchors.bottomMargin: 2
            verticalAlignment: "AlignVCenter"
            horizontalAlignment: "AlignHCenter"
            color: "#314d1f"
            font { pixelSize: wrapper.height * 0.3; family: nokiaPureBold.name; bold: true }
            text: index + 1
        }

        Image {
            id: itemBg
            anchors.left: itemBorder.right
            anchors.right: listArrow.left
            fillMode: "TileHorizontally"
            source: "qrc:/data/images/label_tile.png"
        }

        // this copy is used to know the song title width
        Text {
            id: songTitleShadowCopy
            visible: false
            anchors.right: songTitle.left
            anchors.top: songTitle.top
            horizontalAlignment: songTitle.horizontalAlignment
            verticalAlignment: songTitle.verticalAlignment
            font: songTitle.font
            elide: songTitle.elide
            text: songTitle.text
        }

        Text {
            id: songTitle
            anchors.right: listArrow.left
            anchors.top: parent.top
            anchors.topMargin: 5
            horizontalAlignment: "AlignLeft"
            verticalAlignment: "AlignBottom"
            color: "white"
            font { family: nokiaPureBold.name; pixelSize: wrapper.height * 0.28; bold: true; italic: true }
            elide: "ElideRight"
            text: SongId + " - \"" + SongTitle + "\""
            width: Math.min(listArrow.x - 40, songTitleShadowCopy.paintedWidth)
        }

        Text {
            id: artistName
            anchors.top: songTitle.bottom
            anchors.left: songTitle.left
            anchors.leftMargin: 10
            anchors.right: songTitle.right
            anchors.bottom: itemBg.bottom
            horizontalAlignment: "AlignLeft"
            verticalAlignment: "AlignTop"
            color: "#afe044"
            font { pixelSize: wrapper.height * 0.22; italic: true; family: nokiaPureRegular.name }
            elide: "ElideRight"
            text: "" + ArtistName
        }

        Image {
            id: disc
            anchors.right: parent.right
            anchors.rightMargin: 28
            anchors.verticalCenter: parent.verticalCenter
            source: "qrc:/data/images/disc_piece.png"
        }

        Connections {
            target: wrapper
            onYChanged: updateItemOpacity()
        }

        Connections {
            target: mainList
            onContentYChanged: updateItemOpacity()

        }

        Connections {
            target: mainList.visibleArea
            onHeightRatioChanged: updateItemOpacity()
        }
    }
}
