/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: about
    anchors.fill: parent

    property int maximumHeight: 380
    property int maximumWidth: 700

    property int spacing: 10

    Rectangle {
        id: overlay
        color: "black"
        opacity: 0.5
        anchors.fill: parent

        MouseArea {
            anchors.fill: parent
            onClicked: about.visible = false
        }
    }

    Image {
        id: background
        source: "qrc:/data/images/bg_dialog.png"
        anchors.centerIn: parent
        width: {
            var result = 0.81 * parent.width
            if (result > maximumWidth) {
                return maximumWidth
            } else {
                return result
            }
        }
        height: {
            var result = 0.79 * parent.height
            if (result > maximumHeight) {
                return maximumHeight
            } else {
                return result
            }
        }

        MouseArea { anchors.fill: parent }
    }

    Flickable {
        id: flickableArea
        clip: true
        contentHeight: name.paintedHeight + version.paintedHeight
                       + contact.paintedHeight + warning.paintedHeight
                       + spacing * 2
        anchors.top: background.top
        anchors.topMargin: 30
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.left: background.left
        anchors.leftMargin: 30
        anchors.bottom: aboutButton.top
        anchors.bottomMargin: 20

        Text {
            id: name
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            font { family: nokiaPureBold.name; pixelSize: 0.11 * background.height; bold: true }
            color: "#314d1f"
            text: "Qt Jukebox"
        }

        Text {
            id: version
            anchors.top: name.bottom
            anchors.topMargin: spacing
            anchors.left: name.left
            font { family: nokiaPureRegular.name; pixelSize: 0.06 * background.height; bold: true }
            color: "#314d1f"
            wrapMode: Text.Wrap
            text: "Version:"
        }

        Text {
            id: beta
            anchors.top: version.top
            anchors.left: version.right
            anchors.right: parent.right
            anchors.leftMargin: 10
            font { family: nokiaPureRegular.name; pixelSize: 0.06 * background.height }
            color: "#314d1f"
            wrapMode: Text.Wrap
            text: "1.0 Beta"
        }

        Text {
            id: contact
            anchors.top: version.bottom
            anchors.left: name.left
            font { family: nokiaPureRegular.name; pixelSize: 0.06 * background.height; bold: true }
            color: "#314d1f"
            wrapMode: Text.Wrap
            text: "Contact:"
        }

        Text {
            id: link
            anchors.top: contact.top
            anchors.left: contact.right
            anchors.right: parent.right
            anchors.leftMargin: 10
            font { family: nokiaPureRegular.name; pixelSize: 0.06 * background.height }
            color: "#314d1f"
            wrapMode: Text.Wrap
            text: "http://qt.nokia.com/about/contact-us"
        }

        Text {
            id: warning
            anchors.top: contact.bottom
            anchors.topMargin: spacing
            anchors.left: name.left
            anchors.right: parent.right
            font { family: nokiaPureRegular.name; pixelSize: 0.06 * background.height }
            wrapMode: Text.Wrap
            color: "#314d1f"
            text: "Your use of the Qt Jukebox application is submit to the terms of the Nokia Corporation " +
                  "End User Software License Agreement – Qt Jukebox Application v. 1.0. This beta version " +
                  "of the Qt Jukebox Application is being distributed so that users may test the product. " +
                  "This beta version is not of production quality and users are advised that any use is at " +
                  "their own risk."
        }
    }

    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            scrollbar.opacity = flickableArea.moving ? 1 : 0
        }
    }

    Scrollbar {
        id: scrollbar
        clip: true
        opacity: flickableArea.moving ? 1 : 0
        anchors.top: flickableArea.top
        anchors.bottom: flickableArea.bottom
        anchors.right: flickableArea.right
        yPosition: flickableArea.visibleArea.yPosition
        heightRatio: flickableArea.visibleArea.heightRatio

        Behavior on opacity {
            NumberAnimation {
                duration: 500
                easing.type: Easing.OutCirc
            }
        }
    }

    Connections {
        target: about
        onVisibleChanged: {
            if (about.visible) {
                scrollbar.opacity = 1
                timer.start()
            } else {
                scrollbar.opacity = 0
            }
        }
    }

    Connections {
        target: flickableArea
        onMovingChanged: scrollbar.opacity = flickableArea.moving ? 1 : 0
    }

    Button {
        id: aboutButton

        defaultSrc: "qrc:/data/images/bt_small_normal.png"
        pressedSrc: "qrc:/data/images/bt_small_pressed.png"

        width: musicJukebox.appWidth > 864 ? 171 : createPartyScreen.width * 0.2
        height: musicJukebox.appHeight > 480 ? 56 : createPartyScreen.height * 0.11

        buttonText: "ok"
        margin: 5
        textSize: background.height * 0.05
        defaultTextColor: "#a18e36"
        pressedTextColor: "#ffdf3e"

        anchors.bottom: background.bottom
        anchors.bottomMargin: 0.05 * background.height
        anchors.horizontalCenter: background.horizontalCenter

        onClicked: {
            about.visible = false;
        }
    }
}
