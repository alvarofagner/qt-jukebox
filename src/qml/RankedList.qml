/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: rankedList
    anchors.fill: parent

    property alias interactive: mainList.interactive
    property alias moving: mainList.moving

    signal movementEnded
    signal itemClicked(int index)

    HashTag {
        id: hashTag
        anchors.right: parent.right
        anchors.rightMargin: 310
        anchors.top: parent.top
        anchors.topMargin: 8
    }

    RankedListDelegate {
        id: rankedListDelegate
    }

    Component {
        id: emptyItemHeader

        Rectangle {
            height: 55
            width: rankedList.width
            visible: false
        }
    }

    Text {
        id: offlineMsg
        text: "* offline"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.leftMargin: 20
        font { family: nokiaPureBold.name; pixelSize: 22; bold: true; italic: true }
        color: "#DDD"
        visible: !musicJukebox.connected
    }

    Item {
        id: message
        anchors.verticalCenter: parent.verticalCenter
        x: parent.width/2 - text.width/2
        visible: musicJukebox.catalogEmpty

        Text {
            id: text
            text: "Ranked list is empty..."
            font { family: nokiaPureBold.name; pixelSize: 22; bold: true }
            color: "white"
            clip: true
        }
    }

    Component {
        id: emptyItemFooter

        Rectangle {
            height: 50
            width: rankedList.width
            visible: false
        }
    }

    ListView {
        id: mainList
        anchors.fill: parent
        model: rankedModel
        delegate: rankedListDelegate
        snapMode: "SnapToItem"
        header: emptyItemHeader
        footer: emptyItemFooter
        flickDeceleration: 300
        maximumFlickVelocity: 500
        cacheBuffer: 50
        opacity: musicJukebox.connected ? 1 : 0.5
        onMovementEnded: rankedList.movementEnded()
    }

    MouseArea {
        anchors.fill: parent
        anchors.rightMargin: 278
        onClicked: {
            var index = mainList.indexAt(mouseX, mainList.contentY + mouseY)
            itemClicked(index)
        }
    }
}
