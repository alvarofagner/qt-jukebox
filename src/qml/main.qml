/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Rectangle {
    clip: true
    width: musicJukebox.appWidth
    height: musicJukebox.appHeight
    color: "black"
    property bool resumeCatalog: true

    function loadSelectedCatalog(catalogPath) {
        if (catalogPath === "" || resumeCatalog) {
            return
        }
        musicJukebox.loadCatalog(catalogPath)
    }

    function showMessageDialog(title, text) {
        messageDialog.title = title
        messageDialog.text = text
        messageDialog.visible = true
    }

    FontLoader {
        id: nokiaPureRegular
        source: "qrc:/data/fonts/NokiaPureText.ttf"
    }

    FontLoader {
        id: nokiaPureBold
        source: "qrc:/data/fonts/NokiaPureTextBold.ttf"
    }

    JukeboxMainView {
        id: jukeboxMainView
        anchors.fill: parent
        currentArea: musicJukebox.behavior == MusicJukebox.JukeboxRankMode ? 1 : 3
    }

    Rectangle {
        id: mainBackground
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#a0db3e" }
            GradientStop { position: 1.0; color: "#5ca92e" }
        }

        Image {
            anchors.fill: parent
            fillMode: "Tile"
            source: "qrc:/data/images/dots.png"
            visible: musicJukebox.platform == MusicJukebox.MobilePlatform ? true : false
        }

        Image {
            id: topLeftCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            rotation: -90
            anchors.left: parent.left
        }

        Image {
            id: topRightCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            anchors.right: parent.right
        }

        Image {
            id: bottomLeftCorner
            source: "qrc:/data/images/bord.png"
            rotation: 180
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }

        Image {
            id: bottomRightCorner
            source: "qrc:/data/images/bord.png"
            rotation: 90
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    StartupScreen {
        id: startupScreen
        anchors.fill: parent
        visible: true
    }

    CreatePartyScreen {
        id: createPartyScreen
        anchors.fill: parent
        visible: false
    }

    GeneralDialog {
        id: messageDialog
        anchors.fill: parent
        visible: false

        numberOfButtons: 1
        buttonOneText: "ok"
        onButtonOneClicked: {
            messageDialog.visible = false
        }
    }

    ImportDialog {
        id: importDialog
        anchors.fill: parent
        visible: false
        onCancel: {
            importDialog.visible = false
            actualParent.visible = true
        }
        onFilesSelected: {
            importDialog.visible = false
            actualParent.visible = true
            musicJukebox.loadCatalog(paths)
        }
    }

    SelectPartyScreen {
        id: selectPartyScreen
        anchors.fill: parent
        visible: false
    }

    SplashScreen {
        id: splashScreen
    }

    Timer {
        id: timer
        interval: 2000
        running: true
        onTriggered: {
            splashScreen.opacity = 0
        }
    }

    Connections {
        target: startupScreen
        onExit: {
            mainBackground.visible = false
        }
        onAutoPlayClicked: {
            musicJukebox.playingMode = MusicJukebox.AutoPlay
        }
        onDjProClicked: {
            musicJukebox.playingMode = MusicJukebox.DjPro
        }
        onPrivateClicked: {
            createPartyScreen.visible = true
        }
        onSelectPartySelected: {
            showMessageDialog("Attention please",
                              "You need to be in the same wireless network as the DJ")
            selectPartyScreen.visible = true
            selectPartyScreen.showLoading()
        }
        onTwitterSetupReady: {
            createPartyScreen.visible = true
        }
        onTwitterAuthenticationError: {
            showMessageDialog(reason, description)
        }
    }

    Connections {
        target: createPartyScreen
        onCancel: {
            startupScreen.visible = true
            resumeCatalog = true
        }
        onCreate: {
            startupScreen.visible = false
            createPartyScreen.visible = false
            mainBackground.visible = false
            console.log("Party name: " + partyName)
            // Importing selected catalog
            loadSelectedCatalog(importDialog.filePath)
            musicJukebox.createParty(partyName)
        }
        onImportSongs: {
            importDialog.visible = true
            importDialog.actualParent = createPartyScreen
            createPartyScreen.visible = false
            resumeCatalog = false
        }
    }

    Connections {
        target: jukeboxMainView
        onImportSongs: {
            importDialog.visible = true
            importDialog.actualParent = jukeboxMainView
            jukeboxMainView.visible = false
            resumeCatalog = false
        }
    }

    Connections {
        target: selectPartyScreen
        onCancel: {
            startupScreen.visible = true
        }
        onRefresh: {

        }
        onPartySelected: {
            console.log("Party selected: " + port + " " + address)
            startupScreen.visible = false
            createPartyScreen.visible = false
            mainBackground.visible = false
            musicJukebox.connectClientSocket(uuid)
        }
    }

    Connections {
        target: musicJukebox
        onShowMessageDialog: {
            showMessageDialog(title, text)
        }
        onBehaviorChanged: {
            if (musicJukebox.behavior == MusicJukebox.JukeboxClientMode) {
                jukeboxMainView.setCurrentViewArea(3)
                jukeboxMainView.enableSnap(true)
            } else if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode) {
                jukeboxMainView.setCurrentViewArea(3)
                jukeboxMainView.rankedListInteractive = true
                jukeboxMainView.enableSnap(false)
            } else if (musicJukebox.behavior == MusicJukebox.JukeboxCatalogMode) {
                jukeboxMainView.setCurrentViewArea(3)
                jukeboxMainView.enableSnap(false)
            } else if (musicJukebox.behavior == MusicJukebox.JukeboxRankMode) {
                jukeboxMainView.setCurrentViewArea(1)
                jukeboxMainView.enableSnap(false)
            }
        }
    }
}
