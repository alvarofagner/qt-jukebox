/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: playerItem

    property bool playing: false
    property int currentTime: 0
    property int totalTime: 1
    property int imageDiscIndex: 0
    property bool changingDiscs: false

    signal error(string errorString)

    function updatePlayButton() {
        if (playing) {
            discTimer.start()
            playerItem.playing = true
        } else {
            playerItem.playing = false
            discTimer.stop()
        }
    }

    function enableNextButton() {
        nextButton.enabled = true
        nextButton.opacity = 1
    }

    function setSourceImage() {
        if (playerItem.imageDiscIndex >= 23) {
            playerItem.imageDiscIndex = 0
        } else {
            playerItem.imageDiscIndex++
        }
    }

    Timer {
        id: discTimer
        interval: 42
        running: false
        repeat: true
        onTriggered: setSourceImage()
    }

    Image {
        id: plate
        anchors.right: parent.right
        anchors.rightMargin: musicJukebox.appWidth > 864 ? 17 : (2 / 100) * musicJukebox.appWidth
        y: {
            if (musicJukebox.behavior != MusicJukebox.JukeboxRankMode)
                return area1.y - 0.7 * plate.height
            else
                return area1.y - plate.height / 2
        }
        source: "qrc:/data/images/plate.png"
    }

    Image {
        id: nowPlayingDisc
        source: "qrc:/data/images/disc_large.png"
        anchors.verticalCenter: plate.verticalCenter
        anchors.horizontalCenter: plate.horizontalCenter
        visible: true
    }

    Item {
        id: discImagesRepeater
        anchors.centerIn: plate
        width: nowPlayingDisc.width
        height: nowPlayingDisc.height

        Repeater {
            model: 24
            delegate: Image {
                z: 2
                anchors.centerIn: parent
                source: "qrc:/data/images/disc_" + (index + 1) + ".png"
                visible: (playerItem.imageDiscIndex == index) && !playerItem.changingDiscs
            }
        }
    }

    Image {
        id: needle
        source: "qrc:/data/images/needle.png"
        anchors.right: parent.right
        anchors.rightMargin: musicJukebox.appWidth > 864 ? -60 : -0.07 * musicJukebox.appWidth
        anchors.bottom: plate.verticalCenter
        anchors.bottomMargin: musicJukebox.appHeight > 480 ? -100 : -0.208 * musicJukebox.appHeight
        z: 3
        smooth: true

        transform {
            Rotation {
                id: needleRotation
                origin.x: needle.width
                origin.y: 0
                axis { x: 0; y: 0 ; z: 1 }
                angle: -75
                Behavior on angle {
                    PropertyAnimation {
                        id: needleAnimation
                        duration: 700
                    }
                }
            }
        }

        SequentialAnimation {
            id: nextSongAnimation

            // Stops current song and restart progress bar
            ScriptAction {
                script: {
                    if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode)
                        musicJukebox.stopSong()

                    if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode ||
                        musicJukebox.behavior == MusicJukebox.JukeboxRankMode) {
                        progressBar.visible = false
                        playerItem.totalTime = -1
                    }
                }
            }

            // Stop disc rotation
            PropertyAction { target: discTimer; property: "running"; value: false }

            // Take out needle
            PropertyAnimation {
                target: needleRotation
                property: "angle"
                to: -75
                duration: needleAnimation.duration
            }

            // Move out the disc
            PropertyAction { target: playerItem; property: "changingDiscs"; value: true }
            PropertyAction { target: nowPlayingDisc; property: "smooth"; value: true }
            PropertyAction { target: progressBar; property: "smooth"; value: true }
            PropertyAction { target: progressBar; property: "visible"; value: false }
            ScriptAction { script: { nowPlayingDisc.anchors.verticalCenter = undefined } }
            ParallelAnimation {
                NumberAnimation {
                    easing.type: Easing.InQuad
                    target: nowPlayingDisc
                    property: "y"
                    to: nowPlayingDisc.y - nowPlayingDisc.height
                    duration: 500
                }
                NumberAnimation {
                    target: nowPlayingDisc
                    property: "opacity"
                    to: 0
                    duration: 500
                }
            }

            // Move in the same disc as it was a new one
            ScriptAction { script: { nowPlayingDisc.y = plate.y + 17 + nowPlayingDisc.height } }
            ParallelAnimation {
                NumberAnimation {
                    easing.type: Easing.OutQuad
                    target: nowPlayingDisc
                    property: "y"
                    to: plate.y + 17
                    duration: 500
                }
                NumberAnimation {
                    easing.type: Easing.OutQuad
                    target: nowPlayingDisc
                    property: "opacity"
                    to: 1
                    duration: 500
                }
            }

            // Reset original animation and player settings
            ScriptAction { script: { nowPlayingDisc.anchors.verticalCenter =  plate.verticalCenter } }
            PropertyAction { target: progressBar; property: "visible"; value: true }
            PropertyAction { target: nowPlayingDisc; property: "smooth"; value: false }
            PropertyAction { target: progressBar; property: "smooth"; value: false }
            PropertyAction { target: playerItem; property: "changingDiscs"; value: false }

            // Start needle and disc rotation animation
            PropertyAnimation {
                target: needleRotation
                property: "angle"
                to: -8
                duration: needleAnimation.duration
            }
            PropertyAction { target: discTimer; property: "running"; value: true }

            // Start playing the new song
            ScriptAction {
                script: {
                    if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode)
                        musicJukebox.nextSong()

                    if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode ||
                        musicJukebox.behavior == MusicJukebox.JukeboxRankMode) {
                        progressBar.visible = true
                        playerItem.playing = true
                    }
                }
            }
        }
    }

    states {
        State {
            name: "playing"; when: playerItem.playing
            PropertyChanges { target: needleRotation; angle: -8 }
        }
        State {
            name: "stopped";
            PropertyChanges { target: needleRotation; angle: -75 }
        }
    }

    transitions {
        Transition {
            from: "stopped"
            to: "playing"
            reversible: true
        }
    }

    CircularProgress {
        id: progressBar
        anchors.fill: plate
        progress: playerItem.totalTime < 0 ? 0 : playerItem.currentTime / playerItem.totalTime
        clockWise: false
        barWidth: 20
        startPosition: CircularProgress.Up
        color: "#ffdc3c"

        Behavior on progress {
            NumberAnimation { easing.type: Easing.Linear; duration: 1000 }
        }
    }

    Item {
        id: playButton
        width: musicJukebox.appWidth > 864 ? 132 : 0.153 * musicJukebox.appWidth
        height: musicJukebox.appHeight > 480 ? 90 : 0.1875 * musicJukebox.appHeight

        anchors.left: parent.left
        anchors.leftMargin: (11 / 100) * musicJukebox.appWidth
        y: {
            if (musicJukebox.behavior != MusicJukebox.JukeboxDjMode &&
                musicJukebox.behavior != MusicJukebox.JukeboxRankMode)
                return area1.y - playButton.height
            else
                return area1.y - playButton.height/2
        }
        visible: musicJukebox.behavior == MusicJukebox.JukeboxDjMode

        Image {
            id: playPauseImage
            anchors.fill: parent
            smooth: true
            source: "qrc:/data/images/bt_play_normal.png"
        }

        states {
            State {
                name: "play_normal"; when: !playing && !playButtonMouseArea.pressed
                PropertyChanges { target: playPauseImage; source: "qrc:/data/images/bt_play_normal.png" }
            }

            State {
                name: "play_pressed"; when: !playing && playButtonMouseArea.pressed
                PropertyChanges { target: playPauseImage; source: "qrc:/data/images/bt_play_clicked.png" }
            }

            State {
                name: "pause_normal"; when: playing && !playButtonMouseArea.pressed
                PropertyChanges { target: playPauseImage; source: "qrc:/data/images/bt_pause_normal.png" }
            }

            State {
                name: "pause_pressed"; when: playing && playButtonMouseArea.pressed
                PropertyChanges { target: playPauseImage; source: "qrc:/data/images/bt_pause_clicked.png" }
            }
        }

        MouseArea {
            id: playButtonMouseArea
            anchors.fill: parent
            enabled: !needleAnimation.running

            onPressed: {
                playing = !playing
                musicJukebox.playPauseSong(playing)
                updatePlayButton()
                if(!nextButton.enabled)
                    enableNextButton()
            }
        }
    }

    Item {
        id: nextButton
        width: musicJukebox.appWidth > 864 ? 132 : 0.153 * musicJukebox.appWidth
        height: musicJukebox.appHeight > 480 ? 90 : 0.1875 * musicJukebox.appHeight
        visible: musicJukebox.behavior == MusicJukebox.JukeboxDjMode

        anchors.top: playButton.top
        anchors.left: playButton.right
        anchors.leftMargin: 10
        opacity: 0.3
        enabled: false

        Image {
            id: nextNormal
            anchors.fill: parent
            smooth: true
            source: "qrc:/data/images/bt_next_normal.png"
        }

        Image {
            id: nextClicked
            anchors.fill: parent
            smooth: true
            source: "qrc:/data/images/bt_next_clicked.png"
            visible: false
        }

        MouseArea {
            anchors.fill: parent

            onPressed: {
                nextNormal.visible = !pressed
                nextClicked.visible = pressed
            }

            onReleased: {
                nextNormal.visible = !pressed
                nextClicked.visible = pressed
            }

            onClicked: {
                nextSongAnimation.running = true
            }
        }
    }

    Item {
        id: currentSong
        height: musicJukebox.appHeight > 480 ? 70 : 0.146 * musicJukebox.appHeight

        anchors.top: playButton.bottom
        anchors.topMargin: 0
        anchors.left: playButton.left
        anchors.leftMargin: 30
        anchors.right: plate.left

        Text {
            id: songTitle
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: artistName.top
            color: "#314d1f"
            font {
                pixelSize: musicJukebox.appHeight > 480 ? 25 : 0.052 * musicJukebox.appHeight
                family: nokiaPureBold.name; bold: true; italic: true
            }
            elide: "ElideRight"
            text: {
                if (musicJukebox.nowPlayingSong.songTitle)
                    return "\"" + musicJukebox.nowPlayingSong.songTitle + "\""
                else
                    return " "
            }
        }

        Text {
            id: artistName
            anchors.left: songTitle.left
            anchors.right: songTitle.right
            anchors.bottom: parent.bottom
            color: "#314d1f"
            font {
                pixelSize: musicJukebox.appHeight > 480 ? 18 : 0.0375 * musicJukebox.appHeight
                family: nokiaPureRegular.name; italic: true
            }
            elide: "ElideRight"
            text: musicJukebox.nowPlayingSong.artistName
        }
    }

    Connections {
        target: songTitle

        onTextChanged: {
            if (musicJukebox.behavior == MusicJukebox.JukeboxRankMode)
                nextSongAnimation.running = true
        }
    }

    Connections {
        target: musicJukebox
        onSongEnded: {
            if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode)
                nextSongAnimation.running = true
        }
        onSongDurationChanged: {
            playerItem.totalTime = time
        }
        onSongPositionChanged: {
            playerItem.currentTime = time
        }
    }
}
