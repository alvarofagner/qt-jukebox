/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: listView
    property bool inverted: false
    property variant model

    signal gestureBack()
    signal gestureNext()

    Image {
        id: bottomCorner
        source: inverted ? "qrc:/data/images/bg_empty_left_bord_bottom.png" : "qrc:/data/images/bg_empty_right_bord_bottom.png"
        anchors.bottom: parent.bottom
        anchors.left: {
            if (inverted)
                return parent.left
        }
        anchors.right: {
            if (!inverted)
                return parent.right
        }
    }

    Image {
        id: topCorner
        source: inverted ? "qrc:/data/images/bg_empty_left_bord_top.png" : "qrc:/data/images/bg_empty_right_bord_top.png"
        anchors.top: parent.top
        anchors.left: {
            if (inverted)
                return parent.left
        }
        anchors.right: {
            if (!inverted)
                return parent.right
        }
    }

    Rectangle {
        id: topBackground
        color: "#e8e5d0"
        anchors.top: parent.top
        anchors.bottom: centerBackground.top
        anchors.left: inverted ? topCorner.right : parent.left
        anchors.right: inverted ? parent.right : topCorner.left
    }

    Rectangle {
        id: centerBackground
        color: "#e8e5d0"
        anchors.top: topCorner.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: bottomCorner.top
    }

    Rectangle {
        id: bottomBackground
        color: "#e8e5d0"
        anchors.top: centerBackground.bottom
        anchors.bottom: parent.bottom
        anchors.left: inverted ? bottomCorner.right : parent.left
        anchors.right: inverted ? parent.right : bottomCorner.left
    }

    ListView {
        id: listCatalogView

        anchors.fill: parent

        model: parent.model
        interactive: false

        delegate: CatalogDelegate {
            id: delegate
            inverted: listView.inverted
        }
    }

    property int pressX: 0
    property int pressY: 0

    MouseArea {
        anchors.fill: parent
        onPressed: {
            pressX = mouseX
            pressY = mouseY
            mouse.accepted = true
        }

        onReleased: {
            if(!catalogRepeater.runningAnimation) {
                if (Math.abs(mouseX - pressX) > 10) {
                    if (mouseX > pressX)
                        listView.gestureNext();
                    else
                        listView.gestureBack();

                    mouse.accepted = true
                } else {
                    mouse.accepted = false
                    if (musicJukebox.behavior != MusicJukebox.JukeboxCatalogMode) {
                        var idx = listCatalogView.indexAt(mouseX, mouseY)
                        if (idx >= 0) {
                            var id = listCatalogView.model.getDataByIndex(idx, 0)
                            var songTitle = listCatalogView.model.getDataByIndex(idx, 1)
                            var artistName = listCatalogView.model.getDataByIndex(idx, 2)
                            showVoteDialog(id, songTitle, artistName)
                        }
                    }
                }
                flickableArea.interactive = (musicJukebox.behavior != MusicJukebox.JukeboxCatalogMode)
            }
        }

        onClicked: {
            mouse.accepted = false
        }

        onPositionChanged: {
            var g = Math.atan(Math.abs(mouseY - pressY) / Math.abs(mouseX - pressX))
            if (g < 0.6) {
                mouse.accepted = true
                flickableArea.interactive = false
            } else {
                mouse.accepted = false
                flickableArea.interactive = (musicJukebox.behavior != MusicJukebox.JukeboxCatalogMode)
            }
        }
    }
}
