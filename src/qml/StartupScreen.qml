/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: startupScreen
    anchors.fill: parent

    signal exit()
    signal autoPlayClicked()
    signal djProClicked()
    signal privateClicked()
    signal publicClicked()
    signal selectPartySelected()
    signal twitterSetupReady()
    signal twitterAuthenticationError(string reason, string description)

    function showLoading() {
        twitterSetupDialog.visible = false
        loadingLogin.visible = true
    }

    function hideLoading() {
        loadingLogin.visible = false
        twitterSetupDialog.visible = true
    }

    states: [
        State {
            name: "AUTHENTICATED"
            PropertyChanges { target: twitterSetupDialog; buttonOneText: "switch user" }
            PropertyChanges { target: twitterSetupDialog; buttonTwoEnabled: true } //continue button
            PropertyChanges { target: twitterSetupDialog; text: "<br />Your twitter account: <br />" + "<b>" + twitterClient.username + "</b>"}
        },
        State {
            name: "NEEDAUTH"
            PropertyChanges { target: twitterSetupDialog; buttonOneText: "sign in" }
            PropertyChanges { target: twitterSetupDialog; buttonTwoEnabled: false } // continue button
            PropertyChanges { target: twitterSetupDialog; text: "<br />Sign in on twitter!" }
        }
    ]

    Connections {
        target: twitterClient
        onCredentialsStatusChanged: {
            if (twitterClient.credentialsStatus == TwitterClient.CredentialsValid)
                startupScreen.state = "AUTHENTICATED"
            else
                startupScreen.state = "NEEDAUTH"

            hideLoading()
        }
        onAuthenticatedChanged: {
            twitterClient.verifyCredentials()
        }
        onErrorAuthenticating: {
            startupScreen.state = "NEEDAUTH"
            hideLoading()
            twitterAuthenticationError(reason, description)
        }
    }

    LoadingThrobber {
        id: loadingLogin
        anchors.fill: parent
        visible: false
        running: visible
        text: "Please wait..."
    }

    Item {
        id: buttons
        visible: true
        anchors.centerIn: parent
        anchors.fill: parent

        Row {
            id: rowGuestMode
            anchors.bottom: fixedButtons.top
            anchors.bottomMargin: 60
            anchors.left: fixedButtons.left

            spacing: 60
            visible: musicJukebox.behavior === MusicJukebox.JukeboxGuestMode ||
                     musicJukebox.behavior === MusicJukebox.JukeboxCatalogMode ||
                     musicJukebox.behavior === MusicJukebox.JukeboxRankMode

            Button {
                id: catalogModeButton
                buttonText: "CATALOG"

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                onClicked: {
                    musicJukebox.behavior = MusicJukebox.JukeboxCatalogMode
                    startupScreen.visible = false
                    selectPartySelected()
                }
            }

            Button {
                id: rankingModeButton
                buttonText: "RANKING"

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                onClicked: {
                    musicJukebox.behavior = MusicJukebox.JukeboxRankMode
                    startupScreen.visible = false
                    selectPartySelected()
                }
            }
        }

        Row {
            id: rowNormalMode
            anchors.bottom: fixedButtons.top
            anchors.bottomMargin: 60
            anchors.left: fixedButtons.left

            spacing: 60
            visible: !rowGuestMode.visible

            Button {
                id: joinPartyButton
                buttonText: "JOIN PARTY"

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                onClicked: {
                    musicJukebox.behavior = MusicJukebox.JukeboxClientMode
                    startupScreen.visible = false
                    selectPartySelected()
                }
            }

            Button {
                id: hostPartyButton

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                buttonText: "HOST PARTY"

                onClicked: {
                    musicJukebox.behavior = MusicJukebox.JukeboxDjMode
                    buttons.visible = false
                    playingModeDialog.visible = true
                }
            }
        }

        Row {
            id: fixedButtons
            spacing: 60
            anchors.verticalCenterOffset: aboutButton.height/2 + 30
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: aboutButton

                buttonText: "ABOUT"

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                onClicked: {
                    about.visible = true
                }
            }

            Button {
                id: close

                buttonText: "EXIT"

                width: startupScreen.width * 0.25
                height: startupScreen.height * 0.2

                onClicked: {
                    exitDialog.visible = true
                }
            }
        }
    }

    About {
        id: about
        anchors.fill: parent
        visible: false
    }

    GeneralDialog {
        id: playingModeDialog
        anchors.fill: parent
        title: "How do you want to play the songs?"
        textHorizontalAlignment: "AlignLeft"
        text: "<b>Auto-Play</b>" +
              "<br>" +
              "The player plays automatically the next song on the ranking. " +
              "The party sounds come from the application." +
              "<br><br>" +
              "<b>Dj-PRO</b>" +
              "<br>" +
              "The mode is ideal for professional Dj's who want to have total control " +
              "of the Party. The Dj can use his equipment to output sound while " +
              "using the application as guide on what to play based on ranking."

        numberOfButtons: 3
        buttonOneText: "auto-play"
        buttonTwoText: "dj-pro"
        buttonThreeText: "cancel"
        visible: false
    }

    Connections {
        target: playingModeDialog
        onButtonOneClicked: {
            partyTypeDialog.visible = true
            autoPlayClicked()
        }
        onButtonTwoClicked: {
            partyTypeDialog.visible = true
            djProClicked();
        }
        onButtonThreeClicked: {
            playingModeDialog.visible = false
            buttons.visible = true
        }
    }

    GeneralDialog {
        id: partyTypeDialog
        anchors.fill: parent
        title: "What kind of party do you want to create?"
        textHorizontalAlignment: "AlignLeft"
        text: "<b>Private</b>" +
              "<br>" +
              "Your party can not receive votes externally from Twitter, only from participants that have " +
              "the application installed on the device and in the same wireless network. No twitter sign " +
              "in is required to create this kind of party." +
              "<br><br>" +
              "<b>Public</b>" +
              "<br>" +
              "Your party can receive votes externally from Twitter. Participants can vote from a computer " +
              "or another device. Twitter sign in is required to create this kind of party."

        buttonOneText: "private"
        buttonTwoText: "public"
        buttonThreeText: "cancel"
        visible: false
    }

    GeneralDialog {
        id: twitterSetupDialog
        anchors.fill: parent
        title: "Twitter account setup"
        buttonOneText: "switch user"
        buttonTwoText: "continue"
        buttonThreeText: "cancel"
        visible: false
    }

    GeneralDialog {
        id: exitDialog
        anchors.fill: parent
        visible: false
        z: 1000

        numberOfButtons: 2
        buttonOneText: "yes"
        buttonTwoText: "no"
        defaultButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_normal.png"
        pressedButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_pressed.png"
        defaultButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_normal.png"
        pressedButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_pressed.png"

        text: "<br><br>Do you really want to exit?"

        onButtonOneClicked: {
            Qt.quit()
            return
        }
        onButtonTwoClicked: exitDialog.visible = false
    }

    Connections {
        target: partyTypeDialog
        onButtonOneClicked: {
            partyTypeDialog.visible = false
            privateClicked()

            startupScreen.visible = false
            buttons.visible = true
            musicJukebox.partyType = MusicJukebox.PrivateParty
        }
        onButtonTwoClicked: {
            partyTypeDialog.visible = false

            twitterSetupDialog.visible = true
            showLoading()
            twitterClient.verifyCredentials()

            musicJukebox.partyType = MusicJukebox.PublicParty
        }
        onButtonThreeClicked: {
            partyTypeDialog.visible = false
            playingModeDialog.visible = true
        }
    }

    Connections {
        target: twitterSetupDialog
        onButtonOneClicked: {
            if (startupScreen.state == "AUTHENTICATED")
                startupScreen.state = "NEEDAUTH"

            showLoading()
            twitterClient.auth()
        }
        onButtonTwoClicked: {
            startupScreen.visible = false
            buttons.visible = true
            twitterSetupDialog.visible = false
            twitterSetupReady()
        }
        onButtonThreeClicked: {
            twitterSetupDialog.visible = false
            partyTypeDialog.visible = true
        }
    }
}
