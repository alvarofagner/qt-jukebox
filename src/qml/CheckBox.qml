/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7

Item {
   id: checkBox
    property string defaultSrc: ""
    property string checkedSrc: ""

    property bool checked: false
    property variant image: imgDefault

    width: imgDefault.width
    height: imgDefault.height

    signal clicked

    Image {
        id: imgDefault
        smooth: true
        source: checkBox.defaultSrc
    }

    MouseArea {
        id: boxArea
        anchors.fill: parent
        onClicked: checked = !checked
    }

    states {
        State {
            name: "pressed"
            when: checked
            PropertyChanges {
                target: imgDefault
                source: checkBox.checkedSrc
            }
        }
    }
}
