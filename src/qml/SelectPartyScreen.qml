/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7

Item {
    id: selectPartyScreen
    anchors.fill: parent

    signal refresh()
    signal cancel()
    signal partySelected(string address, int port, string uuid)

    function showLoading() {
        timer.running = true
        lookingForParty.running = true
        lookingForParty.text = "Please wait."
        lookingForParty.visible = true
    }

    function startDiscover() {
        partyDiscover.timeout.connect(onTimeout)
        refreshButton.enabled = false
        partyDiscover.start()
    }

    function onTimeout() {
        partyDiscover.timeout.disconnect(onTimeout)
        refreshButton.enabled = true
    }

    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            lookingForParty.running = false
            lookingForParty.visible = false
            group.visible = true
        }
    }

    Item {
        id: lookingForParty
        anchors.fill: parent
        visible: false

        property alias running: loadingThrobber.running
        property alias text: loadingThrobber.text


        LoadingThrobber {
            id: loadingThrobber
            anchors.fill: parent
            visible: true

            Text {
                id: lookingForPartyText
                anchors.bottom: loadingThrobber.top
                anchors.bottomMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Looking for a party..."
            }
        }
    }


    Item {
        id: group
        anchors.fill: parent
        visible: false

        Text {
            id: message
            horizontalAlignment: "AlignLeft"
            anchors.top: parent.top
            anchors.topMargin: 0.05 * parent.height
            anchors.left: parent.left
            anchors.leftMargin: 30
            anchors.right: parent.right
            color: "#314d1f"
            font { pixelSize: Math.min(0.09 * parent.height, 0.049 * parent.width); family: nokiaPureBold.name }
            wrapMode: "WordWrap"
            text: "Please, select a Party to join:"
        }

        BorderImage {
            id: bg
            anchors.top: message.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0.2 * parent.height
            border.bottom: 60
            border.top: 60
            border.left: 60
            border.right: 60
            source: "qrc:/data/images/bg_importmusic.png"
        }

        ListView {
            id: partiesListView
            anchors.fill: bg
            anchors.margins: 50
            clip: true

            Component {
                id: partyDelegate

                Item {
                    width: partiesListView.width
                    height: 46

                    Rectangle {
                        id: selection
                        opacity: 0
                        color: "#d6d3bf"
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom

                        Behavior on opacity {
                            PropertyAnimation {}
                        }

                        states {
                            State {
                                name: "pressed"; when: fileMouseArea.pressed
                                PropertyChanges { target: selection; opacity: 1 }
                            }
                        }
                    }

                    Rectangle {
                        id: separator
                        color: "#c5c0ad"
                        height: 1
                        anchors.left: selection.left
                        anchors.right: selection.right
                        anchors.bottom: parent.bottom
                    }

                    Text {
                        id: fileText
                        text: name + " " + hashtag + " (" + address + ")"
                        anchors.left: selection.left
                        anchors.leftMargin: 10
                        anchors.right: selection.right
                        anchors.verticalCenter: parent.verticalCenter
                        elide: Text.ElideRight
                        color: "#707068"
                        font { family: nokiaPureRegular.name; pixelSize: 20; bold: true }
                    }

                    MouseArea {
                        id: fileMouseArea
                        anchors.fill: parent
                        onClicked: {
                            selectPartyScreen.visible = false
                            partySelected(address, port, uuid)
                        }
                    }
                }
            }

            model: partyModel
            delegate: partyDelegate

            Text {
                id: noPartiesFound
                anchors.fill: parent
                font.pixelSize: 0.1 * parent.height
                color: "#818181"
                visible: {
                    if (partyModel.rowCount() === 0)
                        return true
                    else
                        return false
                }

                horizontalAlignment: "AlignHCenter"
                verticalAlignment: "AlignVCenter"
                text: "No parties found."
            }

            Connections {
                target: partyModel
                onRowsInserted: {
                    if (partyModel.rowCount() > 0)
                        noPartiesFound.visible = false
                }
                onRowsRemoved: {
                    if (partyModel.rowCount() > 0)
                        noPartiesFound.visible = false
                    else
                        noPartiesFound.visible = true
                }
            }

        }

        Scrollbar {
            id: scrollbar
            opacity: partiesListView.moving ? 1 : 0
            anchors.top: partiesListView.top
            anchors.bottom: partiesListView.bottom
            anchors.right: partiesListView.right
            yPosition: partiesListView.visibleArea.yPosition
            heightRatio: partiesListView.visibleArea.heightRatio
            clip: true

            Behavior on opacity {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutCirc
                }
            }
        }

        Button {
            id: refreshButton
            defaultSrc: "qrc:/data/images/bt_small_normal.png"
            pressedSrc: "qrc:/data/images/bt_small_pressed.png"

            height: selectPartyScreen.height > 480 ? 56 : selectPartyScreen.height * 0.11
            width: selectPartyScreen.width > 864 ? 171 : selectPartyScreen.width * 0.2

            buttonText: "refresh"
            margin: 5
            defaultTextColor: "#a18e36"
            pressedTextColor: "#ffdf3e"

            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0.05 * parent.height
            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 0.04 * parent.width

            onClicked: {
                refresh()
                startDiscover()
            }
        }

        Button {
            id: cancelButton

            defaultSrc: "qrc:/data/images/bt_small_normal.png"
            pressedSrc: "qrc:/data/images/bt_small_pressed.png"
            defaultIconSrc: "qrc:/data/images/icon_bt_remove_normal.png"
            pressedIconSrc: "qrc:/data/images/icon_bt_remove_pressed.png"
            hasIcon: true

            height: selectPartyScreen.height > 480 ? 56 : selectPartyScreen.height * 0.11
            width: selectPartyScreen.width > 864 ? 171 : selectPartyScreen.width * 0.2

            buttonText: "cancel"
            margin: 5
            defaultTextColor: "#a18e36"
            pressedTextColor: "#ffdf3e"

            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0.05 * parent.height
            anchors.left: parent.horizontalCenter
            anchors.leftMargin: 0.04 * parent.width

            onClicked: {
                selectPartyScreen.visible = false
                cancel()
            }
        }
    }
}
