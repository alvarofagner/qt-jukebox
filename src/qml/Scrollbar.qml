import Qt 4.7

Item {
    id: scrollbar
    width: scrollbarKnob.width

    property real yPosition: 0
    property real heightRatio: 1

    Image {
        id: scrollbarKnob

        anchors.right: parent.right

        y: scrollbar.yPosition * scrollbar.height
        height: scrollbar.heightRatio * scrollbar.height

        source: "qrc:/data/images/nob.png"
    }
}

