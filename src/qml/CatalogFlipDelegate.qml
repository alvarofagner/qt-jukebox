/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Flipable {
    id: flipable
    z: catalogRepeater.count - index - 1

    property bool flipped: false
    property int flipDuration: 500

    transform: Rotation {
        id: rotation
        origin.x: -10
        origin.y: flipable.height/2
        axis { x: 0; y: 1; z: 0 }
        angle: 0
    }

    states: State {
        name: "back"
        when: flipable.flipped
    }

    transitions: [
        Transition {
            to: "back"
            SequentialAnimation {
                PropertyAction {
                    target: catalogRepeater
                    property: "runningAnimation"
                    value: true
                }
                NumberAnimation {
                    target: rotation
                    property: "angle"
                    duration: flipDuration / 2
                    to: -90
                }
                NumberAnimation {
                    target: flipable
                    property: "z"
                    duration: 0
                    to: index
                }
                NumberAnimation {
                    target: rotation
                    property: "angle"
                    duration: flipDuration / 2
                    to: -180
                }
                PropertyAction {
                    target: catalogRepeater
                    property: "runningAnimation"
                    value: false
                }
            }
        },
        Transition {
            from: "back"
            SequentialAnimation {
                PropertyAction {
                    target: catalogRepeater
                    property: "runningAnimation"
                    value: true
                }
                NumberAnimation {
                    target: rotation
                    property: "angle"
                    duration: flipDuration / 2
                    to: -90
                }
                NumberAnimation {
                    target: flipable
                    property: "z"
                    duration: 0
                    to: catalogRepeater.count - index - 1
                }
                NumberAnimation {
                    target: rotation
                    property: "angle"
                    duration: flipDuration / 2
                    to: 0
                }
                PropertyAction {
                    target: catalogRepeater
                    property: "runningAnimation"
                    value: false
                }
            }
        }
    ]

    front: CatalogView {
        id: frontCatalogView
        inverted: false

        width: parent.width
        height: parent.height
        onGestureBack: {
            flipable.flipped = !flipable.flipped
        }
        model: frontModel
    }

    back: CatalogView {
        id: backCatalogView
        inverted: true

        width: parent.width
        height: parent.height
        onGestureNext: {
            flipable.flipped = !flipable.flipped
        }
        model: backModel
    }
}
