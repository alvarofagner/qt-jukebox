/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: delegate
    width: parent.width
    height: listView.height / 6

    property bool inverted: false

    function isEven(index) {
        if (index % 2 == 0)
            return true
        return false
    }

    signal clicked(int index)

    MouseArea {
        id: mouseArea
        width: parent.width
        height: parent.height

        anchors.fill: parent

        onClicked: {
            delegate.clicked(index);
            mouse.accepted = false
        }
        onPressed: {
            mouse.accepted = false
        }
        onReleased: {
            mouse.accepted = false
        }
    }

    Rectangle {
        id: centerBackground
        color: isEven(index) ? "#d7d0b7" : "#e3e0c6"
        anchors.left: inverted ? idBackgroundCenter.right : parent.left
        anchors.right: inverted ? parent.right : idBackgroundCenter.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: idBackgroundTop
        color: isEven(index) ? "#a2cb3c" : "#afe044"
        visible: inverted ? topLeftCorner.visible : topRightCorner.visible
        anchors.left: inverted ? topLeftCorner.right : idBackgroundCenter.left
        anchors.right: inverted ? idBackgroundCenter.right : topRightCorner.left
        anchors.top: parent.top
        anchors.bottom: idBackgroundCenter.top
    }

    Rectangle {
        id: idBackgroundCenter
        color: isEven(index) ? "#a2cb3c" : "#afe044"
        width: 0.18 * parent.width
        anchors.left: {
            if (inverted)
                return parent.left
            else
                return parent.right
        }
        anchors.leftMargin: inverted ? 0 : -width
        anchors.top: {
            if (inverted) {
                if (topLeftCorner.visible)
                    return topLeftCorner.bottom
                else
                    return parent.top
            } else {
                if (topRightCorner.visible)
                    return topRightCorner.bottom
                else
                    return parent.top
            }
        }
        anchors.bottom: {
            if (inverted) {
                if (bottomLeftCorner.visible)
                    return bottomLeftCorner.top
                else
                    return parent.bottom
            } else {
                if (bottomRightCorner.visible)
                    return bottomRightCorner.top
                else
                    return parent.bottom
            }
        }
    }

    Rectangle {
        id: idBackgroundBottom
        color: isEven(index) ? "#a2cb3c" : "#afe044"
        visible: inverted ? bottomLeftCorner.visible : bottomRightCorner.visible
        anchors.left: inverted ? bottomLeftCorner.right : idBackgroundCenter.left
        anchors.right: inverted ? idBackgroundCenter.right : bottomRightCorner.left
        anchors.top: idBackgroundCenter.bottom
        anchors.bottom: parent.bottom
    }

    Image {
        id: topLeftCorner
        visible: index == 0 && inverted
        anchors.top: parent.top
        anchors.left: parent.left
        smooth: true
        fillMode: "PreserveAspectFit"
        source: "qrc:/data/images/catalog_item_top_left_corner.png"
    }

    Image {
        id: topRightCorner
        visible: index == 0 && !inverted
        anchors.top: parent.top
        anchors.right: parent.right
        smooth: true
        fillMode: "PreserveAspectFit"
        source: "qrc:/data/images/catalog_item_top_right_corner.png"
    }

    Image {
        id: bottomLeftCorner
        visible: index == 5 && inverted // XXX change the 5 for the itemsByPage came from the model
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        smooth: true
        fillMode: "PreserveAspectFit"
        source: "qrc:/data/images/catalog_item_bottom_left_corner.png"
    }

    Image {
        id: bottomRightCorner
        visible: index == 5 && !inverted // XXX change the 5 for the itemsByPage came from the model
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        fillMode: "PreserveAspectFit"
        source: "qrc:/data/images/catalog_item_bottom_right_corner.png"
    }

    Item {
        id: musicItemTitle
        height: parent.height
        anchors.right: inverted ? delegate.right : idBackgroundCenter.left
        anchors.left: inverted ? idBackgroundCenter.right : delegate.left

        Text {
            id: textTitle
            anchors.top: parent.top
            anchors.bottom: parent.verticalCenter
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 16
            anchors.rightMargin: 16
            anchors.bottomMargin: 1

            text: "\""+SongTitle+"\""
            font {
                family: nokiaPureBold.name; pixelSize: 0.3 * delegate.height;
                italic: true; bold: true; weight: Font.Bold
            }
            color: isEven(index) ? "#615E57" : "#7B7B72"
            verticalAlignment: "AlignBottom"
            elide: Text.ElideRight
        }

        Text {
            id: textAlbum
            width: parent.width
            anchors.top: parent.verticalCenter
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 16
            anchors.bottomMargin: 7

            text: "" + ArtistName
            font { family: nokiaPureRegular.name; italic: true; pixelSize: 0.24 * delegate.height }
            color: isEven(index) ? "#71706E" : "#878681"
            elide: Text.ElideRight
        }
    }

    Item {
        id: musicItemId
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: inverted ? idBackgroundCenter.right : parent.right
        anchors.left: inverted ? parent.left : idBackgroundCenter.left

        Text {
            id: textId
            anchors.fill: parent
            color: "#314d1f"

            text: "#"+SongId
            font {
                family: nokiaPureBold.name; pixelSize: 0.24 * delegate.height;
                bold: true; weight: Font.Bold
            }

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

}
