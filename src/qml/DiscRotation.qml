/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/


import Qt 4.7

Item {
    id: discRotation

    property int index: 0

    Timer {
        id: timer
        interval: 500
        running: true
        repeat: true
        onTriggered: discRotation.index == 24 ? discRotation.index = 0 : discRotation.index + 1
    }

    Image {
        id: discImage

        source: "qrc:/data/images/disc_"+discRotation.index+".png"

        /*NumberAnimation on rotation {
            running: true; from: 0; to: 360; loops: Animation.Infinite; duration: 1200
        }*/
    }
}
