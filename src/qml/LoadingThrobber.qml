/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import QtQuick 1.0
import com.qtjukebox.types 1.0

Item {
    id: loadingThrobber
    property alias text: loadingMessage.text
    property alias running: throbber.running

    Text {
        id: loadingMessage
        width: 600
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: "AlignVCenter"
        horizontalAlignment: "AlignHCenter"
        color: "#314d1f"
        font { pixelSize: 24; family: nokiaPureBold.name; bold: true }
        text: "Loading..."
    }

    Throbber {
        id: throbber
        anchors.top: loadingMessage.bottom
        anchors.topMargin: 10
    }

}
