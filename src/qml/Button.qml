/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7

Item {
    id: button

    property string defaultSrc: "qrc:/data/images/bt_big_normal.png"
    property string pressedSrc: "qrc:/data/images/bt_big_pressed.png"
    property string defaultIconSrc: ""
    property string pressedIconSrc: ""

    property bool hasIcon: false
    property bool enabled: true

    property string buttonText: ""
    property string defaultTextColor: "#314d1f"
    property string pressedTextColor: "#314d1f"

    property real textSize: button.height/3
    property int margin: 0

    property alias border: imgDefault.border

    opacity: enabled ? 1 : 0.5

    signal clicked

    BorderImage {
        id: imgDefault

        anchors.fill: parent

        smooth: true
        source: buttonArea.pressed ? button.pressedSrc : button.defaultSrc
        horizontalTileMode: BorderImage.Stretch
        verticalTileMode: BorderImage.Stretch
        border { left: 20; top: 4; right: 20; bottom: 8 }
    }

    MouseArea {
        id: buttonArea
        anchors.fill: parent
        enabled: button.enabled

        onClicked: button.clicked()
    }

    Text {
        anchors.fill: parent
        text: button.buttonText

        font { family: nokiaPureBold.name; pixelSize: 0.12 * parent.width; bold: true }

        color: buttonArea.pressed ? button.pressedTextColor : button.defaultTextColor

        anchors.bottomMargin: margin
        anchors.leftMargin: hasIcon ? 30 : 0
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter       
    }

   Image {
        id: icon

        smooth: true
        source: buttonArea.pressed ? button.pressedIconSrc : button.defaultIconSrc
        visible: hasIcon

        anchors.top: button.top
        anchors.left: button.left

        anchors.topMargin: button.hasIcon ? button.height * 0.25 : 0
        anchors.leftMargin: button.hasIcon ? button.width * 0.15 : 0

    }

   Component.onCompleted: {
       if (defaultIconSrc != "" && pressedIconSrc != "")
           hasIcon = true
   }
}
