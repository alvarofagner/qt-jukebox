/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: item
    signal modalmode(bool enabled)
    anchors.fill: parent

    states: [
        State {
            name: "visibleSortButtons"
            AnchorChanges {
                target: sortAndSearchButtons
                anchors.verticalCenter: buttonsArea.verticalCenter
                anchors.left: buttonsArea.left
            }

            PropertyChanges {
                target: sortAndSearchButtons
                anchors.leftMargin: sortAndSearchButtons.y
            }
        },
        State {
            name: "hiddenSortButtons"
            AnchorChanges {
                target: sortAndSearchButtons
                anchors.top: buttonsArea.bottom
                anchors.left: buttonsArea.left
            }

            PropertyChanges {
                target:sortAndSearchButtons
                anchors.leftMargin: sortAndSearchButtons.x
            }
        }
    ]

    transitions: Transition {
        AnchorAnimation {
            duration: 500
            easing.type: Easing.InQuad
        }
    }

    Rectangle {
        id: buttonsArea
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: bgArea.top
        anchors.bottomMargin: -1 * connectionDivisor1.height
        opacity: 0
    }

    SortAndSearchButtons {
        id: sortAndSearchButtons
        height: musicJukebox.appHeight > 480 ? 63 : item.height * 0.13

        onClicked: {
            if (state == 0) {
                musicJukebox.sortCatalogByArtistName()
            } else {
                musicJukebox.sortCatalogBySongName()
            }
        }
    }

    Button {
        id: closeButton
        defaultSrc: "qrc:/data/images/bt_close.png"
        pressedSrc: "qrc:/data/images/bt_close.png"
        anchors.verticalCenter: buttonsArea.verticalCenter
        anchors.right: buttonsArea.right
        anchors.rightMargin: {
            if (snapEnabled)
                return musicJukebox.appWidth - buttonSnapUp.x + musicJukebox.appWidth * 0.01
            else
                return closeButton.y
        }

        width: 40
        height: 40

        onClicked: {
            exitDialog.visible = true
        }
    }

    HashTag {
        id: hashTag

        anchors.right: snapEnabled ? closeButton.left : importCatalog.left
        anchors.rightMargin: musicJukebox.appWidth * 0.01
        anchors.verticalCenter: buttonsArea.verticalCenter
    }

    Button {
        id: importCatalog

        defaultSrc: "qrc:/data/images/bt_small_normal.png"
        pressedSrc: "qrc:/data/images/bt_small_pressed.png"
        defaultIconSrc: "qrc:/data/images/icon_bt_import_normal.png"
        pressedIconSrc: "qrc:/data/images/icon_bt_import_pressed.png"
        hasIcon: true

        height: musicJukebox.appHeight > 480 ? 56 : item.height * 0.11
        width:  musicJukebox.appWidth > 864 ? 171 :item.width * 0.20

        buttonText: "import"
        defaultTextColor: "#a18e36"
        pressedTextColor: "#ffdf3e"
        margin: 5
        textSize: item.height * 0.04

        visible: musicJukebox.importCatalog
        anchors.verticalCenter: buttonsArea.verticalCenter
        anchors.right: closeButton.left
        anchors.rightMargin: musicJukebox.appWidth * 0.01

        onClicked: {
            sortAndSearchButtons.visible = false
            bgArea.visible = false
            modalmode(true)
            jukeboxMainView.importSongs()
        }
    }

    Item {
        id: bgArea
        anchors.fill: parent
        anchors.topMargin: Math.floor(0.18 * parent.height)
        anchors.leftMargin: Math.floor(0.01 * parent.width)
        anchors.rightMargin: Math.floor(0.01 * parent.width)
        anchors.bottomMargin: Math.floor(0.01 * parent.width)
    }

    Image {
        id: cornerTopLeft
        source: "qrc:/data/images/bg_catalog_top_left.png"
        anchors.top: topCenterDivisor.bottom
        anchors.left: bgArea.left
        clip: true
    }

    Image {
        id: topCenterDivisor
        source: "qrc:/data/images/lombada_slice_top.png"
        anchors.top: bgArea.top
        anchors.horizontalCenter: bgArea.horizontalCenter
    }

    Image {
        id: cornerTopRight
        source: "qrc:/data/images/bg_catalog_top_right.png"
        anchors.top: topCenterDivisor.bottom
        anchors.right: bgArea.right
    }

    Image {
        id: cornerBottomRight
        source: "qrc:/data/images/bg_catalog_bottom_right.png"
        anchors.bottom: bgArea.bottom
        anchors.right: bgArea.right
    }

    Image {
        id: cornerBottomLeft
        source: "qrc:/data/images/bg_catalog_bottom_left.png"
        anchors.bottom: bgArea.bottom
        anchors.left: bgArea.left
    }

    Image {
        id: bottomBorder
        source: "qrc:/data/images/bg_bottom_slice.png"
        fillMode: "TileHorizontally"
        anchors.bottom: bgArea.bottom
        anchors.left: cornerBottomLeft.right
        anchors.right: cornerBottomRight.left
    }

    Rectangle {
        id: leftBorder
        color: "#14200c"
        width: 4
        anchors.top: cornerTopLeft.bottom
        anchors.left: cornerTopLeft.left
        anchors.bottom: cornerBottomLeft.top
    }

    Rectangle {
        id: centerBackground
        color: "#e8e5d0"
        anchors.left: leftBorder.right
        anchors.right: rightBorder.left
        anchors.top: cornerTopLeft.bottom
        anchors.topMargin: 0
        anchors.bottom: cornerBottomLeft.top
    }

    Rectangle {
        id: topBackground
        color: "#e8e5d0"
        anchors.left: cornerTopLeft.right
        anchors.right: cornerTopRight.left
        anchors.top: cornerTopLeft.top
        anchors.bottom: centerBackground.top
    }

    Rectangle {
        id: bottomBackground
        color: "#e8e5d0"
        anchors.left: cornerBottomLeft.right
        anchors.right: cornerBottomRight.left
        anchors.top: centerBackground.bottom
        anchors.bottom: bottomBorder.top
    }

    Rectangle {
        id: rightBorder
        color: "#14200c"
        width: 4
        anchors.top: cornerTopRight.bottom
        anchors.right: cornerTopRight.right
        anchors.bottom: cornerBottomRight.top
    }

    Image {
        id:middleDivisor1
        source: "qrc:/data/images/lombada_slice_middle_darker.png"
        fillMode: "TileVertically"
        anchors.top: topCenterDivisor.bottom
        anchors.bottom: connectionDivisor1.top
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id: connectionDivisor1
        source: "qrc:/data/images/lombada_slice_connection.png"
        anchors.bottom: bgArea.verticalCenter
        anchors.bottomMargin: Math.floor(bgArea.height/2 * 0.65)
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id:middleDivisor2
        source: "qrc:/data/images/lombada_slice_middle_lighter.png"
        fillMode: "TileVertically"
        anchors.top: connectionDivisor1.bottom
        anchors.bottom: connectionDivisor2.top
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id: connectionDivisor2
        source: "qrc:/data/images/lombada_slice_connection2.png"
        anchors.bottom: bgArea.verticalCenter
        anchors.bottomMargin: Math.floor(bgArea.height/2 * 0.3)
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id:middleDivisor3
        source: "qrc:/data/images/lombada_slice_middle_darker.png"
        fillMode: "TileVertically"
        anchors.top: connectionDivisor2.bottom
        anchors.bottom: connectionDivisor3.top
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id: connectionDivisor3
        source: "qrc:/data/images/lombada_slice_connection.png"
        anchors.bottom: bgArea.bottom
        anchors.bottomMargin: Math.floor(bgArea.height/2 * 0.65)
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id:middleDivisor4
        source: "qrc:/data/images/lombada_slice_middle_lighter.png"
        fillMode: "TileVertically"
        anchors.top: connectionDivisor3.bottom
        anchors.bottom: connectionDivisor4.top
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id: connectionDivisor4
        source: "qrc:/data/images/lombada_slice_connection2.png"
        anchors.bottom: bgArea.bottom
        anchors.bottomMargin: Math.floor(bgArea.height/2 * 0.3)
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id:middleDivisor5
        source: "qrc:/data/images/lombada_slice_middle_darker.png"
        fillMode: "TileVertically"
        anchors.top: connectionDivisor4.bottom
        anchors.bottom: bottomCenterDivisor.top
        anchors.horizontalCenter: bottomCenterDivisor.horizontalCenter
    }

    Image {
        id: bottomCenterDivisor
        source: "qrc:/data/images/lombada_slice_bottom.png"
        anchors.bottom: bgArea.bottom
        anchors.horizontalCenter: bgArea.horizontalCenter
    }

    Item {
        id: leftPage
        anchors.left: leftBorder.right
        anchors.right: topCenterDivisor.left
        anchors.top: topCenterDivisor.bottom
        anchors.bottom: bottomBorder.top
    }

    Image {
        id: firstPageLogo
        source: "qrc:/data/images/logo_catalog_small.png"
        anchors.left: leftPage.left
        anchors.leftMargin: 0.25 * leftPage.width
        anchors.top: leftPage.top
        anchors.topMargin: 0.18 * leftPage.height
    }

    Text {
        id: message1
        anchors.left: firstPageLogo.left
        anchors.top: firstPageLogo.bottom
        anchors.topMargin: 0.08 * leftPage.height
        anchors.right: leftPage.right
        anchors.rightMargin: 0.1 * leftPage.width

        font { family: nokiaPureRegular.name; pixelSize: 0.04 * leftPage.height }
        color: "#9d998a"
        verticalAlignment: "AlignBottom"
        wrapMode: "WordWrap"
        text: "YOU CAN NAVIGATE BETWEEN THE CATALOG PAGES BY FLICKING THEM " +
              "TO THE LEFT OR TO THE RIGHT"
    }

    Rectangle {
        id: separtor
        height: 1
        color: "#9d998a"
        anchors.right: leftPage.right
        anchors.left: firstPageLogo.left
        anchors.top: message1.bottom
        anchors.topMargin: 0.04 * leftPage.height
    }

    Text {
        id: message2
        anchors.left: message1.left
        anchors.top: separtor.bottom
        anchors.topMargin: 0.04 * leftPage.height
        anchors.right: message1.right

        font { family: nokiaPureRegular.name; pixelSize: 0.04 * leftPage.height }
        color: "#9d998a"
        verticalAlignment: "AlignBottom"
        wrapMode: "WordWrap"

        text: "THE CATALOG CAN BE SORTED ALPHABETICALLY BY SONG NAME OR " +
              "ARTIST BY TAPPING ON THE ICONS"
    }

    Item {
        id: rightPage
        anchors.left: topCenterDivisor.right
        anchors.right: rightBorder.left
        anchors.top: topCenterDivisor.bottom
        anchors.bottom: bottomBorder.top
    }

    Image {
        id: lastPageLogo
        source: "qrc:/data/images/logo_catalog_big.png"
        anchors.centerIn: rightPage
    }

    Item {
        id: repeaterContainer
        anchors.top: topBackground.top
        anchors.right: bgArea.right
        anchors.left: topCenterDivisor.right
        anchors.bottom: bottomBackground.bottom

        Repeater {
            id: catalogRepeater
            anchors.fill: parent

            property bool runningAnimation: false

            model: catalogModel

            CatalogFlipDelegate {
                anchors.fill: parent
            }
        }
    }

    Connections {
        target: importDialog
        onCancel: {
            bgArea.visible = true
            sortAndSearchButtons.visible = true

            if (musicJukebox.behavior == MusicJukebox.JukeboxDjMode)
                modalmode(true)
            else
                modalmode(false)
        }
    }

    Connections {
        target: flickableArea

        onVisibleViewChanged: {
            if (flickableArea.visibleView == 3) {
                item.state = "visibleSortButtons"
            } else {
                item.state = "hiddenSortButtons"
            }
        }
    }

    LoadingThrobber {
        id: loadingThrobber
        anchors.fill: item
        visible: false
    }

    Connections {
        target: musicJukebox
        onCatalogLoadedChanged: {
            // TODO On load changed it have to check the actual catalogLoaded state
            // in order to stop or initiate the throbber
            loadingThrobber.visible = false
            bgArea.visible = true
            sortAndSearchButtons.visible = true
        }
    }

    Component.onCompleted: sortAndSearchButtons.state = "visibleSortButtons"

    GeneralDialog {
        id: exitDialog
        anchors.fill: parent
        visible: false
        z: 1000

        numberOfButtons: 2
        buttonOneText: "yes"
        buttonTwoText: "no"
        defaultButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_normal.png"
        pressedButtonOneIconSrc: "qrc:/data/images/icon_bt_vote_pressed.png"
        defaultButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_normal.png"
        pressedButtonTwoIconSrc: "qrc:/data/images/icon_bt_remove_pressed.png"

        text: "<br><br>Do you really want to exit?"

        onButtonOneClicked: {
            Qt.quit()
            return
        }
        onButtonTwoClicked: exitDialog.visible = false
    }
}
