/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7

Item {
    id: throbber

    width: row.width
    height: row.height	
    anchors.horizontalCenter: parent.horizontalCenter
    property int count: 0
    property bool running: false

    Component {
        id: dot

        Image {
            id: throbber
            source: "qrc:/data/images/throbber_01.png"

            states {
                State {
                    name: "off"
                    PropertyChanges { target: throbber; source: "qrc:/data/images/throbber_01.png" }
                }

                State {
                    name: "on"
                    PropertyChanges { target: throbber; source: "qrc:/data/images/throbber_03.png" }
                }
            }
        }
    }

    Row {
        id: row
	anchors.horizontalCenter: parent.horizontalCenter	
        spacing: 4

        children: [
            Loader { sourceComponent: dot },
            Loader { sourceComponent: dot },
            Loader { sourceComponent: dot }
        ]
    }

    Timer {
        id: timer
        repeat: true
        interval: 200
        running: throbber.running

        onTriggered: {
            if (count > 0)
                row.children[count - 1].item.state = "off"

            if (count < row.children.length)
                row.children[count].item.state = "on"

            count = (count + 1) % (row.children.length + 1)
        }

        onRunningChanged: {
            var i;

            if (!running) {
                for (i = 0; i < row.children.length; i++)
                    row.children[i].item.state = "off"
            }
        }
    }
}
