/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7

Item {
    id: input
    property string src: ""
    property string initialText: ""
    property variant image: imgDefault
    property int mode: TextInput.Normal
    property alias text: imgDefaultInput.text
    property alias input: imgDefaultInput

    function closeSoftwareInputPanel() {
        imgDefaultInput.closeSoftwareInputPanel()
    }

    signal textChanged
    signal accepted

    clip: true

    Image {

        id: imgDefault
        smooth: true
        source: input.src
        anchors.fill: parent

        height: 70

        Text {
            id: placeHolderText
            anchors.fill: parent
            anchors.leftMargin: 30
            anchors.rightMargin: 30
            horizontalAlignment: "AlignLeft"
            verticalAlignment: "AlignVCenter"
            font { family: nokiaPureRegular.name; pixelSize: 0.32 * imgDefault.height; italic: true }
            color: "#9b988e"
            text: imgDefaultInput.text ? "" : initialText
        }

        TextInput {
            id: imgDefaultInput
            anchors.fill: parent
            anchors.topMargin: 25
            anchors.leftMargin: 30
            anchors.rightMargin: 30
            font.pixelSize: 0.32 * imgDefault.height
            color: "#65655e"
            echoMode: input.mode
            text: input.text

            onFocusChanged: {
                if (focus && text == "")
                    placeHolderText.text = ""
                else if (!focus && text == "")
                    placeHolderText.text = initialText
            }

            onAccepted: {
                closeSoftwareInputPanel()
                focus = false
                input.accepted()
            }

            onTextChanged: {
                input.textChanged()

                // FIXME: This should use property binding instead
                if (input.text != "")
                    placeHolderText.text = ""
            }
        }
    }
}
