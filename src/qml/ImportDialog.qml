import QtQuick 1.0
import com.qtjukebox.types 1.0

Item {
    id: importDialog
    width: 854
    height: 480

    property string filePath: ""
    property string currentDir: ""
    property variant actualParent

    signal cancel
    signal filesSelected(variant paths)

    function getPathBase(path) {
        var pathStr = "" + path
        var stringList = pathStr.split("/")
        return stringList.pop()
    }

    function getFileExtension(filename) {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined
    }

    Rectangle {
        id: gradientBackground
        anchors.fill: parent
        visible: true
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#a0db3e" }
            GradientStop { position: 1.0; color: "#5ca92e" }
        }

        Image {
            anchors.fill: parent
            fillMode: "Tile"
            source: "qrc:/data/images/dots.png"
            visible: {
                if (musicJukebox.platform == MusicJukebox.MobilePlatform)
                    return true
                else
                    return false
            }
        }

        Image {
            id: topLeftCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            rotation: -90
            anchors.left: parent.left
        }

        Image {
            id: topRightCorner
            source: "qrc:/data/images/bord.png"
            anchors.top: parent.top
            anchors.right: parent.right
        }

        Image {
            id: bottomLeftCorner
            source: "qrc:/data/images/bord.png"
            rotation: 180
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }

        Image {
            id: bottomRightCorner
            source: "qrc:/data/images/bord.png"
            rotation: 90
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    Item {
        id: header
        anchors.top: parent.top
        anchors.topMargin: 18
        anchors.left: parent.left
        anchors.leftMargin: 23
        anchors.right: parent.right
        anchors.rightMargin: 20

        Button {
            id: back
            defaultSrc: "qrc:/data/images/bt_back_normal.png"
            pressedSrc: "qrc:/data/images/bt_back_pressed.png"
            onClicked: importPathsModel.path = importPathsModel.parentPath

            width: {
                if ((musicJukebox.appWidth >= 700) && (musicJukebox.appHeight >= 300))
                    return 52
                else
                    return 38
            }
            height: {
                if ((musicJukebox.appWidth >= 700) && (musicJukebox.appHeight >= 300))
                    return 52
                else
                    return 38
            }
        }

        Text {
            id: importFolder
            text: "IMPORT THIS FOLDER?"
            color: "white"
            anchors.left: back.right
            anchors.leftMargin: 20
            anchors.top: back.top
            font {
                family: nokiaPureRegular.name
                pixelSize: 16
                weight: Font.Light
            }
        }

        Text {
            id: folderName
            text: getPathBase(importPathsModel.path)
            color: "white"
            anchors.top: importFolder.bottom
            anchors.left: importFolder.left
            anchors.right: buttons.left
            anchors.rightMargin: 20
            elide: Text.ElideRight
            font {
                family: nokiaPureRegular.name
                pixelSize: 26
                bold: true
            }
        }

        Row {
            id: buttons
            anchors.top: header.top
            anchors.right: parent.right
            anchors.topMargin: 1
            spacing: 10

            Button {
                id: importButton
                defaultSrc: "qrc:/data/images/bt_small_normal.png"
                pressedSrc: "qrc:/data/images/bt_small_pressed.png"
                defaultIconSrc: "qrc:/data/images/icon_bt_import_normal.png"
                pressedIconSrc: "qrc:/data/images/icon_bt_import_pressed.png"
                hasIcon: true

                buttonText: "import"
                margin: 5
                defaultTextColor: "#a18e36"
                pressedTextColor: "#ffdf3e"
                textSize: importDialog.height * 0.035

                height: importDialog.height > 480 ? 56 : importDialog.height * 0.11
                width:  importDialog.width > 864 ? 171 : importDialog.width * 0.2

                anchors.bottom: parent.bottom
                anchors.bottomMargin: -4

                onClicked: {
                    var paths = importPathsModel.selectedPaths()
                    filesSelected(paths)
                }
            }

            Button {
                id: cancelButton
                defaultSrc: "qrc:/data/images/bt_small_normal.png"
                pressedSrc: "qrc:/data/images/bt_small_pressed.png"
                defaultIconSrc: "qrc:/data/images/icon_bt_remove_normal.png"
                pressedIconSrc: "qrc:/data/images/icon_bt_remove_pressed.png"
                hasIcon: true

                buttonText: "cancel"
                margin: 5
                defaultTextColor: "#a18e36"
                pressedTextColor: "#ffdf3e"
                textSize: importDialog.height * 0.035

                height: importDialog.height > 480 ? 56 : importDialog.height * 0.11
                width:  importDialog.width > 864 ? 171 : importDialog.width * 0.2

                anchors.bottom: parent.bottom
                anchors.bottomMargin: -4

                onClicked: {
                    importDialog.visible = false
                    importDialog.currentDir = ""
                    cancel()
                }
            }
        }
    }

    BorderImage {
        id: bg
        border.left: 60
        border.right: 60
        border.top: 60
        border.bottom: 60
        anchors.top: parent.top
        anchors.topMargin: 0.15 * parent.height
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        source: "qrc:/data/images/bg_importmusic.png"
    }

    ImportPathsModel {
        id: importPathsModel
        stringFilters: ["*.mp3", "*.xspf"]
    }

    ListView {
        id: folderView
        anchors.fill: bg
        anchors.margins: 50
        clip: true

        Component {
            id: fileDelegate

            Item {
                width: folderView.width
                height: 46

                Item {
                    id: icon
                    height: parent.height
                    width: 40
                    anchors.verticalCenter: parent.verticalCenter

                    Image {
                        id: iconImage
                        anchors.centerIn: parent
                        visible: importPathsModel.isFolder(index) ? true : false
                        source: "qrc:/data/images/icon_folder.png"
                    }

                    BorderImage {
                        id: fileIcoBg
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.topMargin: 10
                        anchors.bottomMargin: 10
                        anchors.bottom: parent.bottom
                        visible: importPathsModel.isFolder(index) ? false : true
                        source: "qrc:/data/images/label_bg.png"
                        border.left: 10; border.top: 10
                        border.right: 10; border.bottom: 10
                    }

                    Text {
                        id: fileTypeText
                        color: "white"
                        anchors.centerIn: fileIcoBg
                        text: {
                            if (importPathsModel.isFolder(index)) {
                                return " "
                            } else {
                                var extension = getFileExtension(fileName)
                                if (extension == "MP3" || extension == "mp3") {
                                    return "MP3"
                                } else if (extension == "XSPF" || extension == "xspf") {
                                    return "XSPF"
                                } else {
                                    return " "
                                }
                            }
                        }

                        font {
                            family: nokiaPureRegular.name
                            pixelSize: 0.7 * iconImage.height
                        }
                    }
                }

                Rectangle {
                    id: selection
                    opacity: 0
                    color: "#d6d3bf"
                    anchors.left: icon.right
                    anchors.leftMargin: 10
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom

                    Behavior on opacity {
                        PropertyAnimation {}
                    }

                    states {
                        State {
                            name: "pressed"; when: fileMouseArea.pressed
                            PropertyChanges { target: selection; opacity: 1}
                        }
                    }
                }

                Rectangle {
                    id: separator
                    color: "#c5c0ad"
                    height: 1
                    anchors.left: selection.left
                    anchors.right: selection.right
                    anchors.bottom: parent.bottom
                }

                Text {
                    id: fileText
                    text: fileName
                    anchors.left: selection.left
                    anchors.leftMargin: 10
                    anchors.right: fileCheckBox.left
                    anchors.rightMargin: fileCheckBox.anchors.rightMargin
                    anchors.verticalCenter: parent.verticalCenter
                    elide: Text.ElideRight
                    color: "#707068"
                    font {
                        family: nokiaPureRegular.name
                        pixelSize: 20
                        bold: true
                    }
                }

                CheckBox {
                    id: fileCheckBox
                    defaultSrc: "qrc:/data/images/check_empty.png"
                    checkedSrc: "qrc:/data/images/check_filled.png"
                    anchors.right: selection.right
                    anchors.rightMargin: fileCheckBox.y
                    anchors.verticalCenter: parent.verticalCenter
                    checked: importPathsModel.isChecked(index)

                    MouseArea {
                        id: checkMouseArea
                        anchors.fill: parent
                        onClicked: {
                            if (fileCheckBox.checked) {
                                fileCheckBox.checked = false
                                importPathsModel.setChecked(index, false)
                            } else {
                                fileCheckBox.checked = true
                                importPathsModel.setChecked(index, true)
                            }
                        }
                    }
                }

                MouseArea {
                    id: fileMouseArea
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: fileCheckBox.left
                    onClicked: {
                        if (importPathsModel.isFolder(index)) {
                            importDialog.currentDir = filePath
                            importPathsModel.path = filePath
                        }
                    }
                }
            }
        }

        model: importPathsModel
        delegate: fileDelegate
    }

    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            scrollbar.opacity = folderView.moving ? 1 : 0
        }
    }

    Scrollbar {
        id: scrollbar
        clip: true
        opacity: folderView.moving ? 1 : 0
        anchors.top: folderView.top
        anchors.bottom: folderView.bottom
        anchors.left: folderView.right
        anchors.leftMargin: 4
        yPosition: folderView.visibleArea.yPosition
        heightRatio: folderView.visibleArea.heightRatio

        Behavior on opacity {
            NumberAnimation {
                duration: 500
                easing.type: Easing.OutCirc
            }
        }
    }

    Connections {
        target: importDialog
        onVisibleChanged: {
            if (importDialog.visible) {
                scrollbar.opacity = 1
                timer.start()
            } else {
                scrollbar.opacity = 0
            }
        }
    }

    Connections {
        target: folderView
        onMovingChanged: scrollbar.opacity = folderView.moving ? 1 : 0
    }

}
