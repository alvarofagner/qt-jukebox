/****************************************************************************
**
** This file is part of QtJukebox **
**
** Copyright © 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
**
** Name: Qt Jukebox
** Version: Beta
** Contact: http://qt.nokia.com/about/contact-us
**
** This software, including documentation, is protected by copyright
** controlled by Nokia Corporation.  All rights reserved.  Copying,
** including reproducing, storing, adapting or translating, any or all
** of this material requires prior written consent of Nokia Corporation.
** This material also contains confidential information which may not be
** disclosed to others :wwithout the prior written consent of Nokia.
**
****************************************************************************/

import Qt 4.7
import com.qtjukebox.types 1.0

Item {
    id: createPartyScreen
    anchors.fill: parent

    property int maxWidth: 864
    property int maxHeight: 480
    property string selectedCatalog: ""

    signal importSongs()
    signal create(string partyName)
    signal cancel()

    function avoidSpecialCharacters(text) {
        // XXX For now it is only scaping spaces
        var newText = text.replace(/[^a-zA-Z-]/g, "")
        return newText
    }

    function setCatalog(filePath) {
        selectedCatalog = filePath

        // FIXME: This assignment should not be needed, since
        // songsAddedInput.text is bound to selectedCatalog.
        // Something is breaking the binding.
        songsAddedInput.text = selectedCatalog
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            songsAddedInput.closeSoftwareInputPanel()
            partyNameInput.closeSoftwareInputPanel()
            songsAddedInput.focus = false
            partyNameInput.focus = false
            createPartyScreen.focus = true
        }
    }

    /*
     * This item is used to keep the view elements centered even
     * in a high resolution.
     */
    Item {
        id: contents
        anchors.centerIn: parent
        opacity: 0
        z: 2
        width: {
            if (parent.width > maxWidth)
                return maxWidth
            else
                return parent.width
        }
        height: {
            if (parent.height > maxHeight)
                return maxHeight
            else
                return parent.height
        }
    }

    Text {
        id: message
        horizontalAlignment: "AlignLeft"
        anchors.top: contents.top
        anchors.topMargin: 0.05 * contents.height
        anchors.left: contents.left
        anchors.leftMargin: 0.05 * contents.height
        anchors.right: contents.right
        anchors.rightMargin: 0.05 * contents.height
        color: "#314d1f"
        font { family: nokiaPureBold.name; pixelSize: Math.min(0.09 * contents.height, 0.049 * contents.width) }
        wrapMode: "WordWrap"
        text: "Please, complete these steps to <b>create your party:</b>"
    }

    Button {
        id: importButton

        defaultSrc: "qrc:/data/images/bt_small_normal.png"
        pressedSrc: "qrc:/data/images/bt_small_pressed.png"
        defaultIconSrc: "qrc:/data/images/icon_bt_import_normal.png"
        pressedIconSrc: "qrc:/data/images/icon_bt_import_pressed.png"
        hasIcon: true

        width: contents.width * 0.2
        height: contents.height * 0.11

        buttonText: "import"
        defaultTextColor: "#a18e36"
        pressedTextColor: "#ffdf3e"
        margin: 5
        textSize: contents.width * 0.02

        anchors.top: partyNameInput.bottom
        anchors.topMargin: 0.02 * contents.height
        anchors.left: partyNameInput.left
        visible: true //XXX chage this after have the view completed for import catalog

        onClicked: {
            importSongs()
        }
    }

    Input {
        id: partyNameInput
        src: "qrc:/data/images/entry_box.png"
        initialText: "party name"
        anchors.top: message.bottom
        anchors.topMargin: 10
        anchors.left: message.left
        width: 0.51 * contents.width
        height: 0.17 * contents.height
    }

    Text {
        id: songsAdded
        anchors.verticalCenter: importButton.verticalCenter
        anchors.left: importButton.right
        anchors.leftMargin: 0.01 * contents.width
        visible: true
        color: "#314d1f"
        font { family: nokiaPureRegular.name; pixelSize: 0.04 * contents.width }
        text: "No files added"
        elide: Text.ElideRight
        anchors.right: contents.right
        anchors.rightMargin: 0.05 * contents.width
    }

    Text {
        id: hashtag
        anchors.verticalCenter: partyNameInput.verticalCenter
        anchors.left: partyNameInput.right
        anchors.leftMargin: 0.01 * contents.width
        visible: musicJukebox.partyType == MusicJukebox.PublicParty
        color: "#314d1f"
        font { family: nokiaPureRegular.name; pixelSize: 0.04 * contents.width }
        text: "#QtJB-" + avoidSpecialCharacters(partyNameInput.text)
        elide: Text.ElideRight
        anchors.right: contents.right
        anchors.rightMargin: 0.05 * contents.width
    }

    Connections {
        target: partyNameInput
        onTextChanged: hashtag.text = "#QtJB-" + avoidSpecialCharacters(partyNameInput.text)
    }

    Component.onCompleted:  {
        if (musicJukebox.localPartyName != "")
            partyNameInput.text = musicJukebox.localPartyName

        if (musicJukebox.catalogCount == 0)
            songsAdded.text = "No files added"
        else if (musicJukebox.catalogCount == 1)
            songsAdded.text = "1 file added"
        else
            songsAdded.text = "" + musicJukebox.catalogCount + " files added"
    }

    Connections {
        target: musicJukebox
        onCatalogCountChanged: {
            if (musicJukebox.catalogCount == 0)
                songsAdded.text = "No files added"
            else if (musicJukebox.catalogCount == 1)
                songsAdded.text = "1 file added"
            else
                songsAdded.text = "" + musicJukebox.catalogCount + " files added"
        }
    }

    Button {
        id: createButton

        width: contents.width * 0.33
        height: contents.height * 0.2

        buttonText: "CREATE"
        enabled: partyNameInput.text

        anchors.bottom: contents.bottom
        anchors.bottomMargin: 0.05 * contents.height
        anchors.right: contents.horizontalCenter
        anchors.rightMargin: 0.01 * contents.width

        onClicked: {
            if (musicJukebox.partyType == MusicJukebox.PublicParty) {
                musicJukebox.party.hashtag = hashtag.text
            }
            create(partyNameInput.text)
        }
    }

    Button {
        id: cancelButton

        width: contents.width * 0.33
        height: contents.height * 0.2

        buttonText: "CANCEL"

        anchors.bottom: contents.bottom
        anchors.bottomMargin: 0.05 * contents.height
        anchors.left: contents.horizontalCenter
        anchors.leftMargin: 0.01 * contents.width

        onClicked: {
            cancel()
            createPartyScreen.visible = false
        }
    }
}
