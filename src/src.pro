include(../shared.pri)

TEMPLATE = subdirs

use-3rdparty: SUBDIRS += $$PWD/3rdparty
SUBDIRS += $$PWD/core

OTHER_FILES += \
    $$PWD/qml/About.qml \
    $$PWD/qml/Button.qml \
    $$PWD/qml/Catalog.qml \
    $$PWD/qml/CatalogDelegate.qml \
    $$PWD/qml/CatalogFlipDelegate.qml \
    $$PWD/qml/CatalogModel.qml \
    $$PWD/qml/CatalogView.qml \
    $$PWD/qml/CheckBox.qml \
    $$PWD/qml/CreatePartyScreen.qml \
    $$PWD/qml/GeneralDialog.qml \
    $$PWD/qml/HashTag.qml \
    $$PWD/qml/ImportDialog.qml \
    $$PWD/qml/Input.qml \
    $$PWD/qml/JukeboxMainView.qml \
    $$PWD/qml/LoadingThrobber.qml \
    $$PWD/qml/LoginScreen.qml \
    $$PWD/qml/Player.qml \
    $$PWD/qml/RankedList.qml \
    $$PWD/qml/RankedListDelegate.qml \
    $$PWD/qml/SelectPartyScreen.qml \
    $$PWD/qml/ScrollDialog.qml \
    $$PWD/qml/Scrollbar.qml \
    $$PWD/qml/SortAndSearchButtons.qml \
    $$PWD/qml/SplashScreen.qml \
    $$PWD/qml/StartupScreen.qml \
    $$PWD/qml/Throbber.qml \
    $$PWD/qml/main.qml
