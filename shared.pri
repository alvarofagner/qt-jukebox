BUILD_TREE += $$OUT_PWD
SOURCE_TREE += $$PWD
PREFIX = $$PREFIX
TESTS = $$TESTS

isEmpty(PREFIX): PREFIX = /usr

CONFIG(debug, debug|release) {
    DEFINES += JUKEBOX_DEBUG
} else {
    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
}

contains(MEEGO_EDITION,harmattan) {
    CONFIG += harmattan
    DEFINES += Q_WS_MAEMO_6
}

harmattan|macx|win32|symbian: CONFIG += use-3rdparty

contains(CONFIG,use-3rdparty): DEFINES += USE_3RDPARTY
contains(CONFIG,use-guestmode): DEFINES += USE_GUESTMODE
