TEMPLATE = app
TARGET = twitter_search
QT += network

LIBS += \
    $$BUILD_TREE/lib/libqjson.a

INCLUDEPATH += \
    $$BUILD_TREE/include

SOURCES += \
    main.cpp \
    searchrequester.cpp

HEADERS += \
    searchrequester.h

