#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include "searchrequester.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    SearchRequester req;

    if (argc > 1) {
	req.search(argv[1]);
    } else {
	qWarning() << "Usage:" << argv[0] << "query_string";
	return 1;
    }

    return app.exec();
}
