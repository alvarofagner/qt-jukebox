#ifndef SEARCHREQUESTER_H
#define SEARCHREQUESTER_H

#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTimer>
#include <QtCore/QUrl>
#include <QtCore/QVariant>

class QNetworkAccessManager;
class QNetworkReply;

class SearchRequester : public QObject
{
    Q_OBJECT

public:
    SearchRequester(QObject *parent = 0);
    ~SearchRequester();

    bool search(const QByteArray &query);

private slots:
    void onFinished(QNetworkReply *reply);
    void parseNextUrl();

private:
    bool parseData(const QByteArray &data);
    bool parseResults(const QVariant &results);

    QNetworkAccessManager *m_manager;
    QTimer *m_timer;
    QList<QUrl> m_urlQueue;
};

#endif
