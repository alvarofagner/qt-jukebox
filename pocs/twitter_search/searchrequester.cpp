#include "searchrequester.h"
#include <QtCore/QDebug>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <qjson/parser.h>

const QUrl searchUrl("http://search.twitter.com/search.json");

SearchRequester::SearchRequester(QObject *parent)
    : QObject(parent)
    , m_manager(new QNetworkAccessManager(this))
    , m_timer(new QTimer(this))
    , m_urlQueue()
{
    connect(m_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(onFinished(QNetworkReply*)));
    connect(m_timer, SIGNAL(timeout()),
            SLOT(parseNextUrl()));
    m_timer->start(1000);
}

SearchRequester::~SearchRequester()
{
}

bool SearchRequester::search(const QByteArray &query)
{
    QUrl newUrl = searchUrl;
    newUrl.addQueryItem("q", query);
    newUrl.addEncodedQueryItem("show_user", "true"); //
    m_manager->get(QNetworkRequest(newUrl));
    return true;
}

void SearchRequester::onFinished(QNetworkReply *reply)
{
    if (reply) {
        m_timer->stop();
        parseData(reply->readAll());
        m_timer->start(1000);
    }
}

void SearchRequester::parseNextUrl()
{
    if (!m_urlQueue.empty()) {
        m_manager->get(QNetworkRequest(m_urlQueue.takeFirst()));
    }
}

bool SearchRequester::parseData(const QByteArray &data)
{
    QJson::Parser parser;
    bool ok = true;
    QVariant ret = parser.parse(data, &ok);
    if (ok) {
        //qDebug() << ret;
        if (ret.type() == QVariant::Map) {
            QVariantMap map = ret.toMap();
            //qDebug() << map.keys();
            parseResults(map["results"]);
            /*QByteArray nextPageUrl = map["next_page"].toByteArray();
            if (!nextPageUrl.isEmpty()) {
                QUrl newUrl(searchUrl);
                // remove first interrogation "?"
                newUrl.setEncodedQuery(nextPageUrl.mid(1));
                //qDebug() << newUrl.queryItems();
                m_urlQueue.append(newUrl);
            }*/
            if (map["page"].toInt() <= 1) {
                QByteArray refreshUrl = map["refresh_url"].toByteArray();
                if (!refreshUrl.isEmpty()) {
                    QUrl newUrl(searchUrl);
                    // remove first interrogation "?"
                    newUrl.setEncodedQuery(refreshUrl.mid(1));
                    //qDebug() << newUrl.queryItems();
                    m_urlQueue.append(newUrl);
                }
            }
        }
    }

    return ok;
}

bool SearchRequester::parseResults(const QVariant &results)
{
    if (results.type() == QVariant::List) {
        QVariantList list = results.toList();
        qDebug() << "found" << list.count() << "items";
        /*foreach (const QVariant result, list) {
            if (result.type() == QVariant::Map) {
                QVariantMap map = result.toMap();
                qDebug() << map["text"].toString();
            }
        }*/
        return true;
    } else {
        return false;
    }
}
