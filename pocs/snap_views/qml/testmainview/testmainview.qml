import Qt 4.7
import Qt.labs.gestures 1.0

Rectangle {
    width: 864
    height: 480
    color: "white"

    //    Image {
    //        id: secondBackground
    //        anchors.fill: parent
    //        fillMode: "Tile"
    //        source: "bg.png"
    //    }

    property int limit: 50

    function setCurrentViewArea(area) {
        if (area == 0) {
            flickableArea.contentY = area0.pos.y
            flickableArea.visibleView = 0
            console.log("Set view 0")
        } else if (area == 1) {
            flickableArea.contentY = area1.pos.y
            flickableArea.visibleView = 1
            console.log("Set view 1")
        } else if (area == 2) {
            flickableArea.contentY = area2.pos.y
            flickableArea.visibleView = 2
            console.log("Set view 2")
        } else if (area == 3) {
            flickableArea.contentY = area3.pos.y
            flickableArea.visibleView = 3
            console.log("Set view 3")
        }
    }

    function goBack() {
        if (flickableArea.visibleView == 0) {
            flickableArea.contentY = area0.pos.y
        } else if (flickableArea.visibleView == 1) {
            flickableArea.contentY = area1.pos.y
        } else if (flickableArea.visibleView == 2) {
            flickableArea.contentY = area2.pos.y
        } else if (flickableArea.visibleView == 3) {
            flickableArea.contentY = area3.pos.y
        }
    }

    Flickable {
        id: flickableArea
        width: 864
        height: 480
        contentHeight: 3*480 //XXX the background is 1317???

        property int visibleView: 0  //0, 1, 2, 3

        Behavior on contentY {
            NumberAnimation {easing.type: "OutQuart"}
        }

//        Image {
//            id: mainBackground
//            anchors.fill: parent
//            fillMode: "Tile"
//            source: "bg.png"
//            //smooth: true
//        }

        Rectangle {
            id: area0
            anchors.top: parent.top
            height: 480
            width: parent.width
            opacity: 0.5
            color: "green"

            Text {
                anchors.top: parent.top
                text: "Area 0"
            }
        }

        Rectangle {
            id: area1
            anchors.top: area0.top
            anchors.topMargin: 150
            height: 480
            width: parent.width
            opacity: 0.5
            color: "blue"

            Text {
                anchors.top: parent.top
                text: "Area 1"
            }
        }

        Rectangle {
            id: area2
            anchors.top: area0.bottom
            height: 480
            width: parent.width
            opacity: 0.5
            color: "red"

            Text {
                anchors.top: parent.top
                text: "Area 2"
            }
        }

        Rectangle {
            id: area3
            anchors.top: area2.bottom
            height: 480
            width: parent.width
            opacity: 0.5
            color: "yellow"

            Text {
                anchors.top: parent.top
                text: "Area 3"
            }
        }

        Connections {
            target: flickableArea
            onFlickStarted: console.log("---- Flick started")
            onFlickEnded: console.log("---- Flick ended")
            onMovementEnded: {
                console.log(">>>>> Movement ended")
                if (flickableArea.visibleView == 0) {
                    if (flickableArea.contentY > (area0.pos.y + limit)) {
                        setCurrentViewArea(1)
                    } else {
                        goBack()
                    }
                } else if (flickableArea.visibleView == 1) {
                    if (flickableArea.contentY < (area1.pos.y - limit)) {
                        setCurrentViewArea(0)
                    }
                    if (flickableArea.contentY > (area1.pos.y + limit)) {
                        setCurrentViewArea(2)
                    } else {
                        goBack()
                    }
                } else if (flickableArea.visibleView == 2) {
                    if (flickableArea.contentY < (area2.pos.y - limit)) {
                        setCurrentViewArea(1)
                    }
                    if (flickableArea.contentY > (area2.pos.y + limit)) {
                        setCurrentViewArea(3)
                    } else {
                        goBack()
                    }
                } else if (flickableArea.visibleView == 3) {
                    if (flickableArea.contentY < (area3.pos.y - limit)) {
                        setCurrentViewArea(2)
                    } else {
                        goBack()
                    }
                }
            }

            onContentYChanged: {
                if (flickableArea.flicking) {
                    console.log("Flicking..........")
                    if (flickableArea.visibleView == 0) {
                        if (flickableArea.contentY > (area0.pos.y + limit)) {
                            setCurrentViewArea(1)
                        }
                    } else if (flickableArea.visibleView == 1) {
                        if (flickableArea.contentY < (area1.pos.y - limit)) {
                            setCurrentViewArea(0)
                        }
                        if (flickableArea.contentY > (area1.pos.y + limit)) {
                            setCurrentViewArea(2)
                        }
                    } else if (flickableArea.visibleView == 2) {
                        if (flickableArea.contentY < (area2.pos.y - limit)) {
                            setCurrentViewArea(1)
                        }
                        if (flickableArea.contentY > (area2.pos.y + limit)) {
                            setCurrentViewArea(3)
                        }
                    } else if (flickableArea.visibleView == 3) {
                        if (flickableArea.contentY < (area3.pos.y - limit)) {
                            setCurrentViewArea(2)
                        }
                    }
                }
            }
        }

        Component.onCompleted: {
            setCurrentViewArea(3)
        }
    }
}
