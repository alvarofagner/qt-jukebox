#include "circularprogress.h"

#include <qdeclarative.h>
#include <QDeclarativeView>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<CircularProgress>("Progress", 1, 0, "CircularProgress");

    QDeclarativeView view;
    view.setSource(QUrl::fromLocalFile("main.qml"));
    view.show();

    return app.exec();
}
