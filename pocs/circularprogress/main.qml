import Progress 1.0
import QtQuick 1.0

Item {
    width: 500
    height: 500

    Item {
        id: music
        property int currentTime: 0
        property int totalTime: 10
    }

    CircularProgress {
        anchors.centerIn: parent
        width: 500
        height: 500
        progress: music.currentTime / music.totalTime
        clockWise: true
        startPosition: CircularProgress.Left

        Behavior on progress {
            NumberAnimation { easing.type: Easing.Linear; duration: 1000 }
        }
    }

    Timer {
        interval: 1000
        running: music.currentTime < music.totalTime
        repeat: true
        onTriggered: music.currentTime += 1
    }

    MouseArea {
        anchors.fill: parent
    }

    Text {
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            bottomMargin: 20
        }

        text: "Progress: " + music.currentTime + " / " + music.totalTime
    }
}
