TEMPLATE = app
TARGET = circularprogress
QT += declarative

HEADERS += \
    circularprogress.h

SOURCES += \
    main.cpp \
    circularprogress.cpp
