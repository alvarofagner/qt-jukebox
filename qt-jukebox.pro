PROJECTNAME = Qt Jukebox

DEBUG=$$DEBUG
TESTS=$$TESTS

isEqual(DEBUG, 1){
    CONFIG += debug
}

CONFIG(release, debug|release) {
    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT
}

unix:!symbian: {
    include($$PWD/src/data/data.pri)
}

isEqual(TESTS, 1) {
    include(tests/tests.pri)
    include(tests/check.pri)
}

include(shared.pri)
include(src/src.pro)
