coverage_genhtml.commands = genhtml --output-directory $$BUILD_TREE/doc/coverage/ \
                                    $$BUILD_TREE/doc/coverage/coverage_final.info

coverage_extract.commands = lcov --extract $$BUILD_TREE/doc/coverage/coverage.info \
                            \"$$SOURCE_TREE/src/*\" --output-file $$BUILD_TREE/doc/coverage/coverage_final.info

coverage_capture.commands = mkdir -p $$BUILD_TREE/doc/coverage/ && \
                            lcov -directory $$BUILD_TREE/src --directory $$BUILD_TREE/tests \
                            --capture --output-file $$BUILD_TREE/doc/coverage/coverage.info

run_tests.commands = cd $$BUILD_TREE/tests && \
                     $(MAKE) -f $(MAKEFILE) install

check.depends = sub-src sub-tests run_tests coverage_capture coverage_extract coverage_genhtml

QMAKE_EXTRA_TARGETS += check run_tests coverage_capture coverage_extract coverage_genhtml
